#!/bin/bash
set -e
export PGPASSWORD=$POSTGRES_PASSWORD;
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
 CREATE USER $APP_DB_USER_LOCAL WITH PASSWORD '$APP_DB_PASS_LOCAL';
 ALTER ROLE $APP_DB_USER_LOCAL with SUPERUSER;
 CREATE DATABASE $APP_DB_NAME_LOCAL;
 GRANT ALL PRIVILEGES ON DATABASE $APP_DB_NAME_LOCAL TO $APP_DB_USER_LOCAL;
 GRANT ALL ON DATABASE $APP_DB_NAME_LOCAL TO $APP_DB_USER_LOCAL;
 ALTER DATABASE $APP_DB_NAME_LOCAL OWNER TO $APP_DB_USER_LOCAL;
 GRANT USAGE, CREATE ON SCHEMA PUBLIC TO $APP_DB_USER_LOCAL;
 GRANT ALL ON SCHEMA public TO $APP_DB_USER_LOCAL;
 GRANT USAGE ON schema public TO $APP_DB_USER_LOCAL;
 GRANT SELECT ON ALL TABLES IN SCHEMA public TO $APP_DB_USER_LOCAL;
 GRANT CREATE ON SCHEMA public TO $APP_DB_USER_LOCAL;
 \connect $APP_DB_NAME_LOCAL $APP_DB_USER_LOCAL
 BEGIN;
 -- arboles_familiares.perfil definition

-- Drop table

-- DROP TABLE arboles_familiares.perfil;

CREATE SCHEMA arboles_familiares;

CREATE TABLE arboles_familiares.perfil (
	id_perfil numeric NOT NULL,
	descripcion varchar(30) NOT NULL,
	estado bool NOT NULL,
	CONSTRAINT perfil_pkey PRIMARY KEY (id_perfil)
);


INSERT INTO arboles_familiares.perfil
(id_perfil, descripcion, estado)
VALUES(1, 'Administrador', true);
INSERT INTO arboles_familiares.perfil
(id_perfil, descripcion, estado)
VALUES(2, 'Analista', true);


--------------------------------------------------------------------------

-- arboles_familiares.usuario definition

-- Drop table

-- DROP TABLE arboles_familiares.usuario;

CREATE TABLE arboles_familiares.usuario (
	id_usuario numeric NOT NULL,
	identificacion varchar(10) NOT NULL,
	nombre_completo varchar(128) NOT NULL,
	estado bool NOT NULL,
	fecha_creacion timestamp NOT NULL,
	fecha_actualizacion timestamp NULL,
	contrasenia text NOT NULL,
	estado_contrasenia bool NOT NULL,
	correo varchar(100) NULL,
	CONSTRAINT usuario_pkey PRIMARY KEY (id_usuario)
);

INSERT INTO arboles_familiares.usuario
(id_usuario, identificacion, nombre_completo, estado, fecha_creacion, fecha_actualizacion, contrasenia, estado_contrasenia, correo)
VALUES(1, '1002867800', 'Mabel Ortiz', true, '2023-06-06 15:47:19.365', '2023-06-27 16:50:52.971', 'gAAAAABkm1m8yZemVyOQ-3sAKVBAnOTRS6dCCU1Sns5dqAlyPxa31dBvl4j1DwmkUudtcKfmNUsRxy0yJa_-nqgXnKParFBOEA==', true, 'mabel.ortiz@sercop.gob.ec');

------------------------------------------------------------------------------


-- arboles_familiares.perfil_usuario definition

-- Drop table

-- DROP TABLE arboles_familiares.perfil_usuario;

CREATE TABLE arboles_familiares.perfil_usuario (
	id_perfil_usuario numeric NOT NULL,
	id_usuario numeric NOT NULL,
	id_perfil numeric NOT NULL,
	fecha_asignacion varchar(100) NOT NULL,
	CONSTRAINT perfil_usuario_pkey PRIMARY KEY (id_perfil_usuario)
);


-- arboles_familiares.perfil_usuario foreign keys

ALTER TABLE arboles_familiares.perfil_usuario ADD CONSTRAINT fk_perfil FOREIGN KEY (id_perfil) REFERENCES arboles_familiares.perfil(id_perfil);
ALTER TABLE arboles_familiares.perfil_usuario ADD CONSTRAINT fk_perfil_usuario FOREIGN KEY (id_usuario) REFERENCES arboles_familiares.usuario(id_usuario);

INSERT INTO arboles_familiares.perfil_usuario
(id_perfil_usuario, id_usuario, id_perfil, fecha_asignacion)
VALUES(1, 1, 1, '2023-06-19 12:48:21.419407');


----------------------------------------------------------------------------------

-- arboles_familiares.registro_consulta definition

-- Drop table

-- DROP TABLE arboles_familiares.registro_consulta;

CREATE TABLE arboles_familiares.registro_consulta (
	id_consulta numeric NOT NULL,
	id_usuario numeric NOT NULL,
	cedulas_consultadas text NOT NULL,
	motivo_consulta varchar(100) NOT NULL,
	fecha_consulta timestamp NOT NULL,
	path_archivo_respaldo text NULL,
	respuesta_cedulas_consultadas text NULL,
	nombre_archivo_respaldo text NULL,
	CONSTRAINT consulta_pkey PRIMARY KEY (id_consulta)
);


-- arboles_familiares.registro_consulta foreign keys

ALTER TABLE arboles_familiares.registro_consulta ADD CONSTRAINT fk_usuario_consulta FOREIGN KEY (id_usuario) REFERENCES arboles_familiares.usuario(id_usuario);

---------------------------------------------------------------------------------

-- arboles_familiares.perfil_id_seq definition

-- DROP SEQUENCE arboles_familiares.perfil_id_seq;

CREATE SEQUENCE public.perfil_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 2
	CACHE 1
	NO CYCLE;

---------------------------------------------------------------

-- arboles_familiares.perfil_usuario_id_seq definition

-- DROP SEQUENCE arboles_familiares.perfil_usuario_id_seq;

CREATE SEQUENCE public.perfil_usuario_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 2
	CACHE 1
	NO CYCLE;
---------------------------------------------------------------

-- arboles_familiares.registro_consulta_id_seq definition

-- DROP SEQUENCE arboles_familiares.registro_consulta_id_seq;

CREATE SEQUENCE public.registro_consulta_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;

---------------------------------------------------------------


-- arboles_familiares.usuario_id_seq definition

-- DROP SEQUENCE arboles_familiares.usuario_id_seq;

CREATE SEQUENCE public.usuario_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 2
	CACHE 1
	NO CYCLE;

COMMIT;

EOSQL



