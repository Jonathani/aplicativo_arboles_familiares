#!/bin/sh
postgres_ready() {
python3 << END
import sys
import psycopg2
try:
    psycopg2.connect(
        dbname="$APP_DB_NAME_LOCAL",
        user="$APP_DB_USER_LOCAL",
        password="$APP_DB_PASS_LOCAL",
        host="$APP_DB_HOST_LOCAL",
        port="$APP_DB_PORT_LOCAL",
    )
except psycopg2.OperationalError:
    sys.exit(-1)
sys.exit(0)

END
}

until postgres_ready; do
  >&2 echo 'Espere hasta que haya conexión a la base de datos'
  sleep 1
done
>&2 echo 'Conexión exitosa a la base de datos PostgreSQL'

exec "$@"