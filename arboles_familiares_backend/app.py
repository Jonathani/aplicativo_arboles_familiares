from flask import Flask
from src.ec.gob.sercop.arboles_familiares.controllers.AuthenticationController import init as init_authentication
from src.ec.gob.sercop.arboles_familiares.controllers.GestionUsuariosController import init as init_usuarios
from src.ec.gob.sercop.arboles_familiares.controllers.GestionRegistroConsultaController import init as init_consultas
from src.ec.gob.sercop.arboles_familiares.controllers.GeneracionPerfilRiesgoController import init as init_perfil_riesgo
from src.ec.gob.sercop.arboles_familiares.controllers.GestionHistorialLaboralController import init as init_historial_laboral
from flask_cors import CORS

UPLOAD_FOLDER = '/public/archivos_justificacion'
UPLOAD_FOLDER_PERFIL_RIESGO = '/public/archivos_justificacion_perfil_riesgo'

def create_app():
    app = Flask(__name__)
    app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
    app.config['UPLOAD_FOLDER_PERFIL_RIESGO'] = UPLOAD_FOLDER_PERFIL_RIESGO
    CORS(app, support_credentials=True)
    init_authentication(app)
    init_usuarios(app)
    init_consultas(app)
    init_perfil_riesgo(app)
    init_historial_laboral(app)
    return app

if __name__ == '__main__':
    app = create_app()
    app.run(host='0.0.0.0',port=5010, debug=True)