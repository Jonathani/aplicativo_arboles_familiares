import psycopg2
from sqlalchemy import create_engine
import os


db_string_local = f"postgresql://{os.environ['APP_DB_USER_LOCAL']}:{os.environ['APP_DB_PASS_LOCAL']}@{os.environ['APP_DB_HOST_LOCAL']}:{os.environ['APP_DB_PORT_LOCAL']}/{os.environ['APP_DB_NAME_LOCAL']}"
db_local = create_engine(db_string_local)