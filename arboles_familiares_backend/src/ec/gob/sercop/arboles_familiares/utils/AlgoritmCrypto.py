from cryptography.fernet import Fernet
import base64

class AlgoritmCrypto:

    private_key = b'\xf2\xca\x95EI\xb4\x9a\xe9EG\xbf\xb0\xfa;\xdf\xe3\x1b\xd95\xe0\x088\x8d\xb8\xb2\xf2=B\xa2P\xcd\x8a'
    mi_key = base64.urlsafe_b64encode(private_key)
    fernet = Fernet(mi_key)

    # encrypt
    @staticmethod
    def encrypt(password):
        ciphertext = AlgoritmCrypto.fernet.encrypt(password.encode())
        return ciphertext

    # decrypt
    @staticmethod
    def decrypt(ciphertext):
        decMessage = AlgoritmCrypto.fernet.decrypt(ciphertext).decode()
        return decMessage