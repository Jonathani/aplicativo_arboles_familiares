from pickle import NONE
import random
import string
import smtplib, ssl
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import tomli
from datetime import datetime 
import os
from werkzeug.utils import secure_filename

class Utilidades:

    @staticmethod
    def get_random_string(length):
        letters = string.ascii_lowercase
        result_str = ''.join(random.choice(letters) for i in range(length))
        return result_str
    
    @staticmethod
    def enviar_correos(datos):   
        
        smtp_servidor = os.environ['APP_MAIL_SMTP']
        port_server =  os.environ['APP_MAIL_PORT']
        sender_email = os.environ['APP_MAIL_EMAIL']
        password = os.environ['APP_MAIL_PASS']

        email_message = MIMEMultipart()
        email_message['From'] = sender_email
        email_message['To'] = datos["correo_a_enviar"]
        email_message['Subject'] = datos["asunto"]
        email_message.attach(MIMEText(datos["mensaje_a_enviar"], "html"))
        email_string = email_message.as_string()
        try:
            context = ssl.create_default_context()
            with smtplib.SMTP_SSL(smtp_servidor, port_server, context=context) as server:
                server.login(sender_email, password)
                server.sendmail(sender_email, datos["correo_a_enviar"], email_string)
            print ("Email sent successfully desde aqui...!")
        except Exception as ex:
            print ("Something went wrong….",ex)

    @staticmethod
    def subir_archivos(registro_consulta, id_usuario, archivo_respaldo, directorio_subida):
        nombre_archivo = ""
        now = datetime.now()
        try:
            archivo_total = archivo_respaldo.filename.split(".")
            tipo_archivo = archivo_total[len(archivo_total) - 1] 
            nombre_archivo = f"{registro_consulta.id_consulta}{id_usuario}{now.strftime('%Y%m%d%H%M%S%f')}.{tipo_archivo}"
            archivo_respaldo.filename = nombre_archivo
            filename = secure_filename(archivo_respaldo.filename)
            archivo_respaldo.save(os.path.join(directorio_subida, filename))

        except Exception as ex:
            print ("Something went wrong….",ex)

        return nombre_archivo
    
    @staticmethod
    def subir_archivos_generico(id_archivo, id_usuario, archivo_respaldo, directorio_subida):
        nombre_archivo = ""
        now = datetime.now()
        try:
            archivo_total = archivo_respaldo.filename.split(".")
            tipo_archivo = archivo_total[len(archivo_total) - 1] 
            nombre_archivo = f"{id_archivo}{id_usuario}{now.strftime('%Y%m%d%H%M%S%f')}.{tipo_archivo}"
            archivo_respaldo.filename = nombre_archivo
            filename = secure_filename(archivo_respaldo.filename)
            archivo_respaldo.save(os.path.join(directorio_subida, filename))

        except Exception as ex:
            print ("Something went wrong….",ex)

        return nombre_archivo