
from sqlalchemy.ext.automap import automap_base
from sqlalchemy import Column, Integer, Date, Boolean, Text, Sequence, ForeignKey
from sqlalchemy.orm import Session, relationship
from src.ec.gob.sercop.arboles_familiares.utils.Conexion import db_local
from src.ec.gob.sercop.arboles_familiares.entities.Usuario import Usuario
from src.ec.gob.sercop.arboles_familiares.entities.Catalogo import Catalogo

Base = automap_base()

class PerfilRiesgo(Base):
    __tablename__ = "perfil_de_riesgo"
    __table_args__ = {"schema": "perfil_riesgo"}

    seq = Sequence('perfil_de_riesgo_id_seq')
    id_perfil_de_riesgo = Column('id_perfil_de_riesgo', Integer, seq, server_default=seq.next_value(), primary_key=True)
    numero_solicitud = Column('numero_solicitud', Text)
    justificacion = Column('justificacion', Text)
    ruta_archivo_justificacion = Column('ruta_archivo_justificacion', Text)
    nombre_archivo_justificacion = Column('nombre_archivo_justificacion', Text)
    ruc_solicitado = Column('ruc_solicitado', Text)
    razon_social_solicitado = Column('razon_social_solicitado', Text)
    ruc_fecha_inicio_actividades = Column('ruc_fecha_inicio_actividades', Date)
    created_at = Column('created_at', Date)
    updated_at = Column('updated_at', Date)
    id_estado_solicitud = Column(Integer, ForeignKey(Catalogo.id_catalogo), nullable=False)
    estado_solicitud = relationship(Catalogo, foreign_keys=[id_estado_solicitud])
    id_usuario = Column(Integer, ForeignKey(Usuario.id_usuario), nullable=False)
    usuario = relationship(Usuario, foreign_keys=[id_usuario])    
    estado = Column('estado', Boolean)

    def __init__(self):
        pass

Base.prepare(autoload_with=db_local)
session = Session(db_local)