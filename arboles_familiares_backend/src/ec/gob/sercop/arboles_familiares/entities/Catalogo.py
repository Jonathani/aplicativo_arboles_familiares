
from sqlalchemy.ext.automap import automap_base
from sqlalchemy import Column, String, Integer, Date, Float, Boolean, Numeric, Text, DateTime, Sequence
from sqlalchemy.orm import Session
from src.ec.gob.sercop.arboles_familiares.utils.Conexion import db_local

Base = automap_base()

class Catalogo(Base):
    __tablename__ = "catalogo"
    __table_args__ = {"schema": "sistema"}

    seq = Sequence('catalogo_id_seq')
    id_catalogo = Column('id_catalogo', Integer, seq, server_default=seq.next_value(), primary_key=True)
    descripcion = Column('descripcion', Text)
    id_catalogo_padre = Column('id_catalogo_padre', Numeric)
    created_at = Column('created_at', DateTime)
    updated_at = Column('updated_at', DateTime)
    estado = Column('estado', Boolean)

    def __init__(self):
        pass

Base.prepare(autoload_with=db_local)
session = Session(db_local)