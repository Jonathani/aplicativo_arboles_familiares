
from sqlalchemy.ext.automap import automap_base
from sqlalchemy import Column, String, Integer, Date, Float, Boolean, Numeric, Text, Sequence
from sqlalchemy.orm import Session
from src.ec.gob.sercop.arboles_familiares.utils.Conexion import db_local

Base = automap_base()

class Usuario(Base):
    __tablename__ = "usuario"
    __table_args__ = {"schema": "arboles_familiares"}

    seq = Sequence('usuario_id_seq')
    id_usuario = Column('id_usuario', Integer, seq, server_default=seq.next_value(), primary_key=True)
    identificacion = Column('identificacion', String(10))
    nombre_completo = Column('nombre_completo', String(128))
    estado = Column('estado', Boolean)
    fecha_creacion = Column('fecha_creacion', Date)
    fecha_actualizacion = Column('fecha_actualizacion', Date)
    contrasenia = Column('contrasenia', Text)
    correo = Column('correo', String(100))
    estado_contrasenia = Column('estado_contrasenia', Boolean)

    def __init__(self):
        pass

Base.prepare(autoload_with=db_local)
session = Session(db_local)