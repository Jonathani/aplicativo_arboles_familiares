from sqlalchemy.ext.automap import automap_base
from sqlalchemy import Column, String, Integer, Boolean, Sequence
from sqlalchemy.orm import Session
from src.ec.gob.sercop.arboles_familiares.utils.Conexion import db_local

Base = automap_base()

class Perfil(Base):

    __tablename__ = "perfil"
    __table_args__ = {"schema": "arboles_familiares"}
    
    seq = Sequence('perfil_id_seq')
    id_perfil = Column('id_perfil', Integer, seq, server_default=seq.next_value(), primary_key=True)
    descripcion = Column('descripcion', String(30))
    estado = Column('estado', Boolean)

    def __init__(self):
        pass

Base.prepare(autoload_with=db_local)
session = Session(db_local)