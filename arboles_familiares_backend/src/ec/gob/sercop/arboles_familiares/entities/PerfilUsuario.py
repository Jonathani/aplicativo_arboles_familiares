from sqlalchemy.ext.automap import automap_base
from sqlalchemy import Column, Integer, Date, Sequence, ForeignKey
from sqlalchemy.orm import Session, relationship
from src.ec.gob.sercop.arboles_familiares.entities.Usuario import Usuario
from src.ec.gob.sercop.arboles_familiares.entities.Perfil import Perfil
from src.ec.gob.sercop.arboles_familiares.utils.Conexion import db_local

Base = automap_base()

class PerfilUsuario(Base):

    __tablename__ = "perfil_usuario"
    __table_args__ = {"schema": "arboles_familiares"}

    seq = Sequence('perfil_usuario_id_seq')
    id_perfil_usuario = Column('id_perfil_usuario', Integer, seq, server_default=seq.next_value(), primary_key=True)
    id_usuario = Column(Integer, ForeignKey(Usuario.id_usuario), nullable=False)
    usuario = relationship(Usuario, foreign_keys=[id_usuario])
    id_perfil = Column(Integer, ForeignKey(Perfil.id_perfil), nullable=False)
    perfil = relationship(Perfil, foreign_keys=[id_perfil])
    fecha_asignacion = Column('fecha_asignacion', Date)

    def __init__(self):
        pass

Base.prepare(autoload_with=db_local)
session = Session(db_local)