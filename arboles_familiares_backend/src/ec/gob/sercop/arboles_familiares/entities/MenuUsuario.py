from sqlalchemy.ext.automap import automap_base
from sqlalchemy import Column, Integer, Date, Boolean, Sequence, ForeignKey
from sqlalchemy.orm import Session, relationship
from src.ec.gob.sercop.arboles_familiares.entities.Usuario import Usuario
from src.ec.gob.sercop.arboles_familiares.entities.Menu import Menu
from src.ec.gob.sercop.arboles_familiares.utils.Conexion import db_local

Base = automap_base()

class MenuUsuario(Base):

    __tablename__ = "menu_usuario"
    __table_args__ = {"schema": "arboles_familiares"}

    seq = Sequence('menu_usuario_id_seq')
    id_menu_usuario = Column('id_menu_usuario', Integer, seq, server_default=seq.next_value(), primary_key=True)
    id_menu = Column(Integer, ForeignKey(Menu.id_menu), nullable=False)
    menu = relationship(Menu, foreign_keys=[id_menu])
    id_usuario = Column(Integer, ForeignKey(Usuario.id_usuario), nullable=False)
    usuario = relationship(Usuario, foreign_keys=[id_usuario])
    created_at = Column('created_at', Date)
    update_at = Column('update_at', Date)
    estado = Column('estado', Boolean)

    def __init__(self):
        pass

Base.prepare(autoload_with=db_local)
session = Session(db_local)