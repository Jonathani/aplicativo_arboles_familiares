
from sqlalchemy.ext.automap import automap_base
from sqlalchemy import Column, Integer, Date, Boolean, Text, Sequence, ForeignKey
from sqlalchemy.orm import Session, relationship
from src.ec.gob.sercop.arboles_familiares.utils.Conexion import db_local
from src.ec.gob.sercop.arboles_familiares.entities.PerfilRiesgo import PerfilRiesgo

Base = automap_base()

class RutaArchivos(Base):
    __tablename__ = "ruta_archivos"
    __table_args__ = {"schema": "perfil_riesgo"}

    seq = Sequence('ruta_archivos_id_seq')
    id_ruta_archivos = Column('id_ruta_archivos', Integer, seq, server_default=seq.next_value(), primary_key=True)
    nombre_archivo = Column('nombre_archivo', Text)
    ruta_archivo = Column('ruta_archivo', Text)
    created_at = Column('created_at', Date)
    updated_at = Column('updated_at', Date)
    estado = Column('estado', Boolean)
    id_perfil_de_riesgo = Column(Integer, ForeignKey(PerfilRiesgo.id_perfil_de_riesgo), nullable=False)
    perfil_de_riesgo = relationship(PerfilRiesgo, foreign_keys=[id_perfil_de_riesgo])

    def __init__(self):
        pass

Base.prepare(autoload_with=db_local)
session = Session(db_local)