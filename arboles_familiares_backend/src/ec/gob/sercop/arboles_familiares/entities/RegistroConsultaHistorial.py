from sqlalchemy.ext.automap import automap_base
from sqlalchemy import Column, String, Integer, Date, Text, Sequence, ForeignKey
from sqlalchemy.orm import Session, relationship
from src.ec.gob.sercop.arboles_familiares.entities.Usuario import Usuario
from src.ec.gob.sercop.arboles_familiares.utils.Conexion import db_local

Base = automap_base()

class RegistroConsultaHistorial(Base):

    __tablename__ = "registro_historial_laboral"
    __table_args__ = {"schema": "arboles_familiares"}

    seq = Sequence('registro_historial_laboral_id_seq')
    id_registro_historial_laboral = Column('id_registro_historial_laboral', Integer, seq, server_default=seq.next_value(), primary_key=True)
    id_usuario = Column(Integer, ForeignKey(Usuario.id_usuario), nullable=False)
    usuario = relationship(Usuario, foreign_keys=[id_usuario])
    cedulas_consultadas = Column('cedulas_consultadas', Text)
    motivo_consulta = Column('motivo_consulta', Text)
    fecha_consulta = Column('fecha_consulta', Date)
    path_archivo_respaldo = Column('path_archivo_respaldo', Text)
    nombre_archivo_respaldo = Column('nombre_archivo_respaldo', Text)
    respuesta_cedulas_consultadas = Column('respuesta_cedulas_consultadas', Text)

    def __init__(self):
        pass

Base.prepare(autoload_with=db_local)
session = Session(db_local)