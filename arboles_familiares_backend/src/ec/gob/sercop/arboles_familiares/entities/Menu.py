from sqlalchemy.ext.automap import automap_base
from sqlalchemy import Column, Integer, String, Boolean, Sequence, ForeignKey
from sqlalchemy.orm import Session, relationship
from src.ec.gob.sercop.arboles_familiares.utils.Conexion import db_local

Base = automap_base()

class Menu(Base):

    __tablename__ = "menu"
    __table_args__ = {"schema": "arboles_familiares"}

    seq = Sequence('menu_id_seq')
    id_menu = Column('id_menu', Integer, seq, server_default=seq.next_value(), primary_key=True)
    nombre_label = Column('nombre_label', String(30))
    nombre_icono_principal = Column('nombre_icono_principal', String(30))
    nombre_icono_true = Column('nombre_icono_true', String(30))
    nombre_icono_false = Column('nombre_icono_false', String(30))
    nombre_metodo_principal = Column('nombre_metodo_principal', String(30))
    nombre_metodo_secundario = Column('nombre_metodo_secundario', String(30))
    id_menu_padre = Column('id_menu_padre', Integer)
    # menu = relationship(Menu, foreign_keys=[id_menu])
    estado = Column('estado', Boolean)

    def __init__(self):
        pass

Base.prepare(autoload_with=db_local)
session = Session(db_local)