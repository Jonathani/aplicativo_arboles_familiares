from src.ec.gob.sercop.arboles_familiares.services.GestionConsultasServices import GestionConsultasServices
from src.ec.gob.sercop.arboles_familiares.services.AuthGuard import auth_guard
from flask import jsonify, request
from flask_cors import cross_origin
import os
from pathlib import Path

def init(app):
    
    @app.route('/api/arbolesFamiliares/registrarConsulta', methods=['POST'])
    @auth_guard()
    @cross_origin(supports_credentials=True)
    def registrar_consulta():
        id_usuario = request.form.get('idUsuario')
        cedulas_consultadas = request.form.get('cedulasConsultadas')
        motivo_consulta = request.form.get('motivoConsulta')
        archivo_respaldo = request.files.get('archivoRespaldo')
        respuesta_cedulas_consultadas = request.form.get('respuestaCedulasConsultadas')

        try:
            if (id_usuario and cedulas_consultadas and motivo_consulta and archivo_respaldo and respuesta_cedulas_consultadas):
                if (os.path.exists(app.config['UPLOAD_FOLDER'])):
                    GestionConsultasServices.registrar_consulta(id_usuario, cedulas_consultadas, motivo_consulta, archivo_respaldo, app.config['UPLOAD_FOLDER'], respuesta_cedulas_consultadas)
                else:
                    path = Path(app.config['UPLOAD_FOLDER'])
                    path.mkdir(parents=True)
                    GestionConsultasServices.registrar_consulta(id_usuario, cedulas_consultadas, motivo_consulta, archivo_respaldo, app.config['UPLOAD_FOLDER'], respuesta_cedulas_consultadas)

                mensaje = "Consulta Registrada"
                estado = True
                respuesta = {"status": estado, "mensaje": mensaje}
                response = 200
            else:
                mensaje = "Estimado Usuario, todos los valores deben ser ingresados para registrar un nuevo usuario"
                estado = False
                respuesta = {"status": estado, "mensaje": mensaje}
                response = 500

        except Exception as e:
            print(e)
            respuesta = {"respuesta": e}
            response = 500

        return jsonify(respuesta), response
    
    @app.route('/api/arbolesFamiliares/mostrarConsulta', methods=['POST'])
    @auth_guard()
    @cross_origin(supports_credentials=True)
    def mostrar_consulta():
        listar_informacion_consultada = []
        try:
            id_usuario = request.json["idUsuario"]
            id_perfil = request.json["idPerfil"]
            if (id_usuario and id_perfil):
                info_consulta = GestionConsultasServices.mostrar_consultas(id_usuario, id_perfil)
                if (info_consulta):
                    for row in info_consulta:
                        dato_perfil = {
                            "idConsulta": row.id_consulta,
                            "idUsuario": row.id_usuario,
                            "identificacion": row.identificacion,
                            "nombreCompleto": row.nombre_completo,
                            "cedulasConsultadas": row.cedulas_consultadas,
                            "motivoConsulta": row.motivo_consulta,
                            "fechaConsulta": row.fecha_consulta.strftime("%Y-%m-%d %H:%M:%S"),
                            "pathArchivoRespaldo": row.path_archivo_respaldo,
                            "nombreArchivoRespaldo": row.nombre_archivo_respaldo,
                            "respuestaCedulasConsultadas": row.respuesta_cedulas_consultadas
                        }
                        listar_informacion_consultada.append(dato_perfil)
                
                mensaje = f"Correcto"
                estado = True
                respuesta = {"status": estado, "mensaje": listar_informacion_consultada}
                response = 200
            else:
                mensaje = "Estimado Usuario, todos los valores deben ser ingresados para registrar un nuevo usuario"
                estado = False
                respuesta = {"status": estado, "mensaje": mensaje}
                response = 500
        except Exception as e:
            print(e)
            respuesta = {"respuesta": e}
            response = 500

        return jsonify(respuesta), response
    
    @app.route('/api/arbolesFamiliares/mostrarConsultaDet', methods=['POST'])
    @auth_guard()
    @cross_origin(supports_credentials=True)
    def mostrar_consulta_respuesta():
        try:
            id_consulta = request.json["idConsulta"]
            if (id_consulta):
                info_consulta = GestionConsultasServices.mostrar_consultas_respueta(id_consulta)
                dato_info = {
                    "cedulasConsultadas": info_consulta.cedulas_consultadas,
                    "motivoConsulta": info_consulta.motivo_consulta,
                    "pathArchivoRespaldo": info_consulta.path_archivo_respaldo,
                    "nombreArchivoRespaldo": info_consulta.nombre_archivo_respaldo,
                    "respuestaCedulasConsultadas": info_consulta.respuesta_cedulas_consultadas
                }
                mensaje = f"Correcto"
                estado = True
                respuesta = {"status": estado, "mensaje": dato_info}
                response = 200
            else:
                mensaje = "Estimado Usuario, todos los valores deben ser ingresados para registrar un nuevo usuario"
                estado = False
                respuesta = {"status": estado, "mensaje": mensaje}
                response = 500
        except Exception as e:
            respuesta = {"respuesta": e}
            response = 500
        return jsonify(respuesta), response