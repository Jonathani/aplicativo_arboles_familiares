from src.ec.gob.sercop.arboles_familiares.services.GestionHistorialLaboralServices import GestionHistorialLaboralServices
from src.ec.gob.sercop.arboles_familiares.services.AuthGuard import auth_guard
from flask import jsonify, request
from flask_cors import cross_origin
import os
from pathlib import Path

def init(app):
    
    @app.route('/api/historialLaboral/registrarHistorialLaboral', methods=['POST'])
    @auth_guard()
    @cross_origin(supports_credentials=True)
    def registrar_consulta_historial_laboral():
        id_usuario = request.form.get('idUsuario')
        cedulas_consultadas = request.form.get('cedulasConsultadas')
        motivo_consulta = request.form.get('motivoConsulta')
        archivo_respaldo = request.files.get('archivoRespaldo')
        respuesta_cedulas_consultadas = request.form.get('respuestaCedulasConsultadas')
        try:
            if (id_usuario and cedulas_consultadas and motivo_consulta and archivo_respaldo):
                if (not os.path.exists(app.config['UPLOAD_FOLDER'])):
                    path = Path(app.config['UPLOAD_FOLDER'])
                    path.mkdir(parents=True)

                GestionHistorialLaboralServices.registro_historial_laboral(id_usuario, cedulas_consultadas, motivo_consulta, archivo_respaldo, app.config['UPLOAD_FOLDER'], respuesta_cedulas_consultadas)
                mensaje = "Consulta Registrada"
                estado = True
                respuesta = {"status": estado, "mensaje": mensaje}
                response = 200
            else:
                mensaje = "Estimado Usuario, todos los valores deben ser ingresados para registrar un nuevo usuario"
                estado = False
                respuesta = {"status": estado, "mensaje": mensaje}
                response = 500

        except Exception as e:
            print(e)
            respuesta = {"respuesta": e}
            response = 500

        return jsonify(respuesta), response
    

    @app.route('/api/historialLaboral/listarHistorialLaboral', methods=['POST'])
    @auth_guard()
    @cross_origin(supports_credentials=True)
    def listar_consulta_historial_laboral():
        listar_informacion_consultada = []
        id_usuario = request.json['idUsuario']
        id_perfil = request.json["idPerfil"]
        try:
            if (id_usuario and id_perfil):
                info_consulta = GestionHistorialLaboralServices.listar_consultas_historial_laboral(id_usuario, id_perfil)
                if (info_consulta):
                    for row in info_consulta:
                        dato_perfil = {
                            "idConsulta": row.id_registro_historial_laboral,
                            "idUsuario": row.id_usuario,
                            "identificacion": row.identificacion,
                            "nombreCompleto": row.nombre_completo,
                            "cedulasConsultadas": row.cedulas_consultadas,
                            "motivoConsulta": row.motivo_consulta,
                            "fechaConsulta": row.fecha_consulta.strftime("%Y-%m-%d %H:%M:%S"),
                            "pathArchivoRespaldo": row.path_archivo_respaldo,
                            "nombreArchivoRespaldo": row.nombre_archivo_respaldo,
                            "respuestaCedulasConsultadas": row.respuesta_cedulas_consultadas
                        }
                        listar_informacion_consultada.append(dato_perfil)

                mensaje = f"Información Consultada con Éxito"
                estado = True
                respuesta = {"status": estado, "mensaje":mensaje, "informacionConsultada": listar_informacion_consultada}
                response = 200
            else:
                mensaje = "Estimado Usuario, todos los valores deben ser ingresados para registrar un nuevo usuario"
                estado = False
                respuesta = {"status": estado, "mensaje": mensaje}
                response = 500
        except Exception as e:
            print(e)
            respuesta = {"respuesta": e}
            response = 500

        return jsonify(respuesta), response
