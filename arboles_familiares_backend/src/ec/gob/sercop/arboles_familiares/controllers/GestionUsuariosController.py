import logging
from flask import jsonify, request
from src.ec.gob.sercop.arboles_familiares.services.AuthGuard import auth_guard
from src.ec.gob.sercop.arboles_familiares.services.GestionUsuariosServices import GestionUsuariosServices
from src.ec.gob.sercop.arboles_familiares.utils.Utilidades import Utilidades
from src.ec.gob.sercop.arboles_familiares.utils.AlgoritmCrypto import AlgoritmCrypto
from flask_cors import cross_origin


def init(app):

    @app.route('/api/arbolesFamiliares/infoSesionUsuario', methods=['POST'])
    @auth_guard()
    @cross_origin(supports_credentials=True)
    def info_sesion_usuario():
        informacion_de_sesion = []
        try:
            identificacion = request.json["identificacion"]
            contrasenia = request.json["contrasenia"]
            if (identificacion and contrasenia):
                usuario = GestionUsuariosServices.validar_si_existe_usuario(identificacion.strip())
                if (usuario):
                    usuario_sesion = GestionUsuariosServices.recuperar_informacion_usuario(identificacion.strip())
                    pass_user = bytes(f'{usuario_sesion.contrasenia}', 'utf-8')
                    contrasenia_usuario = AlgoritmCrypto.decrypt(pass_user)
                    if (contrasenia_usuario == contrasenia.strip()):
                        info_sesion = GestionUsuariosServices.informacion_de_sesion_usuario(identificacion)
                        if (info_sesion):
                            for row in info_sesion:
                                dato_perfil = {
                                    "idUsuario": row.id_usuario,
                                    "identificacion": row.identificacion,
                                    "nombreCompleto": row.nombre_completo,
                                    "correo": row.correo,
                                    "estadoUsuario": row.estado,
                                    "estadoContrasenia": row.estado_contrasenia,
                                    "perfilId": row.id_perfil,
                                    "perfilDescripcion": row.descripcion
                                }
                                informacion_de_sesion.append(dato_perfil)

                            mensaje = f"Bienvenido {usuario_sesion.nombre_completo}"
                            estado = True
                            respuesta = {"status": estado,"mensaje": informacion_de_sesion}
                            response = 200
                        else:
                            mensaje = f"Estimado Usuario, su usuario esta inactivo o su contraseña con coincide con la ingresada"
                            estado = False
                            respuesta = {"status": estado, "mensaje": mensaje}
                            response = 500
                    else:
                        mensaje = f"Estimado Usuario, su usuario esta inactivo o su contraseña con coincide con la ingresada"
                        estado = False
                        respuesta = {"status": estado, "mensaje": mensaje}
                        response = 500
                else:
                    mensaje = f"Estimado Usuario, debe ingresar su identificación: {identificacion} ingresada no existe"
                    estado = False
                    respuesta = {"status": estado, "mensaje": mensaje}
                    response = 500
            else:
                mensaje = f"Estimado Usuario, debe ingresar su identificación y contraseña para continuar"
                estado = False
                respuesta = {"status": estado, "mensaje": mensaje}
                response = 500

        except Exception as e:
            respuesta = {"respuesta": e}
            response = 500

        return jsonify(respuesta), response
    
    @app.route('/api/arbolesFamiliares/menusPerfilUsuario', methods=['POST'])
    @auth_guard()
    @cross_origin(supports_credentials=True)
    def menus_perfiles_usuario():
        menus_usuario = []
        id_usuario = request.json["idUsuario"]
        try:
            if (id_usuario):
                if (id_usuario != 0 or id_usuario != "0"):
                    menus_usuario = GestionUsuariosServices.informacion_menus_usuario(id_usuario)
                    mensaje = f"Correcto"
                    estado = True
                    respuesta = {"status": estado, "mensaje": mensaje, "menusUsuario": menus_usuario}
                    response = 200
                else:                    
                    mensaje = f"Correcto"
                    estado = True
                    respuesta = {"status": estado, "mensaje": mensaje, "menusUsuario": menus_usuario}
                    response = 200
            else:
                mensaje = f"Estimado Usuario, no existe menus por mostrar"
                estado = False
                respuesta = {"status": estado, "mensaje": mensaje, "menusUsuario": []}
                response = 500

        except Exception as e:
            respuesta = {"respuesta": e}
            response = 500

        return jsonify(respuesta), response





    @app.route('/api/arbolesFamiliares/crearUsuario', methods=['POST'])
    @auth_guard()
    @cross_origin(supports_credentials=True)
    def crear_usuario():
        try:
            identificacion = request.json["identificacion"]
            nombres_completos = request.json["nombresCompletos"]
            correo = request.json["correo"]
            perfil_id = request.json["perfilId"]
            menus_seleccionados = request.json["menusSeleccionados"]
            contrasenia = Utilidades.get_random_string(16)
            mi_contrasenia_encriptada = (AlgoritmCrypto.encrypt(contrasenia)).decode("utf-8")
            if (identificacion and nombres_completos and correo and perfil_id and mi_contrasenia_encriptada):
                if (GestionUsuariosServices.validar_si_existe_usuario(identificacion)):
                    mensaje = f"Estimado Usuario, el usuario con identificación: {identificacion} ya ha sido registrado"
                    estado = False
                    respuesta = {"status": estado, "mensaje": mensaje}
                    response = 500
                else:
                    GestionUsuariosServices.creacion_de_usuario(identificacion, nombres_completos, correo, perfil_id, mi_contrasenia_encriptada, menus_seleccionados)
                    mensaje = "Información registrada con éxito"
                    estado = True
                    respuesta = {"status": estado, "mensaje": mensaje}
                    response = 200
            else:
                mensaje = "Estimado Usuario, todos los valores deben ser ingresados para registrar un nuevo usuario"
                estado = False
                respuesta = {"status": estado, "mensaje": mensaje}
                response = 500

        except Exception as e:
            print(e)
            respuesta = {"respuesta": e}
            response = 500

        return jsonify(respuesta), response

    @app.route('/api/arbolesFamiliares/actualizarInfoUsuario', methods=['POST'])
    @auth_guard()
    @cross_origin(supports_credentials=True)
    def actualizar_info_usuario():
        try:
            print("\"")
            identificacion = request.json["identificacion"]
            estado = request.json["estado"]
            correo = request.json["correo"]
            nombres_completos = request.json["nombresCompletos"]            
            id_perfil = request.json["idPerfil"]
            menus_seleccionados = request.json["menusSeleccionados"]
            if (identificacion and correo and id_perfil and nombres_completos):
                GestionUsuariosServices.actualizar_informacion_usuario(identificacion, estado, correo, nombres_completos, id_perfil, menus_seleccionados)
                mensaje = "Información actualizada con éxito"
                estado = True
                respuesta = {"status": estado, "mensaje": mensaje}
                response = 200
            else:
                mensaje = "Estimado Usuario, todos los valores deben ser ingresados para registrar un nuevo usuario"
                estado = False
                respuesta = {"status": estado, "mensaje": mensaje}
                response = 500

        except Exception as e:
            print(e)
            respuesta = {"respuesta": e}
            response = 500

        return jsonify(respuesta), response

    @app.route('/api/arbolesFamiliares/actualizarContrasenia', methods=['POST'])
    @auth_guard()
    @cross_origin(supports_credentials=True)
    def actualizar_contrasenia():
        identificacion = request.json["identificacion"]
        contrasenia_registrada = request.json["contraseniaRegistrada"]
        contrasenia_nueva = request.json["contraseniaNueva"]
        contrasenia_nueva_repetida = request.json["contraseniaNuevaRepetir"]
        if (contrasenia_registrada and contrasenia_nueva and contrasenia_nueva_repetida):
            if (contrasenia_nueva == contrasenia_nueva_repetida):
                usuario = GestionUsuariosServices.recuperar_informacion_usuario(identificacion)
                pass_user = bytes(f'{usuario.contrasenia}', 'utf-8')
                contrasenia_usuario = AlgoritmCrypto.decrypt(pass_user)
                if (contrasenia_usuario == contrasenia_registrada):
                    mi_contrasenia_encriptada = (AlgoritmCrypto.encrypt(contrasenia_nueva)).decode("utf-8")
                    GestionUsuariosServices.actualizar_contrasenia_usuario(identificacion, mi_contrasenia_encriptada)
                    mensaje = "Estimado Usuario, se ha actualizado al contraseña con exito"
                    estado = True
                    response = 200
                    respuesta = {"status": estado, "mensaje": mensaje}
                else:
                    mensaje = "Estimado Usuario, la contraseña registrada no es la misma con la que esta ingresando"
                    estado = False
                    respuesta = {"status": estado, "mensaje": mensaje}
                    response = 500
            else:
                mensaje = "Estimado Usuario, la contraseña nueva y la repetición de la contraseña deben ser iguales"
                estado = False
                respuesta = {"status": estado, "mensaje": mensaje}
                response = 500
        else:
            mensaje = "Estimado Usuario, todos los valores deben ser ingresados para actualizar la contrasenia"
            estado = False
            response = 500
            respuesta = {"status": estado, "mensaje": mensaje}

        respuesta = {"status": estado, "mensaje": mensaje}
        return jsonify(respuesta), response

    @app.route('/api/arbolesFamiliares/recuperarContrasenia', methods=['POST'])
    @auth_guard()
    @cross_origin(supports_credentials=True)
    def recuperar_contrasenia():
        try:
            identificacion = request.json["identificacion"]
            correo_electronico = request.json["correoElectronico"]
            recuperar_contrasenia = GestionUsuariosServices.recuperar_email(identificacion, correo_electronico)
            if (recuperar_contrasenia):
                contrasenia = Utilidades.get_random_string(16)
                mi_contrasenia_encriptada = (AlgoritmCrypto.encrypt(contrasenia)).decode("utf-8")
                GestionUsuariosServices.actualizar_contrasenia_temporal(identificacion, mi_contrasenia_encriptada)

                mensaje = "Estimado Usuario, se ha enviado un correo con lo solicitado"
                estado = True
                response = 200
                respuesta = {"status": estado, "mensaje": mensaje}
            else:
                mensaje = "Estimado Usuario, la identificación ingresada y la contraseña no coinciden con lo registrado"
                estado = False
                response = 500
                respuesta = {"status": estado, "mensaje": mensaje}
        except Exception as e:
            mensaje = f"Estimado Usuario, ha ocurrido un error, porfavor vuelva intentar luego {e}"
            estado = False
            response = 500
            respuesta = {"status": estado, "mensaje": mensaje}

        return jsonify(respuesta), response

    @app.route('/api/arbolesFamiliares/listarUsuariosSistema', methods=['POST'])
    @auth_guard()
    @cross_origin(supports_credentials=True)
    def listar_usuarios_sistema():
        listar_usuarios_sistema = []
        usuarios_sistema = []
        try:
            usuarios_sistema = GestionUsuariosServices.listar_usuarios_service()
            for row in usuarios_sistema:
                datos_usuario = {
                    "idUsuario": row.id_usuario,
                    "identificacionUsuario": row.identificacion,
                    "nombresUsuario": row.nombre_completo,
                    "estadoUsuario": 'Activo' if (row.estado) else 'Inactivo',
                    "idPerfil": row.id_perfil,
                    "correo": row.correo,
                    "descripcionPerfil": row.descripcion,
                    "idPerfilUsuario": row.id_perfil_usuario
                }
                listar_usuarios_sistema.append(datos_usuario)

            mensaje = f"Correcto"
            estado = True
            respuesta = {"status": estado, "mensaje": listar_usuarios_sistema}
            response = 200
        except Exception as e:
            mensaje = f"Estimado Usuario, ha ocurrido un error, porfavor vuelva intentar luego {e}"
            estado = False
            response = 500
            respuesta = {"status": estado, "mensaje": mensaje}

        return jsonify(respuesta), response

    @app.route('/api/arbolesFamiliares/listarPerfilesSistema', methods=['POST'])
    @auth_guard()
    @cross_origin(supports_credentials=True)
    def listar_perfiles_sistema():
        listar_perfiles_sistema = []
        perfiles_sistema = []
        try:
            perfiles_sistema = GestionUsuariosServices.listar_perfiles_usuario()
            for row in perfiles_sistema:
                datos_usuario = {
                    "idPerfil": row.id_perfil,
                    "perfilDescripcion": row.descripcion,
                    "estadoPerfil": 'Activo' if (row.estado) else 'Inactivo'
                }
                listar_perfiles_sistema.append(datos_usuario)

            mensaje = f"Correcto"
            estado = True
            respuesta = {"status": estado, "mensaje": listar_perfiles_sistema}
            response = 200
        except Exception as e:
            mensaje = f"Estimado Usuario, ha ocurrido un error, porfavor vuelva intentar luego {e}"
            estado = False
            response = 500
            respuesta = {"status": estado, "mensaje": mensaje}

        return jsonify(respuesta), response

    @app.route('/api/arbolesFamiliares/mostrarInfoUsuario', methods=['POST'])
    @auth_guard()
    @cross_origin(supports_credentials=True)
    def mostrar_info_usuario():
        informacion_del_usuario = []
        try:
            id_usuario = request.json["idUsuario"]
            if (id_usuario):
                info_usuario = GestionUsuariosServices.mostrar_info_usuario(id_usuario)
                for row in info_usuario:
                    dato_user = {
                        "idUsuario": row.id_usuario,
                        "identificacion": row.identificacion,
                        "nombreCompleto": row.nombre_completo,
                        "correo": row.correo,
                        "estadoUsuario": row.estado,
                        "estadoContrasenia": row.estado_contrasenia,
                        "perfilId": row.id_perfil,
                        "perfilDescripcion": row.descripcion
                    }
                    informacion_del_usuario.append(dato_user)

                mensaje = "Información actualizada con éxito"
                estado = True
                respuesta = {"status": estado, "mensaje": informacion_del_usuario}
                response = 200
            else:
                mensaje = "Estimado Usuario, todos los valores deben ser ingresados para registrar un nuevo usuario"
                estado = False
                respuesta = {"status": estado, "mensaje": mensaje}
                response = 500

        except Exception as e:
            print(e)
            respuesta = {"respuesta": e}
            response = 500

        return jsonify(respuesta), response
    
    @app.route('/api/arbolesFamiliares/mostrarTodosLosMenus', methods=['POST'])
    @auth_guard()
    @cross_origin(supports_credentials=True)
    def mostrar_todos_los_menus():
        try:
            menus = GestionUsuariosServices.mostrar_los_menus()
            estado = True
            respuesta = {"status": estado, "mensaje": menus}
            response = 200
        except Exception as e:
            print(e)
            respuesta = {"respuesta": e}
            response = 500

        return jsonify(respuesta), response
    
    @app.route('/api/arbolesFamiliares/menusDelUsuario', methods=['POST'])
    @auth_guard()
    @cross_origin(supports_credentials=True)
    def menus_del_usuario():
        id_usuario = request.json["idUsuario"]        
        try:
            menus_usuario = GestionUsuariosServices.informacion_menus_usuario(id_usuario)
            estado = True
            respuesta = {"status": estado, "mensaje": menus_usuario}
            response = 200
        except Exception as e:
            respuesta = {"respuesta": e}
            response = 500

        return jsonify(respuesta), response



