from src.ec.gob.sercop.arboles_familiares.services.AuthGuard import auth_guard
from src.ec.gob.sercop.arboles_familiares.services.GeneracionPerfilDeRiesgoServices import GeneracionPerfilDeRiesgoServices
from flask import jsonify, request
from flask_cors import cross_origin
import os
from pathlib import Path

def init(app):

    @app.route('/api/perfilDeRiesgo/generacionPerfilRiesgo', methods=['POST'])
    @auth_guard()
    @cross_origin(supports_credentials=True)
    def perfil_de_riesgo():
        id_usuario = request.form.get('idUsuario')
        ruc = request.form.get('ruc')
        razon_social = request.form.get('razonSocial')
        fecha_inicio_actividades = request.form.get('fechaInicioActividades')
        archivo_respaldo_perfil_riesgo = request.files.get('archivoRespaldoPerfilRiesgo')
        justificacion = request.form.get('justificacion')
        archivo_justificacion = request.files.get('archivoJustificacion')
        try:
            if (id_usuario.strip() and ruc.strip() and razon_social.strip() and fecha_inicio_actividades.strip() and archivo_respaldo_perfil_riesgo and justificacion.strip() and archivo_justificacion):
                if (not os.path.exists(app.config['UPLOAD_FOLDER_PERFIL_RIESGO'])):
                    path = Path(app.config['UPLOAD_FOLDER_PERFIL_RIESGO'])
                    path.mkdir(parents=True)

                if (not os.path.exists(app.config['UPLOAD_FOLDER'])):
                    path = Path(app.config['UPLOAD_FOLDER'])
                    path.mkdir(parents=True)

                estado_guardado = GeneracionPerfilDeRiesgoServices.registrar_perfil_de_riesgo(ruc.strip(), razon_social.strip(), fecha_inicio_actividades.strip(), id_usuario.strip(), archivo_respaldo_perfil_riesgo, app.config['UPLOAD_FOLDER_PERFIL_RIESGO'], justificacion, archivo_justificacion, app.config['UPLOAD_FOLDER'])
                if (estado_guardado):
                    mensaje = f"Información Registrada con éxito"
                    estado = True
                    respuesta = {"status": estado,"mensaje": mensaje}
                    response = 200
                else:
                    mensaje = f"Estimado Usuario, ha ocurrido un error, porfavor vuelva a intentar de nuevo"
                    estado = False
                    respuesta = {"status": estado, "mensaje": mensaje}
                    response = 500
            else:
                mensaje = f"Estimado Usuario, debe ingresar todos los datos para continuar con el proceso"
                estado = False
                respuesta = {"status": estado, "mensaje": mensaje}
                response = 500

        except Exception as e:
            respuesta = {"respuesta": e}
            response = 500

        return jsonify(respuesta), response
    

    @app.route('/api/perfilDeRiesgo/listarPerfilRiesgo', methods=['POST'])
    @auth_guard()
    @cross_origin(supports_credentials=True)
    def listar_perfil_de_riesgo():
        id_usuario = request.json["idUsuario"]
        id_perfil = request.json["idPerfil"]
        try:
            if(len(id_usuario.strip()) != 0 and len(id_perfil.strip()) != 0):
                perfiles_de_riesgo = GeneracionPerfilDeRiesgoServices.listar_perfil_de_riesgo(id_usuario.strip(), id_perfil.strip())
                mensaje = f"Correcto"
                estado = True
                respuesta = {"status": estado,"mensaje": mensaje, "perfiles": perfiles_de_riesgo}
                response = 200
            else:
                mensaje = f"Estimado Usuario, debe ingresar todos los datos para continuar con el proceso"
                estado = False
                respuesta = {"status": estado, "mensaje": mensaje}
                response = 500

        except Exception as e:
            respuesta = {"respuesta": e}
            response = 500

        return jsonify(respuesta), response
    
    @app.route('/api/perfilDeRiesgo/archivoPerfilRiesgo', methods=['POST'])
    @auth_guard()
    @cross_origin(supports_credentials=True)
    def archivo_perfil_riesgo():
        id_perfil_riesgo = request.json["idPerfilRiesgo"]
        try:
            traer_archivo_servidor = GeneracionPerfilDeRiesgoServices.traer_archivo_perfil_riesgo(id_perfil_riesgo)
            if (traer_archivo_servidor):
                mensaje = f"Archivo copiado con éxito"
                estado = True
                respuesta = {"status": estado,"mensaje": mensaje}
                response = 200
            else:
                mensaje = f"Error al copiar el archivo"
                estado = False
                respuesta = {"status": estado, "mensaje": mensaje}
                response = 500
        except Exception as e:
            respuesta = {"respuesta": e}
            response = 500

        return jsonify(respuesta), response

    





