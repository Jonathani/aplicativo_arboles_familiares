import datetime
from sqlalchemy.orm import sessionmaker
from src.ec.gob.sercop.arboles_familiares.utils.Conexion import db_local
from src.ec.gob.sercop.arboles_familiares.entities.RegistroConsultaHistorial import RegistroConsultaHistorial
from src.ec.gob.sercop.arboles_familiares.entities.Usuario import Usuario
from src.ec.gob.sercop.arboles_familiares.utils.Utilidades import Utilidades


class GestionHistorialLaboralServices():

    @staticmethod
    def registro_historial_laboral(id_usuario, cedulas_consultadas, motivo_consulta, archivo_respaldo, directorio_subida, respuesta_cedulas_consultadas):

        Session = sessionmaker(bind=db_local)
        session = Session()
        session.begin()
        try:
            #Traer el objeto usuario
            usuario = session.query(Usuario).filter(Usuario.id_usuario == id_usuario).first()

            #Persistir sobre la tabla RegistroConsultaHistorial
            registro_consulta_historial = RegistroConsultaHistorial()
            registro_consulta_historial.cedulas_consultadas = cedulas_consultadas
            registro_consulta_historial.motivo_consulta = motivo_consulta
            registro_consulta_historial.fecha_consulta = datetime.datetime.now()
            registro_consulta_historial.usuario = usuario
            registro_consulta_historial.path_archivo_respaldo = directorio_subida 
            registro_consulta_historial.respuesta_cedulas_consultadas = respuesta_cedulas_consultadas
            session.add(registro_consulta_historial)
            session.commit()
            registro_consulta_historial.nombre_archivo_respaldo = Utilidades.subir_archivos_generico(registro_consulta_historial.id_registro_historial_laboral, id_usuario, archivo_respaldo, directorio_subida)
            session.commit()
        except Exception as e:
            session.rollback()
            raise Exception (e)
        finally:
            session.close()

    @staticmethod
    def listar_consultas_historial_laboral(id_usuario, id_perfil):
        Session = sessionmaker(bind=db_local)
        session = Session()
        session.begin()
        try:
            #Perfil Administrador
            if (id_perfil == 1 or id_perfil == "1"):
                informacion_consulta = session.\
                    query(RegistroConsultaHistorial, RegistroConsultaHistorial.id_registro_historial_laboral, Usuario.id_usuario, Usuario.identificacion, Usuario.nombre_completo, RegistroConsultaHistorial.cedulas_consultadas, \
                    RegistroConsultaHistorial.motivo_consulta,RegistroConsultaHistorial.fecha_consulta, RegistroConsultaHistorial.path_archivo_respaldo, RegistroConsultaHistorial.nombre_archivo_respaldo,
                    RegistroConsultaHistorial.respuesta_cedulas_consultadas).\
                    join(Usuario).\
                    order_by(RegistroConsultaHistorial.id_registro_historial_laboral.desc()).\
                    all()
            elif(id_perfil == 2 or id_perfil == "2"):
            #Perfil Analista
                informacion_consulta = session.\
                    query(RegistroConsultaHistorial, RegistroConsultaHistorial.id_registro_historial_laboral, Usuario.id_usuario, Usuario.identificacion, Usuario.nombre_completo, RegistroConsultaHistorial.cedulas_consultadas, \
                    RegistroConsultaHistorial.motivo_consulta,RegistroConsultaHistorial.fecha_consulta, RegistroConsultaHistorial.path_archivo_respaldo, RegistroConsultaHistorial.nombre_archivo_respaldo,
                    RegistroConsultaHistorial.respuesta_cedulas_consultadas).\
                    join(Usuario).\
                    filter(Usuario.id_usuario == id_usuario).\
                    order_by(RegistroConsultaHistorial.id_registro_historial_laboral.desc()).\
                    all()

            return informacion_consulta
        except Exception as e:
            session.rollback()
            raise Exception (e)
        finally:
            session.close()