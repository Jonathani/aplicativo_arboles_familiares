import datetime
from sqlalchemy.orm import sessionmaker
from src.ec.gob.sercop.arboles_familiares.utils.Conexion import db_local
from src.ec.gob.sercop.arboles_familiares.entities.RegistroConsulta import RegistroConsulta
from src.ec.gob.sercop.arboles_familiares.entities.Usuario import Usuario
from src.ec.gob.sercop.arboles_familiares.utils.Utilidades import Utilidades

class GestionConsultasServices():

    @staticmethod
    def registrar_consulta(id_usuario, cedulas_consultadas, motivo_consulta, archivo_respaldo, directorio_subida, respuesta_cedulas_consultadas):

        Session = sessionmaker(bind=db_local)
        session = Session()
        session.begin()
        try:
            #Traer el objeto usuario
            usuario = session.query(Usuario).filter(Usuario.id_usuario == id_usuario).first()

            #Persistir sobre la tabla RegistroConsulta
            registro_consulta = RegistroConsulta()
            registro_consulta.cedulas_consultadas = cedulas_consultadas
            registro_consulta.motivo_consulta = motivo_consulta
            registro_consulta.fecha_consulta = datetime.datetime.now()
            registro_consulta.usuario = usuario
            registro_consulta.path_archivo_respaldo = directorio_subida 
            registro_consulta.respuesta_cedulas_consultadas = respuesta_cedulas_consultadas
            session.add(registro_consulta)
            session.commit()
            registro_consulta.nombre_archivo_respaldo = Utilidades.subir_archivos(registro_consulta, id_usuario, archivo_respaldo, directorio_subida)
            session.commit()
        except Exception as e:
            session.rollback()
            raise Exception (e)
        finally:
            session.close()

    @staticmethod
    def mostrar_consultas(id_usuario, id_perfil):
        usuarios_sistema = []
        Session = sessionmaker(bind=db_local)
        session = Session()
        session.begin()
        try:
            #Perfil Administrador
            if (id_perfil == 1 or id_perfil == "1"):
                usuarios_sistema = session.\
                    query(RegistroConsulta, RegistroConsulta.id_consulta, Usuario.id_usuario, Usuario.identificacion, Usuario.nombre_completo, RegistroConsulta.cedulas_consultadas, \
                    RegistroConsulta.motivo_consulta,RegistroConsulta.fecha_consulta, RegistroConsulta.path_archivo_respaldo, RegistroConsulta.nombre_archivo_respaldo,
                    RegistroConsulta.respuesta_cedulas_consultadas).\
                    join(Usuario).\
                    order_by(RegistroConsulta.id_consulta.desc()).\
                    all()
                return usuarios_sistema
            elif (id_perfil == 2 or id_perfil == "2"):
                #Perfil Analista
                usuarios_sistema = session.\
                    query(RegistroConsulta, RegistroConsulta.id_consulta, Usuario.id_usuario, Usuario.identificacion, Usuario.nombre_completo, RegistroConsulta.cedulas_consultadas, \
                    RegistroConsulta.motivo_consulta,RegistroConsulta.fecha_consulta, RegistroConsulta.path_archivo_respaldo, RegistroConsulta.nombre_archivo_respaldo,
                    RegistroConsulta.respuesta_cedulas_consultadas).\
                    join(Usuario).\
                    filter(Usuario.id_usuario == id_usuario).\
                    order_by(RegistroConsulta.id_consulta.desc()).\
                    all()
            
            return usuarios_sistema
        except Exception as e:
            session.rollback()
            raise Exception (e)
        finally:
            session.close()

    @staticmethod
    def mostrar_consultas_respueta(id_consulta):
        Session = sessionmaker(bind=db_local)
        session = Session()
        session.begin()
        try:
            registro_consulta = session.query(RegistroConsulta).filter(RegistroConsulta.id_consulta == id_consulta).first()
            return registro_consulta
        except Exception as e:
            print(e)
            session.rollback()
            raise Exception(f"Ha ocurrido un error, vuelva a intentar o comuníquese con el Administrador {e}")
        finally:
            session.close()