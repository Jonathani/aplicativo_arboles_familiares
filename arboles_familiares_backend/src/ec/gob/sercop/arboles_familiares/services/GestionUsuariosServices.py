import logging
import datetime
from sqlalchemy.orm import sessionmaker
from src.ec.gob.sercop.arboles_familiares.utils.Conexion import db_local
from src.ec.gob.sercop.arboles_familiares.entities.Usuario import Usuario
from src.ec.gob.sercop.arboles_familiares.entities.Perfil import Perfil
from src.ec.gob.sercop.arboles_familiares.entities.Menu import Menu
from src.ec.gob.sercop.arboles_familiares.entities.MenuUsuario import MenuUsuario
from src.ec.gob.sercop.arboles_familiares.entities.PerfilUsuario import PerfilUsuario
from src.ec.gob.sercop.arboles_familiares.utils.Variables import Variables
from src.ec.gob.sercop.arboles_familiares.utils.AlgoritmCrypto import AlgoritmCrypto
from src.ec.gob.sercop.arboles_familiares.utils.Utilidades import Utilidades

class GestionUsuariosServices():

    @staticmethod
    def informacion_de_sesion_usuario(identificacion):
        Session = sessionmaker(bind=db_local)
        session = Session()
        session.begin()
        try:
            usuarios_sistema = session.\
                query(PerfilUsuario, Usuario.id_usuario, Usuario.identificacion, Usuario.nombre_completo, Usuario.estado, Usuario.estado_contrasenia,  \
                    Perfil.descripcion, Usuario.correo, Perfil.id_perfil).\
                join(Usuario).\
                join(Perfil).\
                filter(Perfil.estado == True).\
                filter(Usuario.identificacion == identificacion).\
                filter(Usuario.estado == True).\
                all()
            return usuarios_sistema
        except Exception as e:
            raise Exception (e)
        finally:
            session.close()

    @staticmethod
    def informacion_menus_usuario(id_usuario):
        Session = sessionmaker(bind=db_local)
        session = Session()
        session.begin()
        menu_principal = []
        menu_secundario = []
        try:
            menus_principal = session.query(MenuUsuario, MenuUsuario.id_menu_usuario, Menu.id_menu, Menu.nombre_label, Menu.nombre_metodo_principal, Menu.nombre_metodo_secundario,
                Menu.nombre_icono_true, Menu.nombre_icono_false, Menu.nombre_icono_principal).\
                join(Menu).\
                join(Usuario).\
                filter(Menu.id_menu_padre == None).\
                filter(Usuario.id_usuario == id_usuario).\
                filter(MenuUsuario.estado == True).\
                order_by(MenuUsuario.id_menu_usuario).\
                all()
            
            ##Generacion del menu principal
            for row_principal in menus_principal:
                menus_secundario = session.query(MenuUsuario, MenuUsuario.id_menu_usuario, Menu.id_menu, Menu.nombre_label, Menu.nombre_metodo_principal, Menu.nombre_metodo_secundario,
                    Menu.nombre_icono_true, Menu.nombre_icono_false, Menu.nombre_icono_principal).\
                    join(Menu).\
                    join(Usuario).\
                    filter(Menu.id_menu_padre == row_principal.id_menu).\
                    filter(Usuario.id_usuario == id_usuario).\
                    filter(MenuUsuario.estado == True).\
                    order_by(MenuUsuario.id_menu_usuario).\
                    all()
                ##Generacion del menu secundario
                for row_secundario in menus_secundario:
                    menu_s = {
                        "idMenuUsuario": row_secundario.id_menu_usuario,
                        "idMenu": row_secundario.id_menu,
                        "nombreLabel": row_secundario.nombre_label,
                        "nombreMetodoPrincipal": row_secundario.nombre_metodo_principal,
                        "nombreMetodoSecundario": row_secundario.nombre_metodo_secundario,
                        "nombreIconoPrincipal": row_secundario.nombre_icono_principal,
                        "nombreIconoTrue": row_secundario.nombre_icono_true,
                        "nombreIconoFalse": row_secundario.nombre_icono_false
                    }
                    menu_secundario.append(menu_s)

                menu_p = {
                    "idMenuUsuario": row_principal.id_menu_usuario,
                    "idMenu": row_principal.id_menu,
                    "nombreLabel": row_principal.nombre_label,
                    "nombreMetodoPrincipal": row_principal.nombre_metodo_principal,
                    "nombreMetodoSecundario": row_principal.nombre_metodo_secundario,
                    "nombreIconoPrincipal": row_principal.nombre_icono_principal,
                    "nombreIconoTrue": row_principal.nombre_icono_true,
                    "nombreIconoFalse": row_principal.nombre_icono_false,
                    "menusHijos": menu_secundario
                }                
                menu_secundario = []
                menu_principal.append(menu_p)               
            
            return menu_principal
        except Exception as e:
            raise Exception (e)
        finally:
            session.close()

    @staticmethod
    def mostrar_los_menus():
        mostrar_menus_padres = []
        mostrar_menus_hijos = []
        Session = sessionmaker(bind=db_local)
        session = Session()
        session.begin()
        try:
            menus_padre = session.query(Menu).\
                filter(Menu.estado == True).\
                filter(Menu.id_menu_padre == None).\
                order_by(Menu.id_menu).\
                all()
            for row_padre in menus_padre:
                menus_hijo = session.query(Menu).\
                    filter(Menu.estado == True).\
                    filter(Menu.id_menu_padre == row_padre.id_menu).\
                    order_by(Menu.id_menu).\
                    all()
                for row_hijo in menus_hijo:
                    menu_hijo = {
                        "key": row_hijo.id_menu,
                        "label": row_hijo.nombre_label,
                        "data": row_hijo.nombre_label,
                        "icon": "",
                        "children": []
                    }
                    mostrar_menus_hijos.append(menu_hijo)
                
                menu_padre = {
                    "key": row_padre.id_menu,
                    "label": row_padre.nombre_label,
                    "data": row_padre.nombre_label,
                    "icon": "",
                    "children": mostrar_menus_hijos
                }
                mostrar_menus_hijos = []
                mostrar_menus_padres.append(menu_padre)
        except Exception as e:
            raise Exception (e)
        finally:
            session.close()
        
        return mostrar_menus_padres

    @staticmethod
    def creacion_de_usuario(identificacion_usuario, nombres_completos_usuario, correo, perfil_id, contrasenia, menus_seleccionados):
        logger = logging.getLogger('informes_log')
        logger.setLevel(logging.DEBUG)
        fh = logging.FileHandler('debugcontrol.log')
        fh.setLevel(logging.DEBUG)
        logger.addHandler(fh)

        Session = sessionmaker(bind=db_local)
        session = Session()
        session.begin()
        try:
            #Persistir sobre la tabla usuario
            usuario_creacion = Usuario()
            usuario_creacion.identificacion = identificacion_usuario
            usuario_creacion.nombre_completo = nombres_completos_usuario
            usuario_creacion.correo = correo
            usuario_creacion.contrasenia = contrasenia
            usuario_creacion.fecha_creacion = datetime.datetime.now()
            usuario_creacion.estado_contrasenia = False
            usuario_creacion.estado = True
            session.add(usuario_creacion)

            #Traer el objeto perfil
            #Se puede usar como consulta o como actualizacion
            perfil_consulta = session.query(Perfil).filter(Perfil.id_perfil == perfil_id).first()

            #Persistir sobre la tabla PerfilUsuario
            perfil_usuario = PerfilUsuario()
            perfil_usuario.usuario = usuario_creacion
            perfil_usuario.perfil = perfil_consulta
            perfil_usuario.fecha_asignacion = datetime.datetime.now()
            session.add(perfil_usuario)

            #Persistir Menus
            menus_usuario = menus_seleccionados.split(",")
            for row in menus_usuario:
                menu = session.query(Menu).filter(Menu.id_menu == row).first()
                menu_usuario = MenuUsuario()
                menu_usuario.menu = menu
                menu_usuario.usuario = usuario_creacion
                menu_usuario.created_at = datetime.datetime.now()
                menu_usuario.estado = True
                session.add(menu_usuario)

            session.commit()
            #Envio de correo de confirmación de usuario
            GestionUsuariosServices.envio_de_correo_usuario_creado(identificacion_usuario)
        except Exception as e:
            session.rollback()
            raise Exception (e)
        finally:
            session.close()

    @staticmethod
    def validar_si_existe_usuario(identificacion):
        Session = sessionmaker(bind=db_local)
        session = Session()
        session.begin()
        try:
            usuario = session.query(Usuario).filter(Usuario.identificacion == identificacion).first()
            return True if usuario else False
        except Exception as e:
            print(e)
            session.rollback()
            raise Exception(f"Ha ocurrido un error, vuelva a intentar o comuníquese con el Administrador {e}")
        finally:
            session.close()
    
    @staticmethod
    def recuperar_informacion_usuario(identificacion):
        Session = sessionmaker(bind=db_local)
        session = Session()
        session.begin()
        try:
            usuario = session.query(Usuario).filter(Usuario.identificacion == identificacion).first()
            return usuario
        except Exception as e:
            print(e)
            session.rollback()
            raise Exception(f"Ha ocurrido un error, vuelva a intentar o comuníquese con el Administrador {e}")
        finally:
            session.close()
    
    @staticmethod
    def actualizar_informacion_usuario(identificacion, estado, correo, nombres_completos, id_perfil, menus_seleccionados):
        Session = sessionmaker(bind=db_local)
        session = Session()
        session.begin()
        try:
            usuario = session.query(Usuario).filter(Usuario.identificacion == identificacion).first()
            #usuario.estado = True if estado == "1" else False
            usuario.estado = estado
            usuario.nombre_completo = nombres_completos
            usuario.correo = correo
            usuario.fecha_actualizacion = datetime.datetime.now()
            session.add(usuario)

            #Traer el objeto perfil
            perfil = session.query(Perfil).filter(Perfil.id_perfil == id_perfil).first()

            #Actualizar la tabla perfilUsuario
            perfil_usuario = session.query(PerfilUsuario).filter(PerfilUsuario.id_usuario == usuario.id_usuario).first()
            perfil_usuario.perfil = perfil
            perfil_usuario.fecha_asignacion = datetime.datetime.now()
            session.add(perfil_usuario)

            #Menus Seleccionados
            #Menus Nuevos
            menus_nuevos = []
            menus_usuario = menus_seleccionados.split(",")
            for row in menus_usuario:
                menus_nuevos.append(int(row))

            #Menus Actuales
            menus_actuales = []
            menus_actuales_usuario = GestionUsuariosServices.mostrar_menus_usuario(usuario.id_usuario)
            for row in menus_actuales_usuario:
                menus_actuales.append(int(str(row.id_menu)))

            #Ingresar nuevos menus
            for row_nuevo in menus_nuevos:
                encontrado = row_nuevo in menus_actuales
                if (encontrado == False):
                    menu = session.query(Menu).filter(Menu.id_menu == row_nuevo).first()
                    menu_usuario = MenuUsuario()
                    menu_usuario.menu = menu
                    menu_usuario.usuario = usuario
                    menu_usuario.created_at = datetime.datetime.now()
                    menu_usuario.estado = True
                    session.add(menu_usuario)

            #Deshabilitar menus quitados
            for row_actual in menus_actuales:
                encontrado = row_actual in menus_nuevos
                if (encontrado == False):
                    menu_usuario = session.query(MenuUsuario).\
                        filter(MenuUsuario.id_menu == row_actual).\
                        filter(MenuUsuario.id_usuario == usuario.id_usuario).\
                        first()
                    menu_usuario.estado = False
                    menu_usuario.update_at = datetime.datetime.now()
                    session.add(menu_usuario)

            session.commit()
        except Exception as e:
            session.rollback()
            raise Exception (e)
        finally:
            session.close()

    @staticmethod
    def mostrar_menus_usuario(id_usuario):
        Session = sessionmaker(bind=db_local)
        session = Session()
        session.begin()
        try:
            menus_usuario = session.query(MenuUsuario).\
                filter(MenuUsuario.id_usuario == id_usuario).\
                filter(MenuUsuario.estado == True).\
                all()
            return menus_usuario
        except Exception as e:
            session.rollback()
            raise Exception (e)
        finally:
            session.close()

    @staticmethod
    def actualizar_contrasenia_usuario(identificacion, contrasenia):

        Session = sessionmaker(bind=db_local)
        session = Session()
        session.begin()
        try:
            usuario = session.query(Usuario).filter(Usuario.identificacion == identificacion).first()
            usuario.estado_contrasenia = True
            usuario.contrasenia = contrasenia
            usuario.fecha_actualizacion = datetime.datetime.now()
            session.add(usuario)

            session.commit()
            #Envio de correo de confirmación de usuario
            GestionUsuariosServices.envio_de_correo_actualizar_contrasenia(identificacion)
        except Exception as e:
            session.rollback()
            raise Exception (e)
        finally:
            session.close()

    @staticmethod
    def recuperar_email(identificacion, correo):
        Session = sessionmaker(bind=db_local)
        session = Session()
        session.begin()
        try:
            usuario = session.query(Usuario).filter(Usuario.identificacion == identificacion).\
                filter(Usuario.correo == correo).\
                first()
            return True if usuario else False
        except Exception as e:
            session.rollback()
            raise Exception(f"Ha ocurrido un error, vuelva a intentar o comuníquese con el Administrador {e}")
        finally:
            session.close()
    
    def actualizar_contrasenia_temporal(identificacion, contrasenia):
        Session = sessionmaker(bind=db_local)
        session = Session()
        session.begin()
        try:
            usuario = session.query(Usuario).filter(Usuario.identificacion == identificacion).first()
            usuario.contrasenia = contrasenia
            usuario.fecha_actualizacion = datetime.datetime.now()
            usuario.estado_contrasenia = False
            session.add(usuario)

            session.commit()
            #Envio de correo de confirmación de usuario
            GestionUsuariosServices.envio_de_correo_actualizar_contrasenia_temporal(identificacion)
        except Exception as e:
            print(e)
            session.rollback()
            raise Exception(f"Ha ocurrido un error, vuelva a intentar o comuníquese con el Administrador {e}")
        finally:
            session.close()


    @staticmethod
    def listar_usuarios_service():
        Session = sessionmaker(bind=db_local)
        session = Session()
        session.begin()
        usuarios_sistema = []
        try:
            #usuarios_sistema = session.query(Usuario).all()
            usuarios_sistema = session.\
                query(PerfilUsuario, Usuario.id_usuario, Usuario.identificacion, Usuario.nombre_completo, Usuario.estado, Usuario.estado_contrasenia,  \
                    Perfil.descripcion, Usuario.correo, Perfil.id_perfil, PerfilUsuario.id_perfil_usuario).\
                join(Usuario).\
                join(Perfil).\
                all()
            return usuarios_sistema
        except Exception as e:
            session.rollback()
            raise Exception(f"Ha ocurrido un error, vuelva a intentar o comuníquese con el Administrador {e}")
        finally:
            session.close()
        
    @staticmethod
    def listar_perfiles_usuario():
        Session = sessionmaker(bind=db_local)
        session = Session()
        session.begin()
        perfiles_sistema = []
        try:
            perfiles_sistema = session.query(Perfil).all()
            return perfiles_sistema
        except Exception as e:
            session.rollback()
            raise Exception(f"Ha ocurrido un error, vuelva a intentar o comuníquese con el Administrador {e}")
        finally:
            session.close()


    @staticmethod
    def mostrar_info_usuario(id_usuario):
        Session = sessionmaker(bind=db_local)
        session = Session()
        session.begin()
        try:
            informacion_usuario = session.\
                query(PerfilUsuario, Usuario.id_usuario, Usuario.identificacion, Usuario.nombre_completo, Usuario.estado, Usuario.estado_contrasenia,  \
                    Perfil.descripcion, Usuario.correo, Perfil.id_perfil).\
                join(Usuario).\
                join(Perfil).\
                filter(Usuario.id_usuario == id_usuario).\
                all()
            return informacion_usuario
        except Exception as e:
            raise Exception (e)
        finally:
            session.close()

    @staticmethod
    def envio_de_correo_actualizar_contrasenia_temporal(identificacion):
        Session = sessionmaker(bind=db_local)
        session = Session()
        session.begin()
        try:
            usuario = session.query(Usuario).filter(Usuario.identificacion == identificacion).first()
            receiver_email = usuario.correo
            asunto = "Recuperación de Contraseña"
            nombre_usuario = usuario.nombre_completo
            id_usuario = usuario.identificacion
            pass_user = bytes(f'{usuario.contrasenia}', 'utf-8')
            contrasenia_usuario = AlgoritmCrypto.decrypt(pass_user)
            url_sistema = Variables.URL_SISTEMA
            body_text = f"""
                <html>
                    Estimado/a, <b>{nombre_usuario}</b>
                    <br><br>
                    En relación a la recuperación de su usuario, {nombre_usuario}, es la siguiente:<br> 
                    Usuario: <b>{id_usuario}</b> y <br>
                    La contraseña  <b>{contrasenia_usuario}</b>.
                    <br><br>
                    Por favor ingresar en la siguiente link: {url_sistema} y cambiar su contraseña.
                    <br><br>
                    Atentamente,
                    <br><br>
                    Servicio Nacional de Contratación Pública<br>
                    Plataforma Gubernamental Financiera,Amazonas entre Unión Nacional de Periodistas y Alonso Pereira<br>
                    <br>www.sercop.gob.ec<br>
                </html>
            """
            datos = {
                "correo_a_enviar": receiver_email,
                "mensaje_a_enviar": body_text,
                "asunto": asunto
            }
            Utilidades.enviar_correos(datos)
        except Exception as e:
            session.rollback()
            raise Exception(f"Ha ocurrido un error, vuelva a intentar o comuníquese con el Administrador {e}")
        finally:
            session.close()

    @staticmethod
    def envio_de_correo_usuario_creado(identificacion):
        Session = sessionmaker(bind=db_local)
        session = Session()
        session.begin()
        try:
            usuario = session.query(Usuario).filter(Usuario.identificacion == identificacion).first()
            pass_user = bytes(f'{usuario.contrasenia}', 'utf-8')

            receiver_email = usuario.correo
            asunto = "Creación de Usuario"
            nombre_usuario = usuario.nombre_completo
            id_usuario = usuario.identificacion
            contrasenia_usuario = AlgoritmCrypto.decrypt(pass_user)
            url_sistema = Variables.URL_SISTEMA
            body_text = f"""
                <html>
                    Estimado/a, <b>{nombre_usuario}</b>
                    <br><br>
                    En relación a la creación de su usuario, {nombre_usuario}, se ha procedido con la creación del usuario: <b>{id_usuario}</b> y la contraseña  <b>{contrasenia_usuario}</b>.
                    <br><br>
                    Por favor ingresar en la siguiente link: {url_sistema} y cambiar su contraseña.
                    <br><br>
                    Atentamente,
                    <br><br>
                    Servicio Nacional de Contratación Pública<br>
                    Plataforma Gubernamental Financiera,Amazonas entre Unión Nacional de Periodistas y Alonso Pereira<br>
                    <br>www.sercop.gob.ec<br>
                </html>
            """
            datos = {
                "correo_a_enviar": receiver_email,
                "mensaje_a_enviar": body_text,
                "asunto": asunto
            }
            Utilidades.enviar_correos(datos)
        except Exception as e:
            session.rollback()
            raise Exception(f"Ha ocurrido un error, vuelva a intentar o comuníquese con el Administrador {e}")
        finally:
            session.close()


    @staticmethod
    def envio_de_correo_actualizar_contrasenia(identificacion):
        Session = sessionmaker(bind=db_local)
        session = Session()
        session.begin()
        try:
            usuario = session.query(Usuario).filter(Usuario.identificacion == identificacion).first()
            receiver_email = usuario.correo
            asunto = "Contraseña Actualizada"
            nombre_usuario = usuario.nombre_completo
            url_sistema = Variables.URL_SISTEMA
            body_text = f"""
                <html>
                    Estimado/a, <b>{nombre_usuario}</b>
                    <br><br>
                    En relación al cambio de contraseña. Se ha actualizado la contraseña con éxito.<br>
                    Por favor ingresar en la siguiente link: {url_sistema}.
                    <br><br><br>
                    Atentamente,
                    <br><br>
                    Servicio Nacional de Contratación Pública<br>
                    Plataforma Gubernamental Financiera,Amazonas entre Unión Nacional de Periodistas y Alonso Pereira<br>
                    <br>www.sercop.gob.ec<br>
                </html>
            """
            datos = {
                "correo_a_enviar": receiver_email,
                "mensaje_a_enviar": body_text,
                "asunto": asunto
            }
            Utilidades.enviar_correos(datos)
        except Exception as e:
            session.rollback()
            raise Exception(f"Ha ocurrido un error, vuelva a intentar o comuníquese con el Administrador {e}")
        finally:
            session.close()
        

