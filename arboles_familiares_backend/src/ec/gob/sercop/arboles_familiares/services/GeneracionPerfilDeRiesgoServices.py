
import datetime
from sqlalchemy.orm import sessionmaker
import pysftp
import os
from src.ec.gob.sercop.arboles_familiares.utils.Conexion import db_local
from src.ec.gob.sercop.arboles_familiares.entities.PerfilRiesgo import PerfilRiesgo
from src.ec.gob.sercop.arboles_familiares.entities.Usuario import Usuario
from src.ec.gob.sercop.arboles_familiares.entities.Catalogo import Catalogo
from src.ec.gob.sercop.arboles_familiares.entities.RutaArchivos import RutaArchivos
from src.ec.gob.sercop.arboles_familiares.utils.Variables import Variables
from src.ec.gob.sercop.arboles_familiares.utils.Utilidades import Utilidades

class GeneracionPerfilDeRiesgoServices():

    @staticmethod
    def generar_numero_solicitud(id_solicitud):
        secuencia = str(id_solicitud).rjust(5, "0")
        numero_solicitud = f"SPR-{secuencia}"
        return numero_solicitud

    @staticmethod
    def registrar_perfil_de_riesgo(ruc, razon_social, fecha_inicio_actividades, id_usuario, archivo_respaldo, directorio_subida, justificacion, archivo_justificacion, directorio_archivo_justificacion):
        Session = sessionmaker(bind=db_local)
        session = Session()
        session.begin()

        estado_guardado = False
        try:
            #Traer el objeto usuario
            usuario = session.query(Usuario).filter(Usuario.id_usuario == id_usuario).first()

            #Traer el objeto catalogo
            estado_perfil_riesgo = session.query(Catalogo).filter(Catalogo.id_catalogo == Variables.ESTADO_REGISTRADO_PERFIL_RIESGO).first()

            #Persistir sobre el objeto perfil riesgo
            perfil_riesgo = PerfilRiesgo()
            perfil_riesgo.ruc_solicitado = ruc
            perfil_riesgo.razon_social_solicitado = razon_social
            perfil_riesgo.ruc_fecha_inicio_actividades = datetime.datetime.strptime(fecha_inicio_actividades, '%Y-%m-%d')
            perfil_riesgo.created_at = datetime.datetime.now()
            perfil_riesgo.usuario = usuario
            perfil_riesgo.justificacion = justificacion
            perfil_riesgo.ruta_archivo_justificacion = directorio_archivo_justificacion
            perfil_riesgo.estado_solicitud = estado_perfil_riesgo
            perfil_riesgo.estado = True            
            session.add(perfil_riesgo)
            session.commit()            
            perfil_riesgo.nombre_archivo_justificacion = Utilidades.subir_archivos_generico(perfil_riesgo.id_perfil_de_riesgo, usuario.id_usuario, archivo_justificacion, directorio_archivo_justificacion)
            session.commit()

            #Persistir sobre el objeto de ruta de archivos
            archivo_de_respaldo = RutaArchivos()
            archivo_de_respaldo.nombre_archivo = Utilidades.subir_archivos_generico(perfil_riesgo.id_perfil_de_riesgo, usuario.id_usuario, archivo_respaldo, directorio_subida)
            archivo_de_respaldo.ruta_archivo = directorio_subida
            archivo_de_respaldo.created_at = datetime.datetime.now()
            archivo_de_respaldo.perfil_de_riesgo = perfil_riesgo
            archivo_de_respaldo.estado = True
            session.add(archivo_de_respaldo)            
            session.commit()

            perfil_riesgo.numero_solicitud = GeneracionPerfilDeRiesgoServices.generar_numero_solicitud(perfil_riesgo.id_perfil_de_riesgo)
            session.add(perfil_riesgo)
            session.commit()

            estado_guardado = True            

        except Exception as e:
            session.rollback()
            raise Exception (e)
        finally:
            session.close()
        
            return estado_guardado
        
    @staticmethod
    def listar_perfil_de_riesgo(id_usuario, id_perfil):
        Session = sessionmaker(bind=db_local)
        session = Session()
        session.begin()
        listar_registro_perfiles = []
        perfiles_totales = []
        try:
            if (int(id_perfil) == Variables.ESTADOS_PERFIL_ADMINISTRADOR):
                listar_registro_perfiles = session.query(PerfilRiesgo).\
                    order_by(PerfilRiesgo.created_at.desc()).\
                    all()
                
            elif (int(id_perfil) == Variables.ESTADOS_PERFIL_ANALISTA):
                listar_registro_perfiles = session.query(PerfilRiesgo).\
                    filter(PerfilRiesgo.id_usuario == id_usuario).\
                    order_by(PerfilRiesgo.created_at.desc()).\
                    all()

            for row in listar_registro_perfiles:
                usuario = session.query(Usuario).filter(Usuario.id_usuario == row.id_usuario).first()
                catalogo = session.query(Catalogo).filter(Catalogo.id_catalogo == row.id_estado_solicitud).first()
                dato_perfil = {
                    "idPerfilRiesgo": row.id_perfil_de_riesgo,
                    "numeroSolicitud": row.numero_solicitud, 
                    "rucSolicitado": row.ruc_solicitado,
                    "razonSocialSolicitado": row.razon_social_solicitado,
                    "rucFechaInicioActividades": row.ruc_fecha_inicio_actividades.strftime("%Y-%m-%d"),
                    "fechaCreacion": row.created_at.strftime("%Y-%m-%d %H:%M:%S"),
                    "fechaActualizacion": row.updated_at.strftime("%Y-%m-%d %H:%M:%S") if row.updated_at else None,
                    "justificacion": row.justificacion,
                    "archivoJustificacion": row.nombre_archivo_justificacion,
                    "pathArchivoJustificacion": row.ruta_archivo_justificacion if row.ruta_archivo_justificacion else None,
                    "estadoSolicitud": catalogo.descripcion,
                    "usuarioCreacion": usuario.nombre_completo
                }
                perfiles_totales.append(dato_perfil)

            return perfiles_totales
        except Exception as e:
            session.rollback()
            raise Exception (e)
        finally:
            session.close()

    
    @staticmethod
    def traer_archivo_perfil_riesgo(id_perfil_riesgo):
        estado = False
        ruta_archivo_remoto = f"/home/riesgos/git_project/perfiles_gen/010_repaldos_ejecucion/{id_perfil_riesgo}/SPR_{id_perfil_riesgo}.pdf"
        ruta_archivo_local = f"/public/archivos_reporte_perfil_riesgo/SPR_{id_perfil_riesgo}.pdf"
        try:
            existe_archivo = os.path.isfile(ruta_archivo_local)
            if (not existe_archivo):
                cnopts = pysftp.CnOpts()
                cnopts.hostkeys = None
                with pysftp.Connection(Variables.SFTP_HOSTNAME,
                        username=Variables.SFTP_USERNAME,
                        password=Variables.SFTP_PASSWORD,
                        cnopts = cnopts
                        ) as sftp:
                    if (sftp.isfile(ruta_archivo_remoto)): ## TRUE
                        sftp.get(ruta_archivo_remoto, f"/public/archivos_reporte_perfil_riesgo/SPR_{id_perfil_riesgo}.pdf")

            estado = True
        except Exception as e:
            estado = False

        return estado