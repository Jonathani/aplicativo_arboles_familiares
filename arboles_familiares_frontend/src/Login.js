import "primereact/resources/themes/lara-light-indigo/theme.css";
import "primeflex/primeflex.css";
import React, { useState, useRef } from 'react';
import { useNavigate } from 'react-router-dom';
import { InputText } from 'primereact/inputtext';
import { Password } from 'primereact/password';
import { Button } from 'primereact/button';
import { Toast } from 'primereact/toast';
import { Dialog } from 'primereact/dialog';
import { classNames } from 'primereact/utils';
import { Controller, useForm } from 'react-hook-form';
import Cookies from 'js-cookie';
import Mensajes from "./gob/ec/sercop/dirRiesgos/utilidades/Mensajes";
import Variables from "./gob/ec/sercop/dirRiesgos/utilidades/Variables";
import ServicioWeb from "./gob/ec/sercop/dirRiesgos/utilidades/ServicioWeb";
import './Login.css'

function Login() {

    //const { navigate } = this.props
    const navigate = useNavigate();
    const toast = useRef(null);
    const [loading, setLoading] = useState(false);
    const [usuarioRecuperarDialog, setUsuarioRecuperarDialog] = useState(false);
    const [identificacionRecuperar, setIdentificacionRecuperar] = useState("");
    const [correoRecuperar, setCorreoRecuperar] = useState("");
    const defaultValues = { identificacion: '', contrasenia: '' };
    const form = useForm({ defaultValues });
    const errors = form.formState.errors;

    const onSubmit = (data) => {
        ; (async () => {
            let token = await ServicioWeb.obtenerToken(Variables.USER_LOGIN_API_BACKEND, Variables.PASSWORD_LOGIN_API_BACKEND, Variables.PETICION_BACKEND_ARBOL_TOKEN);
            let rowDetallado = {
                identificacion: data.identificacion.trim(),
                contrasenia: data.contrasenia.trim(),
            };
            let requestBody = {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                  'origin':'x-request-with',
                  'Access-Control-Allow-Origin': '*',
                  'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                  'Access-Control-Allow-Credentials': 'true',
                  'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                  'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                  Authorization: token,            
                },
                body: JSON.stringify(rowDetallado),
            };
            let rows = await ServicioWeb.obtenerDatosConBody(
                Variables.PETICION_BACKEND_ARBOL_INFO_SESION,
                requestBody
            );
            if (rows.status){
                const expirationTime = new Date(new Date().getTime() + 3000000);
                Cookies.set('infoSesionUsuario', JSON.stringify(rows.mensaje[0]), { expires: expirationTime });
                navigate("/menu", { replace: true });
            }else{
                showToast('error', 'Mensaje', rows.mensaje, 6000);
            }
        })()
    }

    const showToast = (severityValue, summaryValue, detailValue, timeLife) => {
        toast?.current?.show({ severity: severityValue, summary: summaryValue, detail: detailValue, life: timeLife });
    }

    const getFormErrorMessage = (name) => {
        return errors[name] ? <small className="p-error">{errors[name].message}</small> : <small className="p-error">&nbsp;</small>;
    };

    const recuperarContrasenia = () => {
        setUsuarioRecuperarDialog(true);
    }

    const hideDialogRecuperar = () => {
        setUsuarioRecuperarDialog(false);
        setIdentificacionRecuperar("");
        setCorreoRecuperar("");
    }

    const aceptarRecuperarContrasenia = () => {
        let validEmail = /^\w+([.-_+]?\w+)*@\w+([.-]?\w+)*(\.\w{2,10})+$/;
        if (validEmail.test(correoRecuperar.trim())) {
            ; (async () => {
                setLoading(true)
                let token = await ServicioWeb.obtenerToken(Variables.USER_LOGIN_API_BACKEND, Variables.PASSWORD_LOGIN_API_BACKEND, Variables.PETICION_BACKEND_ARBOL_TOKEN);
                let rowDetallado = {
                    identificacion: identificacionRecuperar.trim(),
                    correoElectronico: correoRecuperar.trim()
                }
                let requestBody = {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: token,
                        'origin': 'x-request-with',
                        'Access-Control-Allow-Origin': '*',
                        'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                        'Access-Control-Allow-Credentials': 'true',
                        'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                        'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                    },
                    body: JSON.stringify(rowDetallado)
                }
                let rows = await ServicioWeb.obtenerDatosConBody(
                    Variables.PETICION_BACKEND_ARBOL_RECUPERAR_CONTRASENIA,
                    requestBody
                );
                if (rows.status) {
                    showToast('info', 'Mensaje', rows.mensaje, 6000);
                    setTimeout(function () {
                        setLoading(false)
                        setUsuarioRecuperarDialog(false);
                    }, 3000);
                } else {
                    showToast('info', 'Mensaje', rows.mensaje, 6000);
                    setLoading(false)
                }
            })()
        } else {
            showToast('error', 'Error', Mensajes.MENSAJE_ERROR_RECUPERAR_CONTRASENIA, 6000);
        }
    }

    const usuarioDialogRecuperarFooter = (
        <>
            <Toast ref={toast} />
            <React.Fragment>
                <Button label="Cancelar" icon="pi pi-times" outlined onClick={hideDialogRecuperar} />
                <Button label="Enviar" icon="pi pi-check" onClick={aceptarRecuperarContrasenia} loading={loading} />
            </React.Fragment>
        </>
    );

    return (
        <>
            <Toast ref={toast} />
            <div className="loginCSS">
                <div className="surface-card p-4 shadow-2 border-round">
                    <div className="text-center mb-5">
                        <div className="text-900 text-3xl font-medium mb-2">Login</div>
                    </div>

                    <div>
                        <form onSubmit={form.handleSubmit(onSubmit)} className="flex flex-column gap-2">
                            <Controller
                                name="identificacion"
                                control={form.control}
                                rules={{ required: 'La identificación es obligatoria.' }}
                                render={({ field, fieldState }) => (
                                    <>
                                        <label htmlFor={field.name}>Identificación</label>
                                        <InputText id={field.name} {...field} type="text" keyfilter="int" maxLength={10} placeholder="Identificación" className={classNames({ 'p-invalid': fieldState.error })} />
                                        {getFormErrorMessage(field.name)}
                                    </>
                                )}
                            />
                            <Controller
                                name="contrasenia"
                                control={form.control}
                                rules={{ required: 'La contraseña es obligatoria.' }}
                                render={({ field, fieldState }) => (
                                    <>
                                        <label htmlFor={field.name}>Contraseña</label>
                                        <Password id={field.name} {...field} type="password" maxLength={60} placeholder="Contraseña" className={classNames({ 'p-invalid': fieldState.error })} feedback={false} toggleMask />
                                        {getFormErrorMessage(field.name)}
                                    </>
                                )}
                            />
                            <div className="flex align-items-center justify-content-between mb-6">
                                <div className="flex align-items-center">
                                </div>
                                <a className="font-medium no-underline ml-2 text-blue-500 text-right cursor-pointer" onClick={recuperarContrasenia}>¿Olvido su contraseña?</a>
                            </div>

                            <Button label="Ingresar" icon="pi pi-user" type="submit" className="w-full" />
                        </form>
                    </div>
                </div>
            </div>
            {/* Dialogo recuperar contraseña */}
            <Dialog visible={usuarioRecuperarDialog} style={{ width: '32rem' }}
                breakpoints={{ '960px': '75vw', '641px': '90vw' }}
                header="Recuperar Contraseña" modal className="p-fluid"
                footer={usuarioDialogRecuperarFooter}
                onHide={hideDialogRecuperar}>
                <div className="field">
                    <label htmlFor="identificacion" className="font-bold">
                        Identificación
                    </label>
                    <InputText id="identificacion" type="text" keyfilter="int" maxLength={10} defaultValue={identificacionRecuperar} onChange={(e) => setIdentificacionRecuperar(e.target.value)} required autoFocus />
                </div>
                <div className="field">
                    <label htmlFor="correo" className="font-bold">
                        Correo
                    </label>
                    <InputText id="correo" type="text" maxLength={60} defaultValue={correoRecuperar} onChange={(e) => setCorreoRecuperar(e.target.value)} required />
                </div>
            </Dialog>
        </>
    );
}

export default Login