import { BrowserRouter as Router, Route, Routes, useNavigate } from 'react-router-dom'
import logo from './logo.svg';
import './App.css';
import Menu from './Menu';
import Login from './Login';
import CambiarContrasenia from './gob/ec/sercop/dirRiesgos/administracion/CambiarContrasenia';

function App() {
  
  return (
    <>
      <Router>
        <Routes>
          <Route exact path="/" element={<Login />} />
          <Route exact path="/login" element={<Login />} />
          <Route path="/menu" element={<Menu />} />
          <Route path="/cambiarContrasenia" element={<CambiarContrasenia />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
