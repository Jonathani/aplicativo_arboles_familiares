import React, { useState, useEffect, useRef } from 'react'
import ComisionService from '../service/ComisionService';
import { Tooltip } from 'primereact/tooltip';
import { Column } from 'primereact/column';
import { DataTable } from 'primereact/datatable';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { FilterMatchMode } from 'primereact/api';
import './Comision.css'

function Comision(props) {

    const [comision, setComision] = useState([]);
    const [loading, setLoading] = useState(true);
    const [globalFilterValue, setGlobalFilterValue] = useState('');
    const [filters, setFilters] = useState({
        global: { value: null, matchMode: FilterMatchMode.CONTAINS },
        idComision: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        idSoliCompra: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        cedula: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        nombre: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        cargo: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        funcion: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
    });

    const cargarDatosData = (idVarConsulta) => {
        ComisionService.getComisionMedium(idVarConsulta).then((data) => {
            setComision(getComision(data));
            setLoading(false);
        });
    }

    useEffect(() => {
        cargarDatosData(props.idVarConsulta);
    }, []);

    const getComision = (data) => {
        return [...(data || [])].map((d) => {
            d.date = new Date(d.date);
            return d;
        });
    };

    const dt = useRef(null);

    const onGlobalFilterChange = (e) => {
        const value = e.target.value;
        let _filters = { ...filters };
        _filters['global'].value = value;
        setFilters(_filters);
        setGlobalFilterValue(value);
    };

    const cols = [
        { field: 'idComision', header: 'Id Comision' },
        { field: 'idSoliCompra', header: 'Id SoliCompra' },
        { field: 'cedula', header: 'Cedula' },
        { field: 'nombre', header: 'Nombre' },        
        { field: 'cargo', header: 'Cargo' },
        { field: 'funcion', header: 'Función' },
    ];

    const exportColumns = cols.map((col) => ({ title: col.header, dataKey: col.field }));

    const exportCSV = (selectionOnly) => {
        dt.current.exportCSV({ selectionOnly });
    };

    const exportPdf = () => {
        import('jspdf').then((jsPDF) => {
            import('jspdf-autotable').then(() => {
                const doc = new jsPDF.default(0, 0);

                doc.autoTable(exportColumns, comision);
                doc.save('autoridades.pdf');
            });
        });
    };

    const exportExcel = () => {
        import('xlsx').then((xlsx) => {
            const worksheet = xlsx.utils.json_to_sheet(comision);
            const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
            const excelBuffer = xlsx.write(workbook, {
                bookType: 'xlsx',
                type: 'array'
            });
            saveAsExcelFile(excelBuffer, 'comision');
        });
    };

    const header = (
        <div className="flex justify-content-end">
            <span className="p-input-icon-left">
                <i className="pi pi-search" />
                <InputText style={{fontSize: '10px'}} value={globalFilterValue} onChange={onGlobalFilterChange} placeholder="Buscar" />
            </span>
            <br></br>
            <Button type="button" style={{fontSize: '10px'}} icon="pi pi-file" rounded onClick={() => exportCSV(false)} data-pr-tooltip="CSV" label="CSV" />
            <Button type="button" style={{fontSize: '10px'}} icon="pi pi-file-excel" severity="success" rounded onClick={exportExcel} data-pr-tooltip="XLS" label="XLSX" />
            <Button type="button" style={{fontSize: '10px'}} icon="pi pi-file-pdf" severity="warning" rounded onClick={exportPdf} data-pr-tooltip="PDF" label="PDF" />
        </div>
    );

    const saveAsExcelFile = (buffer, fileName) => {
        import('file-saver').then((module) => {
            if (module && module.default) {
                let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
                let EXCEL_EXTENSION = '.xlsx';
                const data = new Blob([buffer], {
                    type: EXCEL_TYPE
                });
                module.default.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
            }
        });
    };

    return (
        <>
            <div className="tamanio_toolbar">
                <Tooltip target=".export-buttons>button" position="bottom" />
                <DataTable value={comision} paginator rows={10}
                    dataKey="idAutoridades"
                    rowsPerPageOptions={[10, 25, 50]}
                    ref={dt}
                    filters={filters}
                    filterDisplay="row"
                    loading={loading}
                    globalFilterFields={['idSoliCompra', 'cedula', 'nombre', 'cargo', 'funcion']}
                    header={header}
                    emptyMessage="Información no encontrada"
                    paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                    currentPageReportTemplate="Showing {first} to {last} of {totalRecords} entries"
                    tableStyle={{ fontSize: '10px' }}>
                    
                    <Column sortable field="idSoliCompra" header="Id SoliCompra" footer="Id SoliCompra" filter filterPlaceholder="Buscar por Id SoliCompra" />
                    <Column sortable field="cedula" header="Cédula" footer="Cédula" filter filterPlaceholder="Buscar por Cédula" />
                    <Column sortable field="nombre" header="Nombre" footer="Nombre" filter filterPlaceholder="Buscar por Nombre" />
                    <Column sortable field="funcion" header="Función" footer="Función" filter filterPlaceholder="Buscar por Función" />
                    <Column sortable field="cargo" header="Cargo" footer="Cargo" filter filterPlaceholder="Buscar por Cargo" />                    
                </DataTable>
            </div>
        </>
    );
}

export default Comision;