import React, { useState, useEffect, useRef } from 'react'
import DireccionesComercialesSoceService from '../service/DireccionesComercialesSoceService';
import { Tooltip } from 'primereact/tooltip';
import { Column } from 'primereact/column';
import { DataTable } from 'primereact/datatable';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { FilterMatchMode } from 'primereact/api';
import './DireccionesComercialesSoce.css'

function DireccionesComercialesSoce(props) {

    const [direccionesComercialesSoce, setDireccionesComercialesSoce] = useState([]);
    const [loading, setLoading] = useState(true);
    const [globalFilterValue, setGlobalFilterValue] = useState('');
    const [filters, setFilters] = useState({
        global: { value: null, matchMode: FilterMatchMode.CONTAINS },
        idDireccionesComercialSoce: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        cedula: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        razonSocial: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        codPais: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        provincia: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        ciudad: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        parroquia: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        calle: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        interseccion: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        numero: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        edificio: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        departamento: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
    });

    const cargarDatosData = (idVarConsulta) => {
        DireccionesComercialesSoceService.getDireccionesComercialesSoceMedium(idVarConsulta).then((data) => {
            setDireccionesComercialesSoce(getDireccionesComercialesSoce(data));
            setLoading(false);
        });
    }

    useEffect(() => {
        cargarDatosData(props.idVarConsulta);
    }, []);

    const getDireccionesComercialesSoce = (data) => {
        return [...(data || [])].map((d) => {
            d.date = new Date(d.date);
            return d;
        });
    };

    const dt = useRef(null);

    const onGlobalFilterChange = (e) => {
        const value = e.target.value;
        let _filters = { ...filters };
        _filters['global'].value = value;
        setFilters(_filters);
        setGlobalFilterValue(value);
    };

    const cols = [
        { field: 'idDireccionesComercialSoce', header: 'Id Direcciones Comercial Soce' },
        { field: 'cedula', header: 'Cédula' },
        { field: 'razonSocial', header: 'Razon Social' },
        { field: 'codPais', header: 'Código País' },        
        { field: 'provincia', header: 'Provincia' },
        { field: 'ciudad', header: 'Ciudad' },
        { field: 'parroquia', header: 'Parroquia' },
        { field: 'calle', header: 'Calle' },        
        { field: 'interseccion', header: 'Intersección' },
        { field: 'numero', header: 'Número' },
        { field: 'edificio', header: 'Edificio' },
        { field: 'departamento', header: 'Departamento' },
    ];

    const exportColumns = cols.map((col) => ({ title: col.header, dataKey: col.field }));

    const exportCSV = (selectionOnly) => {
        dt.current.exportCSV({ selectionOnly });
    };

    const exportPdf = () => {
        import('jspdf').then((jsPDF) => {
            import('jspdf-autotable').then(() => {
                const doc = new jsPDF.default(0, 0);

                doc.autoTable(exportColumns, direccionesComercialesSoce);
                doc.save('direccionesComercialesSoce.pdf');
            });
        });
    };

    const exportExcel = () => {
        import('xlsx').then((xlsx) => {
            const worksheet = xlsx.utils.json_to_sheet(direccionesComercialesSoce);
            const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
            const excelBuffer = xlsx.write(workbook, {
                bookType: 'xlsx',
                type: 'array'
            });
            saveAsExcelFile(excelBuffer, 'direccionesComercialesSoce');
        });
    };

    const header = (
        <div className="flex justify-content-end">
            <span className="p-input-icon-left">
                <i className="pi pi-search" />
                <InputText style={{fontSize: '10px'}} value={globalFilterValue} onChange={onGlobalFilterChange} placeholder="Buscar" />
            </span>
            <br></br>
            <Button type="button" style={{fontSize: '10px'}} icon="pi pi-file" rounded onClick={() => exportCSV(false)} data-pr-tooltip="CSV" label="CSV" />
            <Button type="button" style={{fontSize: '10px'}} icon="pi pi-file-excel" severity="success" rounded onClick={exportExcel} data-pr-tooltip="XLS" label="XLSX" />
            <Button type="button" style={{fontSize: '10px'}} icon="pi pi-file-pdf" severity="warning" rounded onClick={exportPdf} data-pr-tooltip="PDF" label="PDF" />
        </div>
    );

    const saveAsExcelFile = (buffer, fileName) => {
        import('file-saver').then((module) => {
            if (module && module.default) {
                let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
                let EXCEL_EXTENSION = '.xlsx';
                const data = new Blob([buffer], {
                    type: EXCEL_TYPE
                });
                module.default.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
            }
        });
    };

    return (
        <>
            <div className="tamanio_toolbar">
                <Tooltip target=".export-buttons>button" position="bottom" />
                <DataTable value={direccionesComercialesSoce} paginator rows={10}
                    dataKey="idDireccionesComercialSoce"
                    rowsPerPageOptions={[10, 25, 50]}
                    ref={dt}
                    filters={filters}
                    filterDisplay="row"
                    loading={loading}
                    globalFilterFields={['cedula', 'razonSocial', 'codPais', 'provincia', 'ciudad', 'parroquia', 'calle', 'interseccion', 'numero'
                        , 'edificio', 'departamento']}
                    header={header}
                    emptyMessage="Información no encontrada"
                    paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                    currentPageReportTemplate="Showing {first} to {last} of {totalRecords} entries"
                    tableStyle={{ fontSize: '10px' }}>
                    
                    <Column sortable field="cedula" header="Cédula" footer="Cédula" filter filterPlaceholder="Buscar por Cédula" />
                    <Column sortable field="razonSocial" header="Razon Social" footer="Razon Social" filter filterPlaceholder="Buscar por Razon Social" />
                    <Column sortable field="codPais" header="Código País" footer="Código País" filter filterPlaceholder="Buscar por Código País" />
                    <Column sortable field="provincia" header="Provincia" footer="Provincia" filter filterPlaceholder="Buscar por Provincia" />
                    <Column sortable field="ciudad" header="Ciudad" footer="Ciudad" filter filterPlaceholder="Buscar por Ciudad" />
                    <Column sortable field="parroquia" header="Parroquía" footer="Parroquía" filter filterPlaceholder="Buscar por Parroquía" />
                    <Column sortable field="calle" header="Calle" footer="Calle" filter filterPlaceholder="Buscar por Calle" />
                    <Column sortable field="interseccion" header="Intersección" footer="Intersección" filter filterPlaceholder="Buscar por Intersección" />
                    <Column sortable field="numero" header="Número" footer="Número" filter filterPlaceholder="Buscar por Número" />
                    <Column sortable field="edificio" header="Edificio" footer="Edificio" filter filterPlaceholder="Buscar por Edificio" />
                    <Column sortable field="departamento" header="Departamento" footer="Departamento" filter filterPlaceholder="Buscar por Departamento" />
                </DataTable>
            </div>
        </>
    );
}

export default DireccionesComercialesSoce;