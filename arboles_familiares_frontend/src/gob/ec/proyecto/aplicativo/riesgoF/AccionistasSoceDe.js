import React, { useState, useEffect, useRef } from 'react'
import AccionistasSoceDeService from '../service/AccionistasSoceDeService';
import { Tooltip } from 'primereact/tooltip';
import { Column } from 'primereact/column';
import { DataTable } from 'primereact/datatable';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { FilterMatchMode } from 'primereact/api';
import './AccionistasSoceDe.css'

function AccionistasSoceDe(props) {

    const [accionistasSoceDe, setAccionistasSoceDe] = useState([]);
    const [loading, setLoading] = useState(true);
    const [globalFilterValue, setGlobalFilterValue] = useState('');
    const [filters, setFilters] = useState({
        global: { value: null, matchMode: FilterMatchMode.CONTAINS },
        idAccionistasSoceDe: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        ruc: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        razonSocial: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        accionistrasCedRuc: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        nombreComercial: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        accionistasNombresApellidos: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
    });

    const cargarDatosData = (idVarConsulta) => {
        AccionistasSoceDeService.getAccionistasSoceDeMedium(idVarConsulta).then((data) => {
            setAccionistasSoceDe(getAccionistasSoceDe(data));
            setLoading(false);
        });
    }

    useEffect(() => {
        cargarDatosData(props.idVarConsulta);
    }, []);

    const getAccionistasSoceDe = (data) => {
        return [...(data || [])].map((d) => {
            d.date = new Date(d.date);
            return d;
        });
    };

    const dt = useRef(null);

    const onGlobalFilterChange = (e) => {
        const value = e.target.value;
        let _filters = { ...filters };
        _filters['global'].value = value;
        setFilters(_filters);
        setGlobalFilterValue(value);
    };

    const cols = [
        { field: 'idAccionistasSoceDe', header: 'Id Accionistas Soce De' },
        { field: 'ruc', header: 'Ruc' },
        { field: 'razonSocial', header: 'Razon Social' },
        { field: 'accionistasCedRuc', header: 'Accionistas Ced Ruc' },        
        { field: 'nombreComercial', header: 'Nombre Comercial' },
        { field: 'accionistasNombresApellidos', header: 'Accionistas Nombres Apellidos' },
    ];

    const exportColumns = cols.map((col) => ({ title: col.header, dataKey: col.field }));

    const exportCSV = (selectionOnly) => {
        dt.current.exportCSV({ selectionOnly });
    };

    const exportPdf = () => {
        import('jspdf').then((jsPDF) => {
            import('jspdf-autotable').then(() => {
                const doc = new jsPDF.default(0, 0);

                doc.autoTable(exportColumns, accionistasSoceDe);
                doc.save('accionistasSoceDe.pdf');
            });
        });
    };

    const exportExcel = () => {
        import('xlsx').then((xlsx) => {
            const worksheet = xlsx.utils.json_to_sheet(accionistasSoceDe);
            const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
            const excelBuffer = xlsx.write(workbook, {
                bookType: 'xlsx',
                type: 'array'
            });
            saveAsExcelFile(excelBuffer, 'accionistasSoceDe');
        });
    };

    const header = (
        <div className="flex justify-content-end">
            <span className="p-input-icon-left">
                <i className="pi pi-search" />
                <InputText style={{fontSize: '10px'}} value={globalFilterValue} onChange={onGlobalFilterChange} placeholder="Buscar" />
            </span>
            <br></br>
            <Button style={{fontSize: '10px'}} type="button" icon="pi pi-file" rounded onClick={() => exportCSV(false)} data-pr-tooltip="CSV" label="CSV" />
            <Button style={{fontSize: '10px'}} type="button" icon="pi pi-file-excel" severity="success" rounded onClick={exportExcel} data-pr-tooltip="XLS" label="XLSX" />
            <Button style={{fontSize: '10px'}} type="button" icon="pi pi-file-pdf" severity="warning" rounded onClick={exportPdf} data-pr-tooltip="PDF" label="PDF" />
        </div>
    );

    const saveAsExcelFile = (buffer, fileName) => {
        import('file-saver').then((module) => {
            if (module && module.default) {
                let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
                let EXCEL_EXTENSION = '.xlsx';
                const data = new Blob([buffer], {
                    type: EXCEL_TYPE
                });
                module.default.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
            }
        });
    };

    return (
        <>
            <div className="tamanio_toolbar">
                <Tooltip target=".export-buttons>button" position="bottom" />
                <DataTable value={accionistasSoceDe} paginator rows={10}
                    dataKey="idAccionistasSoceDe"
                    rowsPerPageOptions={[10, 25, 50]}
                    ref={dt}
                    filters={filters}
                    filterDisplay="row"
                    loading={loading}
                    globalFilterFields={['ruc', 'razonSocial', 'accionistasCedRuc', 'nombreComercial', 'accionistasNombresApellidos']}
                    header={header}
                    emptyMessage="Información no encontrada"
                    paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                    currentPageReportTemplate="Showing {first} to {last} of {totalRecords} entries"
                    tableStyle={{ fontSize: '10px' }}>
                    
                    <Column sortable field="ruc" header="Ruc" footer="Ruc" filter filterPlaceholder="Buscar por Ruc" />
                    <Column sortable field="razonSocial" header="Razon Social" footer="Razon Social" filter filterPlaceholder="Buscar por Razon Social" />
                    <Column sortable field="accionistasCedRuc" header="Accionistas Ced Ruc" footer="Accionistas Ced Ruc" filter filterPlaceholder="Buscar por Accionistas Ced Ruc" />
                    <Column sortable field="nombreComercial" header="Nombre Comercial" footer="Nombre Comercial" filter filterPlaceholder="Buscar por Nombre Comercial" />
                    <Column sortablefield="accionistasNombresApellidos" header="Accionistas Nombres Apellidos" footer="Accionistas Nombres Apellidos" filter filterPlaceholder="Buscar por Accionistas Nombres Apellidos" />
                </DataTable>
            </div>
        </>
    );
}

export default AccionistasSoceDe;