import React, { useState, useEffect, useRef } from 'react'
import OfertasService from '../service/OfertasService';
import { Tooltip } from 'primereact/tooltip';
import { Column } from 'primereact/column';
import { DataTable } from 'primereact/datatable';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { FilterMatchMode } from 'primereact/api';
import './Ofertas.css'

function Ofertas(props) {

    const [ofertas, setOfertas] = useState([]);
    const [loading, setLoading] = useState(true);
    const [globalFilterValue, setGlobalFilterValue] = useState('');
    const [filters, setFilters] = useState({
        global: { value: null, matchMode: FilterMatchMode.CONTAINS },
        idOfertas: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        idSoliCompra: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        codigoProcedimiento: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        objectoProceso: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        nombreContratante: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        rucContratante: { value: null, matchMode: FilterMatchMode.STARTS_WITH },       
        tipoProceso: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        fechaPublicacion: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        presupuesto: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        estado: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        fechaPropuesta: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        valorPropuesta: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        idParticipante: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        razonSocialParticipante: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
    });

    const cargarDatosData = (idVarConsulta) => {
        OfertasService.getOfertasMedium(idVarConsulta).then((data) => {
            setOfertas(getOfertas(data));
            setLoading(false);
        });
    }

    useEffect(() => {
        cargarDatosData(props.idVarConsulta);
    }, []);

    const getOfertas = (data) => {
        return [...(data || [])].map((d) => {
            d.date = new Date(d.date);
            return d;
        });
    };

    const dt = useRef(null);

    const onGlobalFilterChange = (e) => {
        const value = e.target.value;
        let _filters = { ...filters };
        _filters['global'].value = value;
        setFilters(_filters);
        setGlobalFilterValue(value);
    };

    const cols = [
        { field: 'idOfertas', header: 'Id Ofertas' },
        { field: 'idSoliCompra', header: 'Id SoliCompra' },
        { field: 'codigoProcedimiento', header: 'Codigo Procedimiento' },
        { field: 'objectoProceso', header: 'Objecto Proceso' },        
        { field: 'nombreContratante', header: 'Nombre Contratante' },
        { field: 'rucContratante', header: 'Ruc Contratante' },
        { field: 'tipoProceso', header: 'Tipo Proceso' },
        { field: 'fechaPublicacion', header: 'Fecha Publicacion' },
        { field: 'presupuesto', header: 'Presupuesto' },
        { field: 'estado', header: 'Estado' },        
        { field: 'fechaPropuesta', header: 'Fecha Propuesta' },
        { field: 'valorPropuesta', header: 'Valor Propuesta' },
        { field: 'idParticipante', header: 'Id Participante' },
        { field: 'razonSocialParticipante', header: 'Razon Social Participante' },
    ];

    const exportColumns = cols.map((col) => ({ title: col.header, dataKey: col.field }));

    const exportCSV = (selectionOnly) => {
        dt.current.exportCSV({ selectionOnly });
    };

    const exportPdf = () => {
        import('jspdf').then((jsPDF) => {
            import('jspdf-autotable').then(() => {
                const doc = new jsPDF.default(0, 0);

                doc.autoTable(exportColumns, ofertas);
                doc.save('ofertas.pdf');
            });
        });
    };

    const exportExcel = () => {
        import('xlsx').then((xlsx) => {
            const worksheet = xlsx.utils.json_to_sheet(ofertas);
            const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
            const excelBuffer = xlsx.write(workbook, {
                bookType: 'xlsx',
                type: 'array'
            });
            saveAsExcelFile(excelBuffer, 'ofertas');
        });
    };

    const header = (
        <div className="flex justify-content-end">
            <span className="p-input-icon-left">
                <i className="pi pi-search" />
                <InputText style={{fontSize: '10px'}} value={globalFilterValue} onChange={onGlobalFilterChange} placeholder="Buscar" />
            </span>
            <br></br>
            <Button style={{fontSize: '10px'}} type="button" icon="pi pi-file" rounded onClick={() => exportCSV(false)} data-pr-tooltip="CSV" label="CSV" />
            <Button style={{fontSize: '10px'}} type="button" icon="pi pi-file-excel" severity="success" rounded onClick={exportExcel} data-pr-tooltip="XLS" label="XLSX" />
            <Button style={{fontSize: '10px'}} type="button" icon="pi pi-file-pdf" severity="warning" rounded onClick={exportPdf} data-pr-tooltip="PDF" label="PDF" />
        </div>
    );

    const saveAsExcelFile = (buffer, fileName) => {
        import('file-saver').then((module) => {
            if (module && module.default) {
                let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
                let EXCEL_EXTENSION = '.xlsx';
                const data = new Blob([buffer], {
                    type: EXCEL_TYPE
                });
                module.default.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
            }
        });
    };

    return (
        <>
            <div className="tamanio_toolbar">
                <Tooltip target=".export-buttons>button" position="bottom" />
                <DataTable value={ofertas} paginator rows={10}
                    dataKey="idOfertas"
                    rowsPerPageOptions={[10, 25, 50]}
                    ref={dt}
                    filters={filters}
                    filterDisplay="row"
                    loading={loading}
                    globalFilterFields={['idSoliCompra', 'codigoProcedimiento', 'objectoProceso', 'nombreContratante', 'rucContratante',
                        'tipoProceso', 'fechaPublicacion', 'presupuesto', 'estado', 'fechaPropuesta' ,'valorPropuesta', 'idParticipante', 'razonSocialParticipante']}
                    header={header}
                    emptyMessage="Información no encontrada"
                    paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                    currentPageReportTemplate="Showing {first} to {last} of {totalRecords} entries"
                    tableStyle={{ fontSize: '10px' }}>
                    
                    <Column sortable field="idSoliCompra" header="Id SoliCompra" footer="Id SoliCompra" filter filterPlaceholder="Buscar por Id SoliCompra" />
                    <Column sortable field="codigoProcedimiento" header="Código Procedimiento" footer="Código Procedimiento" filter filterPlaceholder="Buscar por Código Procedimiento" />
                    <Column sortable field="objectoProceso" header="Objecto Proceso" footer="Objecto Proceso" filter filterPlaceholder="Buscar por Objecto Proceso" />
                    <Column sortable field="nombreContratante" header="Nombre Contratante" footer="Nombre Contratante" filter filterPlaceholder="Buscar por Nombre Contratante" />
                    <Column sortable field="rucContratante" header="Ruc Contratante" footer="Ruc Contratante" filter filterPlaceholder="Buscar por Ruc Contratante" />
                    <Column sortable field="tipoProceso" header="Tipo Proceso" footer="Tipo Proceso" filter filterPlaceholder="Buscar por Tipo Proceso" />
                    <Column sortable field="fechaPublicacion" header="Fecha Publicación" footer="Fecha Publicación" filter filterPlaceholder="Buscar por Fecha Publicación" />
                    <Column sortable field="presupuesto" header="Presupuesto" footer="Presupuesto" filter filterPlaceholder="Buscar por Presupuesto" />
                    <Column sortable field="estado" header="Estado" footer="Estado" filter filterPlaceholder="Buscar por Estado" />
                    <Column sortable field="fechaPropuesta" header="Fecha Propuesta" footer="Fecha Propuesta" filter filterPlaceholder="Buscar por Fecha Propuesta" />                    
                    <Column sortable field="valorPropuesta" header="Valor Propuesta" footer="Valor Propuesta" filter filterPlaceholder="Buscar por Valor Propuesta" />
                    <Column sortable field="idParticipante" header="Id Participante" footer="Id Participante" filter filterPlaceholder="Buscar por Id Participante" />
                    <Column sortable field="razonSocialParticipante" header="Razon Social Participante" footer="Razon Social Participante" filter filterPlaceholder="Buscar por Razon Social Participante" />
                </DataTable>
            </div>
        </>
    );
}

export default Ofertas;