import React, { useState, useEffect, useRef } from 'react'
import ConsorcioService from '../service/ConsorcioService';
import { Tooltip } from 'primereact/tooltip';
import { Column } from 'primereact/column';
import { DataTable } from 'primereact/datatable';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { FilterMatchMode } from 'primereact/api';
import './Consorcio.css'

function Consorcio(props) {

    const [consorcios, setConsorcios] = useState([]);
    const [loading, setLoading] = useState(true);
    const [globalFilterValue, setGlobalFilterValue] = useState('');
    const [filters, setFilters] = useState({
        global: { value: null, matchMode: FilterMatchMode.CONTAINS },
        personaIdConsorcio: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        razonSocialConsorcio: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        idSoliCompra: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        fechaRegistro: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        personaIdParticipante: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        razonSocialParticipante: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        fechaRegistroParticipante: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        idConsorcio: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
    });

    const cargarDatosData = (idVarConsulta) => {
        ConsorcioService.getConsorcioMedium(idVarConsulta).then((data) => {
            setConsorcios(getConsorcios(data));
            setLoading(false);
        });
    }

    useEffect(() => {
        cargarDatosData(props.idVarConsulta);
    }, []);

    const getConsorcios = (data) => {
        return [...(data || [])].map((d) => {
            d.date = new Date(d.date);
            return d;
        });
    };

    const dt = useRef(null);

    const onGlobalFilterChange = (e) => {
        const value = e.target.value;
        let _filters = { ...filters };
        _filters['global'].value = value;
        setFilters(_filters);
        setGlobalFilterValue(value);
    };

    const cols = [
        { field: 'personaIdConsorcio', header: 'Persona Id Consorcio' },
        { field: 'razonSocialConsorcio', header: 'Razon Social Consorcio' },
        { field: 'idSoliCompra', header: 'Id SoliCompra' },
        { field: 'fechaRegistro', header: 'Fecha Registro' },
        { field: 'personaIdParticipante', header: 'Persona Id Participante' },
        { field: 'cedulaParticipante', header: 'Cedula Participante' },
        { field: 'razonSocialParticipante', header: 'Razon Social Participante' },
        { field: 'fechaRegistroParticipante', header: 'Fecha Registro Participante' },
        { field: 'idConsorcio', header: 'Id Consorcio' },
    ];

    const exportColumns = cols.map((col) => ({ title: col.header, dataKey: col.field }));

    const exportCSV = (selectionOnly) => {
        dt.current.exportCSV({ selectionOnly });
    };

    const exportPdf = () => {
        import('jspdf').then((jsPDF) => {
            import('jspdf-autotable').then(() => {
                const doc = new jsPDF.default(0, 0);

                doc.autoTable(exportColumns, consorcios);
                doc.save('consorcios.pdf');
            });
        });
    };

    const exportExcel = () => {
        import('xlsx').then((xlsx) => {
            const worksheet = xlsx.utils.json_to_sheet(consorcios);
            const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
            const excelBuffer = xlsx.write(workbook, {
                bookType: 'xlsx',
                type: 'array'
            });
            saveAsExcelFile(excelBuffer, 'consorcios');
        });
    };

    const header = (
        <div className="flex justify-content-end">
            <span className="p-input-icon-left">
                <i className="pi pi-search" />
                <InputText style={{fontSize: '10px'}} value={globalFilterValue} onChange={onGlobalFilterChange} placeholder="Buscar" />
            </span>
            <br></br>
            <Button type="button" style={{fontSize: '10px'}} icon="pi pi-file" rounded onClick={() => exportCSV(false)} data-pr-tooltip="CSV" label="CSV" />
            <Button type="button" style={{fontSize: '10px'}} icon="pi pi-file-excel" severity="success" rounded onClick={exportExcel} data-pr-tooltip="XLS" label="XLSX" />
            <Button type="button" style={{fontSize: '10px'}} icon="pi pi-file-pdf" severity="warning" rounded onClick={exportPdf} data-pr-tooltip="PDF" label="PDF" />
        </div>
    );

    const saveAsExcelFile = (buffer, fileName) => {
        import('file-saver').then((module) => {
            if (module && module.default) {
                let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
                let EXCEL_EXTENSION = '.xlsx';
                const data = new Blob([buffer], {
                    type: EXCEL_TYPE
                });
                module.default.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
            }
        });
    };

    return (
        <>
            <div className="tamanio_toolbar">
                <Tooltip target=".export-buttons>button" position="bottom" />
                <DataTable value={consorcios} paginator rows={10}
                    dataKey="consorcioId"
                    rowsPerPageOptions={[10, 25, 50]}
                    ref={dt}
                    filters={filters}
                    filterDisplay="row"
                    loading={loading}
                    globalFilterFields={['personaIdConsorcio', 'razonSocialConsorcio', 'idSoliCompra', 'fechaRegistro', 'personaIdParticipante',
                    'cedulaParticipante', 'razonSocialParticipante', 'fechaRegistroParticipante', 'idConsorcio']}
                    header={header}
                    emptyMessage="Información no encontrada"
                    paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                    currentPageReportTemplate="Showing {first} to {last} of {totalRecords} entries"
                    tableStyle={{ fontSize: '10px' }}>
                    
                    <Column sortable field="personaIdConsorcio" header="Persona Id Consorcio" footer="Persona Id Consorcio" filter filterPlaceholder="Buscar por Persona Id Consorcio" />
                    <Column sortable field="razonSocialConsorcio" header="Razon Social Consorcio" footer="Razon Social Consorcio" filter filterPlaceholder="Buscar por Razon Social Consorcio" />
                    <Column sortable field="idSoliCompra" header="Id SoliCompra" footer="Id SoliCompra" filter filterPlaceholder="Buscar por Id SoliCompra" />
                    <Column sortable field="fechaRegistro" header="Fecha Registro" footer="Fecha Registro" filter filterPlaceholder="Buscar por Fecha Registro" />
                    <Column sortable field="personaIdParticipante" header="Persona Id Participante" footer="Persona Id Participante" filter filterPlaceholder="Buscar por Persona Id Participante" />
                    <Column sortable field="cedulaParticipante" header="Cedula Participante" footer="Cedula Participante" filter filterPlaceholder="Buscar por Cedula Participante" />
                    <Column sortable field="razonSocialParticipante" header="Razon Social Participante" footer="Razon Social Participante" filter filterPlaceholder="Buscar por Razon Social Participante" />
                    <Column sortable field="fechaRegistroParticipante" header="Fecha Registro Participante" footer="Fecha Registro Participante" filter filterPlaceholder="Buscar por Fecha Registro Participante" />
                    <Column sortable field="idConsorcio" header="Id Consorcio" footer="Id Consorcio" filter filterPlaceholder="Buscar por Id Consorcio" />
                </DataTable>
            </div>
        </>
    );
}

export default Consorcio;