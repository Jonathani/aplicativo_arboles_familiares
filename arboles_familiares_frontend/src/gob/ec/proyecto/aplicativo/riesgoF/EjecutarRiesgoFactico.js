import React, { useState, useEffect, useRef } from 'react'
import ReactDOM, { createRoot } from 'react-dom/client';
import {
    Routes,
    Route,
    BrowserRouter as Router
} from "react-router-dom";
import { TabMenu } from 'primereact/tabmenu';
import { styled } from '@mui/material/styles';
import { Controller, useForm } from 'react-hook-form';
import { classNames } from 'primereact/utils';
import { Toast } from 'primereact/toast';
import { Panel } from 'primereact/panel';
import { InputTextarea } from "primereact/inputtextarea";
import { InputText } from 'primereact/inputtext';
import { Calendar } from 'primereact/calendar';
import { Button } from 'primereact/button';
import { Tooltip } from 'primereact/tooltip';
import { Column } from 'primereact/column';
import { DataTable } from 'primereact/datatable';
import { Dialog } from 'primereact/dialog';
import { FilterMatchMode } from 'primereact/api';
import { ConfirmDialog } from 'primereact/confirmdialog';
import Cookies from 'js-cookie';
import VariablesConsultaService from '../service/VariablesConsultaService';
import './EjecutarRiesgoFactico.css'
import "primeicons/primeicons.css";
import Consorcio from './Consorcio';
import Autoridades from './Autoridades';
import Comision from './Comision';
import Ofertas from './Ofertas';
import AccionistasSuperciasDe from './AccionistasSuperciasDe';
import AccionistasSoceDe from './AccionistasSoceDe';
import ArbolesFamiliares from './ArbolesFamiliares';
import AccionistasSuperciasEn from './AccionistasSuperciasEn';
import DireccionesComercialesSoce from './DireccionesComercialesSoce';
import DireccionesComercialesSri from './DireccionesComercialesSri';
import Mensajes from '../utilidades/Mensajes';
import Variables from '../utilidades/Variables';
import ServicioWeb from '../utilidades/ServicioWeb';
import Login from '../../../../../Login';


function EjecutarRiesgoFactico() {

    const DrawerHeader = styled('div')(({ theme }) => ({
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    }));

    let informacionSesionUsuario = Cookies.get('infoSesionUsuario');
    let objetoSesion = !!informacionSesionUsuario ? JSON.parse(informacionSesionUsuario) : "";

    const toast = useRef(null);
    const dt = useRef(null);
    const defaultValues = { codigo: '', personasExtra: '', fecha: '', hora: '' };
    const form = useForm({ defaultValues });
    const errors = form.formState.errors;
    const [blocked, setBlocked] = useState(false);
    const [loading, setLoading] = useState(true);
    const [variablesConsulta, setVariablesConsulta] = useState([]);
    const [globalFilterValue, setGlobalFilterValue] = useState('');
    const [visualizarDetalle, setVisualizarDetalle] = useState(false);
    const [idVariableConsulta, setIdVariableConsulta] = useState('');
    const [visibleGuardar, setVisibleGuardar] = useState(false);

    //Datos de enviar a guardar
    const [codigosEnviar, setCodigosEnviar] = useState('');
    const [personasExtraEnviar, setPersonasExtraEnviar] = useState('');
    const [fechaEnviar, setFechaEnviar] = useState('');

    //Mostrar tabla de variables ingresadas

    const [filters, setFilters] = useState({
        global: { value: null, matchMode: FilterMatchMode.CONTAINS },
        fechaDesde: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        personasExtra: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        codigo: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        fechaCreacion: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
    });

    const onGlobalFilterChange = (e) => {
        const value = e.target.value;
        let _filters = { ...filters };
        _filters['global'].value = value;
        setFilters(_filters);
        setGlobalFilterValue(value);
    };

    const cols = [
        { field: 'fechaDesde', header: 'Fecha Desde' },
        { field: 'personasExtra', header: 'Personas Extra' },
        { field: 'codigo', header: 'Código' },
        { field: 'fechaCreacion', header: 'Fecha Creación' }
    ];

    const exportColumns = cols.map((col) => ({ title: col.header, dataKey: col.field }));

    const exportCSV = (selectionOnly) => {
        dt.current.exportCSV({ selectionOnly });
    };

    const exportPdf = () => {
        import('jspdf').then((jsPDF) => {
            import('jspdf-autotable').then(() => {
                const doc = new jsPDF.default(0, 0);

                doc.autoTable(exportColumns, variablesConsulta);
                doc.save('usuarios.pdf');
            });
        });
    };

    const exportExcel = () => {
        import('xlsx').then((xlsx) => {
            const worksheet = xlsx.utils.json_to_sheet(variablesConsulta);
            const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
            const excelBuffer = xlsx.write(workbook, {
                bookType: 'xlsx',
                type: 'array'
            });
            saveAsExcelFile(excelBuffer, 'usuarios');
        });
    };

    const header = (
        <div className="flex justify-content-end">
            <span className="p-input-icon-left">
                <i className="pi pi-search" />
                <InputText value={globalFilterValue} style={{ fontSize: '10px' }} onChange={onGlobalFilterChange} placeholder="Buscar" />
            </span>
            <br></br>
            <Button type="button" style={{ fontSize: '10px' }} icon="pi pi-file" rounded onClick={() => exportCSV(false)} data-pr-tooltip="CSV" label="CSV" />
            <Button type="button" style={{ fontSize: '10px' }} icon="pi pi-file-excel" severity="success" rounded onClick={exportExcel} data-pr-tooltip="XLS" label="XLSX" />
            <Button type="button" style={{ fontSize: '10px' }} icon="pi pi-file-pdf" severity="warning" rounded onClick={exportPdf} data-pr-tooltip="PDF" label="PDF" />
        </div>
    );

    const saveAsExcelFile = (buffer, fileName) => {
        import('file-saver').then((module) => {
            if (module && module.default) {
                let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
                let EXCEL_EXTENSION = '.xlsx';
                const data = new Blob([buffer], {
                    type: EXCEL_TYPE
                });
                module.default.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
            }
        });
    };

    const getVariablesConsulta = (data) => {
        return [...(data || [])].map((d) => {
            d.date = new Date(d.date);
            return d;
        });
    };
    const cargarDatosData = () => {
        VariablesConsultaService.getVariablesConsultaMedium().then((data) => {
            setVariablesConsulta(getVariablesConsulta(data));
            setLoading(false);
        });
    }

    useEffect(() => {
        cargarDatosData();
    }, []);

    const showToast = (severityValue, summaryValue, detailValue, timeLife) => {
        toast?.current?.show({ severity: severityValue, summary: summaryValue, detail: detailValue, life: timeLife });
    }

    const getFormErrorMessage = (name) => {
        return errors[name] ? <small className="p-error">{errors[name].message}</small> : <small className="p-error">&nbsp;</small>;
    };

    const realizarConsulta = (data) => {
        setIdVariableConsulta(data.idVariableConsulta)
        setVisualizarDetalle(true)
    }

    const aceptarGuardado = (data) => {
        let codigo = "'" + data.codigo + "'";
        let personasExtra = data.personasExtra;
        let fecha = data.fecha;

        let fechaAEnviar = "'" + fecha.getFullYear() + "-" +
            ((fecha.getMonth().toString().length === 1) ? ("0" + (fecha.getMonth() + 1)) : (fecha.getMonth() + 1)) + "-" +
            ((fecha.getDate().toString().length === 1) ? ("0" + fecha.getDate()) : fecha.getDate()) + " " +
            ((fecha.getHours().toString().length === 1) ? ("0" + fecha.getHours()) : fecha.getHours()) + ":" +
            ((fecha.getMinutes().toString().length === 1) ? ("0" + fecha.getMinutes()) : fecha.getMinutes()) + ":00'";

        setCodigosEnviar(codigo)
        setPersonasExtraEnviar(personasExtra)
        setFechaEnviar(fechaAEnviar)
        setVisibleGuardar(true)
    }

    const ejecutarEtl = () => {

        ; (async () => {
            setBlocked(true)
            let tokenApi = await ServicioWeb.obtenerTokenEtl(Variables.USER_LOGIN_API_BACKEND, Variables.PASSWORD_LOGIN_API_BACKEND, Variables.PETICION_APIS_ETLS_TOKEN);
            let rowDetallado = {
                fecha: fechaEnviar,
                personasExtra: personasExtraEnviar,
                codigos: codigosEnviar,
                idUsuario: objetoSesion.idUsuario
            }
            let requestBody = {
                method: 'POST',
                headers: {
                    Accept: "application/json",
                    'Content-Type': 'application/json',
                    'origin': 'x-request-with',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                    'Access-Control-Allow-Credentials': 'true',
                    'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                    Authorization: tokenApi
                },
                body: JSON.stringify(rowDetallado)
            }
            let rows = await ServicioWeb.obtenerDatosConBody(
                Variables.PETICION_APIS_EJECUCION_ETL,
                requestBody
            );
            if (rows.status) {
                cargarDatosData();
                setBlocked(false)
                showToast('success', 'Success', rows.respuesta, 6000);
            } else {
                setBlocked(false)
                showToast('error', 'Error', rows.respuesta, 6000);
            }
        })();
    }

    const actionBodyTemplate = (rowData) => {
        return (
            <React.Fragment>
                <Button icon="pi pi-pencil" style={{ fontSize: '10px' }} rounded outlined className="mr-2" label="Mostrar Info" onClick={() => realizarConsulta(rowData)} />
            </React.Fragment>
        );
    };

    const items = [
        {
            label: 'Consorcio',
            icon: 'pi pi-book',
            command: () => {
                const root = ReactDOM.createRoot(document.getElementById("detalleConsulta"));
                root.render(
                    <React.StrictMode>
                        <Router>
                            <Routes>
                                <Route path="/" element={<Login />} />
                                <Route path="/login" element={<Login />} />
                            </Routes>
                            <Consorcio idVarConsulta={idVariableConsulta} />
                        </Router>
                    </React.StrictMode>
                );
            }
        },
        {
            label: 'Autoridades',
            icon: 'pi pi-book',
            command: () => {
                const root = ReactDOM.createRoot(document.getElementById("detalleConsulta"));
                root.render(
                    <React.StrictMode>
                        <Router>
                            <Routes>
                                <Route path="/" element={<Login />} />
                                <Route path="/login" element={<Login />} />
                            </Routes>
                            <Autoridades idVarConsulta={idVariableConsulta} />
                        </Router>
                    </React.StrictMode>
                );
            }
        },
        {
            label: 'Comisión',
            icon: 'pi pi-book',
            command: () => {
                const root = ReactDOM.createRoot(document.getElementById("detalleConsulta"));
                root.render(
                    <React.StrictMode>
                        <Router>
                            <Routes>
                                <Route path="/" element={<Login />} />
                                <Route path="/login" element={<Login />} />
                            </Routes>
                            <Comision idVarConsulta={idVariableConsulta} />
                        </Router>
                    </React.StrictMode>
                );
            }
        },
        {
            label: 'Ofertas',
            icon: 'pi pi-book',
            command: () => {
                const root = ReactDOM.createRoot(document.getElementById("detalleConsulta"));
                root.render(
                    <React.StrictMode>
                        <Router>
                            <Routes>
                                <Route path="/" element={<Login />} />
                                <Route path="/login" element={<Login />} />
                            </Routes>
                            <Ofertas idVarConsulta={idVariableConsulta} />
                        </Router>
                    </React.StrictMode>
                );
            }
        },
        {
            label: 'Accionistas Supercias De',
            icon: 'pi pi-book',
            command: () => {
                const root = ReactDOM.createRoot(document.getElementById("detalleConsulta"));
                root.render(
                    <React.StrictMode>
                        <Router>
                            <Routes>
                                <Route path="/" element={<Login />} />
                                <Route path="/login" element={<Login />} />
                            </Routes>
                            <AccionistasSuperciasDe idVarConsulta={idVariableConsulta} />
                        </Router>
                    </React.StrictMode>
                );
            }
        },
        {
            label: 'Accionistas Soce De',
            icon: 'pi pi-book',
            command: () => {
                const root = ReactDOM.createRoot(document.getElementById("detalleConsulta"));
                root.render(
                    <React.StrictMode>
                        <Router>
                            <Routes>
                                <Route path="/" element={<Login />} />
                                <Route path="/login" element={<Login />} />
                            </Routes>
                            <AccionistasSoceDe idVarConsulta={idVariableConsulta} />
                        </Router>
                    </React.StrictMode>
                );
            }
        },
        {
            label: 'Arboles Familiares',
            icon: 'pi pi-book',
            command: () => {
                const root = ReactDOM.createRoot(document.getElementById("detalleConsulta"));
                root.render(
                    <React.StrictMode>
                        <Router>
                            <Routes>
                                <Route path="/" element={<Login />} />
                                <Route path="/login" element={<Login />} />
                            </Routes>
                            <ArbolesFamiliares idVarConsulta={idVariableConsulta} />
                        </Router>
                    </React.StrictMode>
                );
            }
        },
        {
            label: 'Accionistas Supercias En',
            icon: 'pi pi-book',
            command: () => {
                const root = ReactDOM.createRoot(document.getElementById("detalleConsulta"));
                root.render(
                    <React.StrictMode>
                        <Router>
                            <Routes>
                                <Route path="/" element={<Login />} />
                                <Route path="/login" element={<Login />} />
                            </Routes>
                            <AccionistasSuperciasEn idVarConsulta={idVariableConsulta} />
                        </Router>
                    </React.StrictMode>
                );
            }
        },
        {
            label: 'Direcciones Comerciales SOCE',
            icon: 'pi pi-book',
            command: () => {
                const root = ReactDOM.createRoot(document.getElementById("detalleConsulta"));
                root.render(
                    <React.StrictMode>
                        <Router>
                            <Routes>
                                <Route path="/" element={<Login />} />
                                <Route path="/login" element={<Login />} />
                            </Routes>
                            <DireccionesComercialesSoce idVarConsulta={idVariableConsulta} />
                        </Router>
                    </React.StrictMode>
                );
            }
        },
        {
            label: 'Direcciones Comerciales SRI',
            icon: 'pi pi-book',
            command: () => {
                const root = ReactDOM.createRoot(document.getElementById("detalleConsulta"));
                root.render(
                    <React.StrictMode>
                        <Router>
                            <Routes>
                                <Route path="/" element={<Login />} />
                                <Route path="/login" element={<Login />} />
                            </Routes>
                            <DireccionesComercialesSri idVarConsulta={idVariableConsulta} />
                        </Router>
                    </React.StrictMode>
                );
            }
        }
    ];

    const hideVisualizarDetalle = () => {
        setVisualizarDetalle(false)
    }

    return (
        <>
            <DrawerHeader />
            <ConfirmDialog visible={visibleGuardar} onHide={() => setVisibleGuardar(false)} message={Mensajes.ACEPTAR_GUARDADO_GENERICO}
                header="Confirmación" icon="pi pi-exclamation-triangle" accept={ejecutarEtl} />
            <div className="container_ejecutar_riesgof">
                <Toast ref={toast} />
                <div className="panel_principal_ejecutar_riesgof">
                    <Panel header={<h5>Ejecutar Riesgo Fáctico</h5>} className="mypanel">
                        <form onSubmit={form.handleSubmit(aceptarGuardado)} className="flex flex-column gap-3">
                            <div className="card flex flex-wrap gap-3 p-fluid">
                                <div className="flex-auto">
                                    <Controller
                                        name="codigo"
                                        control={form.control}
                                        rules={{ required: 'El código proceso es obligatorio.' }}
                                        render={({ field, fieldState }) => (
                                            <>
                                                <label style={{ fontSize: '10px' }} htmlFor={field.name} className={classNames({ 'p-error': errors.value })}>Código Proceso: <span className='required'>*</span></label>
                                                <span className="p-float-label">
                                                    <InputText id={field.name} {...field}
                                                        type="text"
                                                        style={{ fontSize: '10px' }}
                                                        maxLength={60}
                                                        className={classNames({ 'p-invalid': fieldState.error }, "md:w-40rem w-full")}
                                                    />
                                                    <label htmlFor={field.name}></label>
                                                </span>
                                                {getFormErrorMessage(field.name)}
                                            </>
                                        )}
                                    />
                                </div>
                                <div className="flex-auto">
                                    <Controller
                                        name="fecha"
                                        control={form.control}
                                        rules={{ required: 'La fecha es obligatoria.' }}
                                        render={({ field, fieldState }) => (
                                            <>
                                                <label style={{ fontSize: '10px' }} htmlFor={field.name} className={classNames({ 'p-error': errors.value })}>Fecha: <span className='required'>*</span></label>
                                                <span className="p-float-label">
                                                    <div className="card flex justify-content-center">
                                                        <Calendar id={field.name} {...field}
                                                            style={{ fontSize: '10px' }}
                                                            showTime hourFormat="24"
                                                            className={classNames({ 'p-invalid': fieldState.error }, "md:w-40rem w-full", "datepicker-touch-ui")}
                                                            showIcon
                                                        />
                                                    </div>
                                                    <label htmlFor={field.name}></label>
                                                </span>
                                                {getFormErrorMessage(field.name)}
                                            </>
                                        )}
                                    />
                                </div>
                            </div>
                            <Controller
                                name="personasExtra"
                                control={form.control}
                                rules={{}}
                                render={({ field, fieldState }) => (
                                    <>
                                        <label style={{ fontSize: '10px' }} htmlFor={field.name} className={classNames({ 'p-error': errors.value })}>Personas Adicionales:</label>
                                        <span className="p-float-label">
                                            <InputTextarea id={field.name} {...field}
                                                maxLength={500}
                                                style={{ fontSize: '10px' }}
                                                rows={4} cols={100}
                                                className={classNames({ 'p-invalid': fieldState.error }, "md:w-40rem w-full")}
                                            />
                                            <label htmlFor={field.name}></label>
                                        </span>
                                        {getFormErrorMessage(field.name)}
                                    </>
                                )}
                            />
                            <Button label="Ejecutar" type="submit" icon="pi pi-search" loading={blocked} style={{ fontSize: '10px' }} />
                        </form>
                        <div className="tamanio_toolbar">
                            <Tooltip target=".export-buttons>button" position="bottom" />
                            <DataTable value={variablesConsulta}
                                paginator
                                rows={10}
                                dataKey="idVariableConsulta"
                                rowsPerPageOptions={[10, 25, 50]}
                                ref={dt}
                                filters={filters}
                                filterDisplay="row"
                                loading={loading}
                                globalFilterFields={['codigo', 'fechaDesde', 'personasExtra', 'fechaCreacion']}
                                header={header}
                                action={<EjecutarRiesgoFactico loadParameter={cargarDatosData} />}
                                emptyMessage="Información no encontrada"
                                paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                                currentPageReportTemplate="Showing {first} to {last} of {totalRecords} entries"
                                size={"large"}>

                                <Column body={actionBodyTemplate} exportable={false} style={{ fontSize: '10px' }}></Column>
                                <Column field="codigo" sortable header="Código de Proceso" footer="Código de Proceso" filter filterPlaceholder="Buscar por Código de Proceso" style={{ fontSize: '10px' }} />
                                <Column field="fechaDesde" sortable header="Fecha Desde" footer="Fecha Desde" filter filterPlaceholder="Buscar por Fecha Desde" style={{ fontSize: '10px' }} />
                                <Column field="personasExtra" header="Personas Extra" footer="Personas Extra" filter filterPlaceholder="Buscar por Personas Extra" style={{ fontSize: '10px' }} />
                                <Column field="fechaCreacion" sortable header="Fecha Ejecución" footer="Fecha Ejecución" filter filterPlaceholder="Buscar por Fecha Ejecución" style={{ fontSize: '10px' }} />
                            </DataTable>
                        </div>
                        {/* Dialogo para el detalle de la consulta de riesgo factico */}
                        <Dialog visible={visualizarDetalle} style={{ width: '60%' }}
                            breakpoints={{ '960px': '75vw', '641px': '90vw' }}
                            header="Visualizar Detalle" modal className="p-fluid"
                            onHide={hideVisualizarDetalle}>
                            <div className="card">
                                {/* Menubar TabMenu */}
                                <TabMenu model={items} />
                                <div id="detalleConsulta">
                                </div>
                            </div>
                        </Dialog>
                    </Panel>
                </div>
            </div>
        </>
    );
}

export default EjecutarRiesgoFactico;