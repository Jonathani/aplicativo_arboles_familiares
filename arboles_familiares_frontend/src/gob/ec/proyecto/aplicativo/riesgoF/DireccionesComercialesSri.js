import React, { useState, useEffect, useRef } from 'react'
import DireccionesComercialesSriService from '../service/DireccionesComercialesSriService';
import { Tooltip } from 'primereact/tooltip';
import { Column } from 'primereact/column';
import { DataTable } from 'primereact/datatable';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { FilterMatchMode } from 'primereact/api';
import './DireccionesComercialesSri.css'

function DireccionesComercialesSri(props) {

    const [direccionesComercialesSri, setDireccionesComercialesSri] = useState([]);
    const [loading, setLoading] = useState(true);
    const [globalFilterValue, setGlobalFilterValue] = useState('');
    const [filters, setFilters] = useState({
        global: { value: null, matchMode: FilterMatchMode.CONTAINS },
        idDireccionesComercial: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        numeroRuc: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        calle: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        numero: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        interseccion: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        referenciaUbicacion: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        provincia: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        canton: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        estadoEstablecimiento: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
    });

    const cargarDatosData = (idVarConsulta) => {
        DireccionesComercialesSriService.getDireccionesComercialesSriMedium(idVarConsulta).then((data) => {
            setDireccionesComercialesSri(getDireccionesComercialesSri(data));
            setLoading(false);
        });
    }

    useEffect(() => {
        cargarDatosData(props.idVarConsulta);
    }, []);

    const getDireccionesComercialesSri = (data) => {
        return [...(data || [])].map((d) => {
            d.date = new Date(d.date);
            return d;
        });
    };

    const dt = useRef(null);

    const onGlobalFilterChange = (e) => {
        const value = e.target.value;
        let _filters = { ...filters };
        _filters['global'].value = value;
        setFilters(_filters);
        setGlobalFilterValue(value);
    };

    const cols = [
        { field: 'idDireccionesComercial', header: 'Id Direcciones Comercial' },
        { field: 'numeroRuc', header: 'Número Ruc' },
        { field: 'razonSocial', header: 'Razon Social' },
        { field: 'ubicacionGeografica', header: 'Ubicación Geográfica' },        
        { field: 'calle', header: 'Calle' },
        { field: 'numero', header: 'Número' },
        { field: 'interseccion', header: 'intersección' },
        { field: 'referenciaUbicacion', header: 'Referencia Ubicación' },
        { field: 'provincia', header: 'Provincia' },
        { field: 'canton', header: 'Canton' },
        { field: 'estadoEstablecimiento', header: 'Estado Establecimiento' },
    ];

    const exportColumns = cols.map((col) => ({ title: col.header, dataKey: col.field }));

    const exportCSV = (selectionOnly) => {
        dt.current.exportCSV({ selectionOnly });
    };

    const exportPdf = () => {
        import('jspdf').then((jsPDF) => {
            import('jspdf-autotable').then(() => {
                const doc = new jsPDF.default(0, 0);

                doc.autoTable(exportColumns, direccionesComercialesSri);
                doc.save('direccionesComercialesSri.pdf');
            });
        });
    };

    const exportExcel = () => {
        import('xlsx').then((xlsx) => {
            const worksheet = xlsx.utils.json_to_sheet(direccionesComercialesSri);
            const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
            const excelBuffer = xlsx.write(workbook, {
                bookType: 'xlsx',
                type: 'array'
            });
            saveAsExcelFile(excelBuffer, 'direccionesComercialesSri');
        });
    };

    const header = (
        <div className="flex justify-content-end">
            <span className="p-input-icon-left">
                <i className="pi pi-search" />
                <InputText style={{fontSize: '10px'}} value={globalFilterValue} onChange={onGlobalFilterChange} placeholder="Buscar" />
            </span>
            <br></br>
            <Button type="button" style={{fontSize: '10px'}} icon="pi pi-file" rounded onClick={() => exportCSV(false)} data-pr-tooltip="CSV" label="CSV" />
            <Button type="button" style={{fontSize: '10px'}} icon="pi pi-file-excel" severity="success" rounded onClick={exportExcel} data-pr-tooltip="XLS" label="XLSX" />
            <Button type="button" style={{fontSize: '10px'}} icon="pi pi-file-pdf" severity="warning" rounded onClick={exportPdf} data-pr-tooltip="PDF" label="PDF" />
        </div>
    );

    const saveAsExcelFile = (buffer, fileName) => {
        import('file-saver').then((module) => {
            if (module && module.default) {
                let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
                let EXCEL_EXTENSION = '.xlsx';
                const data = new Blob([buffer], {
                    type: EXCEL_TYPE
                });
                module.default.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
            }
        });
    };

    return (
        <>
            <div className="tamanio_toolbar">
                <Tooltip target=".export-buttons>button" position="bottom" />
                <DataTable value={direccionesComercialesSri} paginator rows={10}
                    dataKey="idDireccionesComercial"
                    rowsPerPageOptions={[10, 25, 50]}
                    ref={dt}
                    filters={filters}
                    filterDisplay="row"
                    loading={loading}
                    globalFilterFields={['numeroRuc', 'razonSocial', 'ubicacionGeografica', 'calle', 'numero', 'interseccion', 'referenciaUbicacion', 
                    'provincia', 'canton' , 'estadoEstablecimiento']}
                    header={header}
                    emptyMessage="Información no encontrada"
                    paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                    currentPageReportTemplate="Showing {first} to {last} of {totalRecords} entries"
                    tableStyle={{ fontSize: '10px' }}>
                    
                    <Column sortable field="numeroRuc" header="Número Ruc" footer="Número Ruc" filter filterPlaceholder="Buscar por Número Ruc" />
                    <Column sortable field="razonSocial" header="Razon Social" footer="Razon Social" filter filterPlaceholder="Buscar por Razon Social" />
                    <Column sortable field="ubicacionGeografica" header="Ubicación Geográfica" footer="Ubicación Geográfica" filter filterPlaceholder="Buscar por Ubicación Geográfica" />
                    <Column sortable field="calle" header="Calle" footer="Calle" filter filterPlaceholder="Buscar por Calle" />
                    <Column sortable field="numero" header="Número" footer="Número" filter filterPlaceholder="Buscar por Número" />
                    <Column sortable field="interseccion" header="Intersección" footer="Intersección" filter filterPlaceholder="Buscar por Intersección" />
                    <Column sortable field="referenciaUbicacion" header="Referencia Ubicación" footer="Referencia Ubicación" filter filterPlaceholder="Buscar por Referencia Ubicación" />
                    <Column sortable field="provincia" header="Provincia" footer="Provincia" filter filterPlaceholder="Buscar por Provincia" />
                    <Column sortable field="canton" header="Canton" footer="Canton" filter filterPlaceholder="Buscar por Canton" />
                    <Column sortable field="estadoEstablecimiento" header="Estado Establecimiento" footer="Estado Establecimiento" filter filterPlaceholder="Buscar por Estado Establecimiento" />
                </DataTable>
            </div>
        </>
    );
}

export default DireccionesComercialesSri;