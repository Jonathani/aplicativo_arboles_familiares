import React, { useState, useEffect, useRef } from 'react'
import ArbolesFamiliaresService from '../service/ArbolesFamiliaresService';
import { Tooltip } from 'primereact/tooltip';
import { Column } from 'primereact/column';
import { DataTable } from 'primereact/datatable';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { FilterMatchMode } from 'primereact/api';
import './ArbolesFamiliares.css'

function ArbolesFamiliares(props) {

    const [arbolesFamiliares, setArbolesFamiliares] = useState([]);
    const [loading, setLoading] = useState(true);
    const [globalFilterValue, setGlobalFilterValue] = useState('');
    const [filters, setFilters] = useState({
        global: { value: null, matchMode: FilterMatchMode.CONTAINS },
        idArbolesFamiliares: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        cedula: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        cedulaConyuge: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        cedulaMadre: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        cedulaPadre: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        estadoCivil: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        fechaNacimiento: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        identificacionUsuarioConsultado: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        nivel: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        nombreConyuge: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        nombresMadre: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        nombresPadre: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        nombresUsuarioConsultado: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        tipo: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
    });

    const cargarDatosData = (idVarConsulta) => {
        ArbolesFamiliaresService.getArbolesFamiliaresMedium(idVarConsulta).then((data) => {
            setArbolesFamiliares(getArbolesFamiliares(data));
            setLoading(false);
        });
    }

    useEffect(() => {
        cargarDatosData(props.idVarConsulta);
    }, []);

    const getArbolesFamiliares = (data) => {
        return [...(data || [])].map((d) => {
            d.date = new Date(d.date);
            return d;
        });
    };

    const dt = useRef(null);

    const onGlobalFilterChange = (e) => {
        const value = e.target.value;
        let _filters = { ...filters };
        _filters['global'].value = value;
        setFilters(_filters);
        setGlobalFilterValue(value);
    };

    const cols = [
        { field: 'idArbolesFamiliares', header: 'Id Arboles Familiares' },
        { field: 'cedula', header: 'Cédula' },
        { field: 'cedulaConyuge', header: 'Cédula Conyuge' },
        { field: 'cedulaMadre', header: 'Cédula Madre' },        
        { field: 'cedulaPadre', header: 'Cédula Padre' },
        { field: 'estadoCivil', header: 'Estado Civil' },
        { field: 'fechaNacimiento', header: 'Fecha Nacimiento' },
        { field: 'identificacionUsuarioConsultado', header: 'Identificación Usuario Consultado' },
        { field: 'nivel', header: 'Nivel' },
        { field: 'nombreConyuge', header: 'Nombre Conyuge' },        
        { field: 'nombresMadre', header: 'Nombres Madre' },
        { field: 'nombresPadre', header: 'Nombres Padre' },
        { field: 'nombresUsuarioConsultado', header: 'Nombres Usuario Consultado' },
        { field: 'tipo', header: 'Tipo' },
    ];

    const exportColumns = cols.map((col) => ({ title: col.header, dataKey: col.field }));

    const exportCSV = (selectionOnly) => {
        dt.current.exportCSV({ selectionOnly });
    };

    const exportPdf = () => {
        import('jspdf').then((jsPDF) => {
            import('jspdf-autotable').then(() => {
                const doc = new jsPDF.default(0, 0);

                doc.autoTable(exportColumns, arbolesFamiliares);
                doc.save('arbolesFamiliares.pdf');
            });
        });
    };

    const exportExcel = () => {
        import('xlsx').then((xlsx) => {
            const worksheet = xlsx.utils.json_to_sheet(arbolesFamiliares);
            const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
            const excelBuffer = xlsx.write(workbook, {
                bookType: 'xlsx',
                type: 'array'
            });
            saveAsExcelFile(excelBuffer, 'arbolesFamiliares');
        });
    };

    const header = (
        <div className="flex justify-content-end">
            <span className="p-input-icon-left">
                <i className="pi pi-search" />
                <InputText style={{ fontSize: '10px' }} value={globalFilterValue} onChange={onGlobalFilterChange} placeholder="Buscar" />
            </span>
            <br></br>
            <Button type="button" style={{ fontSize: '10px' }} icon="pi pi-file" rounded onClick={() => exportCSV(false)} data-pr-tooltip="CSV" label="CSV" />
            <Button type="button" style={{ fontSize: '10px' }} icon="pi pi-file-excel" severity="success" rounded onClick={exportExcel} data-pr-tooltip="XLS" label="XLSX" />
            <Button type="button" style={{ fontSize: '10px' }} icon="pi pi-file-pdf" severity="warning" rounded onClick={exportPdf} data-pr-tooltip="PDF" label="PDF" />
        </div>
    );

    const saveAsExcelFile = (buffer, fileName) => {
        import('file-saver').then((module) => {
            if (module && module.default) {
                let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
                let EXCEL_EXTENSION = '.xlsx';
                const data = new Blob([buffer], {
                    type: EXCEL_TYPE
                });
                module.default.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
            }
        });
    };

    return (
        <>
            <div className="tamanio_toolbar">
                <Tooltip target=".export-buttons>button" position="bottom" />
                <DataTable value={arbolesFamiliares} paginator rows={10}
                    dataKey="idArbolesFamiliares"
                    rowsPerPageOptions={[10, 25, 50]}
                    ref={dt}
                    filters={filters}
                    filterDisplay="row"
                    loading={loading}
                    globalFilterFields={['cedula', 'cedulaConyuge', 'cedulaMadre', 'cedulaPadre', 'estadoCivil', 
                    'fechaNacimiento', 'identificacionUsuarioConsultado', 'nivel', 'nombreConyuge', 'nombresMadre','nombresPadre', 'nombresUsuarioConsultado', 'tipo']}
                    header={header}
                    emptyMessage="Información no encontrada"
                    paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                    currentPageReportTemplate="Showing {first} to {last} of {totalRecords} entries"
                    tableStyle={{fontSize: '10px'}}>
                    
                    <Column sortable field="cedula" header="Cédula" footer="Cédula" filter filterPlaceholder="Buscar por Cédula" />
                    <Column sortable field="cedulaConyuge" header="Cédula Conyuge" footer="Cédula Conyuge" filter filterPlaceholder="Buscar por Cédula Conyuge" />
                    <Column sortable field="cedulaMadre" header="Cédula Madre" footer="Cédula Madre" filter filterPlaceholder="Buscar por Cédula Madre" />
                    <Column sortable field="cedulaPadre" header="Cédula Padre" footer="Cédula Padre" filter filterPlaceholder="Buscar por Cédula Padre" />
                    <Column sortable field="estadoCivil" header="Estado Civil" footer="Estado Civil" filter filterPlaceholder="Buscar por Estado Civil" />
                    <Column sortable field="fechaNacimiento" header="Fecha Nacimiento" footer="Fecha Nacimiento" filter filterPlaceholder="Buscar por Fecha Nacimiento" />
                    <Column sortable field="identificacionUsuarioConsultado" header="Identificación Usuario Consultado" footer="Identificación Usuario Consultado" filter filterPlaceholder="Buscar por Identificación Usuario Consultado" />
                    <Column sortable field="nivel" header="Nivel" footer="Nivel" filter filterPlaceholder="Buscar por Nivel" />
                    <Column sortable field="nombreConyuge" header="Nombre Conyuge" footer="Nombre Conyuge" filter filterPlaceholder="Buscar por Nombre Conyuge" />
                    <Column sortable field="nombresMadre" header="Nombres Madre" footer="Nombres Madre" filter filterPlaceholder="Buscar por Nombres Madre" />
                    <Column sortable field="nombresPadre" header="Nombres Padre" footer="Nombres Padre" filter filterPlaceholder="Buscar por Nombres Padre" />
                    <Column sortable field="nombresUsuarioConsultado" header="Nombres Usuario Consultado" footer="Nombres Usuario Consultado" filter filterPlaceholder="Buscar por Nombres Usuario Consultado" />
                    <Column sortable field="tipo" header="Tipo" footer="Tipo" filter filterPlaceholder="Buscar por Tipo" />
                </DataTable>
            </div>
        </>
    );
}

export default ArbolesFamiliares;