import React, { useState, useEffect, useRef } from 'react'
import AccionistasSuperciasDeService from '../service/AccionistasSuperciasDeService';
import { Tooltip } from 'primereact/tooltip';
import { Column } from 'primereact/column';
import { DataTable } from 'primereact/datatable';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { FilterMatchMode } from 'primereact/api';
import './AccionistasSuperciasDe.css'

function AccionistasSuperciasDe(props) {

    const [accionistasSuperciasDe, setAccionistasSuperciasDe] = useState([]);
    const [loading, setLoading] = useState(true);
    const [globalFilterValue, setGlobalFilterValue] = useState('');
    const [filters, setFilters] = useState({
        global: { value: null, matchMode: FilterMatchMode.CONTAINS },
        idAccionistrasSuperciasDe: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        expediente: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        ruc: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        razonSocial: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        nombre: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
    });

    const cargarDatosData = (idVarConsulta) => {
        AccionistasSuperciasDeService.getAccionistasSuperciasDeMedium(idVarConsulta).then((data) => {
            setAccionistasSuperciasDe(getAccionistasSuperciasDe(data));
            setLoading(false);
        });
    }

    useEffect(() => {
        cargarDatosData(props.idVarConsulta);
    }, []);

    const getAccionistasSuperciasDe = (data) => {
        return [...(data || [])].map((d) => {
            d.date = new Date(d.date);
            return d;
        });
    };

    const dt = useRef(null);

    const onGlobalFilterChange = (e) => {
        const value = e.target.value;
        let _filters = { ...filters };
        _filters['global'].value = value;
        setFilters(_filters);
        setGlobalFilterValue(value);
    };

    const cols = [
        { field: 'idAccionistasSuperciasDe', header: 'Id Accionistas Supercias De' },
        { field: 'expediente', header: 'Expediente' },
        { field: 'ruc', header: 'Ruc' },
        { field: 'razonSocial', header: 'Razon Social' },        
        { field: 'cedula', header: 'Cédula' },
        { field: 'nombre', header: 'Nombre' },
    ];

    const exportColumns = cols.map((col) => ({ title: col.header, dataKey: col.field }));

    const exportCSV = (selectionOnly) => {
        dt.current.exportCSV({ selectionOnly });
    };

    const exportPdf = () => {
        import('jspdf').then((jsPDF) => {
            import('jspdf-autotable').then(() => {
                const doc = new jsPDF.default(0, 0);

                doc.autoTable(exportColumns, accionistasSuperciasDe);
                doc.save('accionistasSuperciasDe.pdf');
            });
        });
    };

    const exportExcel = () => {
        import('xlsx').then((xlsx) => {
            const worksheet = xlsx.utils.json_to_sheet(accionistasSuperciasDe);
            const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
            const excelBuffer = xlsx.write(workbook, {
                bookType: 'xlsx',
                type: 'array'
            });
            saveAsExcelFile(excelBuffer, 'accionistasSuperciasDe');
        });
    };

    const header = (
        <div className="flex justify-content-end">
            <span className="p-input-icon-left">
                <i className="pi pi-search" />
                <InputText style={{fontSize: '10px'}} value={globalFilterValue} onChange={onGlobalFilterChange} placeholder="Buscar" />
            </span>
            <br></br>
            <Button type="button" style={{fontSize: '10px'}} icon="pi pi-file" rounded onClick={() => exportCSV(false)} data-pr-tooltip="CSV" label="CSV" />
            <Button type="button" style={{fontSize: '10px'}} icon="pi pi-file-excel" severity="success" rounded onClick={exportExcel} data-pr-tooltip="XLS" label="XLSX" />
            <Button type="button" style={{fontSize: '10px'}} icon="pi pi-file-pdf" severity="warning" rounded onClick={exportPdf} data-pr-tooltip="PDF" label="PDF" />
        </div>
    );

    const saveAsExcelFile = (buffer, fileName) => {
        import('file-saver').then((module) => {
            if (module && module.default) {
                let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
                let EXCEL_EXTENSION = '.xlsx';
                const data = new Blob([buffer], {
                    type: EXCEL_TYPE
                });
                module.default.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
            }
        });
    };

    return (
        <>
            <div className="tamanio_toolbar">
                <Tooltip target=".export-buttons>button" position="bottom" />
                <DataTable value={accionistasSuperciasDe} paginator rows={10}
                    dataKey="idAccionistrasSuperciasDe"
                    rowsPerPageOptions={[10, 25, 50]}
                    ref={dt}
                    filters={filters}
                    filterDisplay="row"
                    loading={loading}
                    globalFilterFields={['expediente', 'ruc', 'razonSocial', 'cedula', 'nombre']}
                    header={header}
                    emptyMessage="Información no encontrada"
                    paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                    currentPageReportTemplate="Showing {first} to {last} of {totalRecords} entries"
                    tableStyle={{ fontSize: '10px' }}>
                    
                    <Column field="expediente" sortable header="Expediente" footer="Expediente" filter filterPlaceholder="Buscar por Expediente" />
                    <Column field="ruc" sortable header="Ruc" footer="Ruc" filter filterPlaceholder="Buscar por Ruc" />
                    <Column field="razonSocial" sortable header="Razon Social" footer="Razon Social" filter filterPlaceholder="Buscar por Razon Social"  />
                    <Column field="cedula" sortable header="Cédula" footer="Cédula" filter filterPlaceholder="Buscar por Cédula" />
                    <Column field="nombre" sortable header="Nombre" footer="Nombre" filter filterPlaceholder="Buscar por Nombre" />
                </DataTable>
            </div>
        </>
    );
}

export default AccionistasSuperciasDe;