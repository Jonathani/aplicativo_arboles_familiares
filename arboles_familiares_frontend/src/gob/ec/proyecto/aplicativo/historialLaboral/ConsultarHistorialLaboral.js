import React, { useState, useEffect, useRef, useCallback } from 'react'
import { styled } from '@mui/material/styles';
import { Panel } from 'primereact/panel';
import { Toast } from 'primereact/toast';
import { classNames } from 'primereact/utils';
import { Controller, useForm } from 'react-hook-form';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { ConfirmDialog } from 'primereact/confirmdialog';
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { Divider } from 'primereact/divider';
import { Column } from 'primereact/column';
import { Tooltip } from 'primereact/tooltip';
import { FileUpload } from 'primereact/fileupload';
import { InputTextarea } from "primereact/inputtextarea";
import { Tag } from 'primereact/tag';
import Mensajes from '../utilidades/Mensajes';
import ServicioWeb from '../utilidades/ServicioWeb';
import Variables from '../utilidades/Variables';
import Cookies from 'js-cookie';
import './ConsultarHistorialLaboral.css';

function ConsultarHistorialLaboral() {

    let informacionSesionUsuario = Cookies.get('infoSesionUsuario');
    let objetoSesion = !!informacionSesionUsuario ? JSON.parse(informacionSesionUsuario) : "";

    const dt = useRef(null);
    const defaultValues = { ruc: '' };
    const form = useForm({ defaultValues });
    const errors = form.formState.errors;
    const [blocked, setBlocked] = useState(false);
    const [visibleConsultar, setVisibleConsultar] = useState(false);
    const [globalFilterValue, setGlobalFilterValue] = useState('');
    const [arregloRespuesta, setArregloRespuesta] = React.useState([]);
    const [arregloRespuestaFinal, setArregloRespuestaFinal] = React.useState([]);

    const [totalSize, setTotalSize] = useState(0);

    const [cedula, setCedula] = useState("");
    const [justificacionDetalleConn, setJustificacionDetalleConn] = useState("");
    const [archivoJustificacion, setArchivoJustificacion] = useState("");

    const [historialLaboral, setHistorialLaboral] = useState([]);
    const [loading, setLoading] = useState(false);

    const [filters, setFilters] = useState({
        global: { value: null, matchMode: FilterMatchMode.CONTAINS },
        cedula: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        nombresCompletos: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        ruc: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        razonSocial: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        periodoDesde: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        periodoHasta: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
    });

    const onGlobalFilterChange = (e) => {
        const value = e.target.value;
        let _filters = { ...filters };
        _filters['global'].value = value;
        setFilters(_filters);
        setGlobalFilterValue(value);
    };

    const cols = [
        { field: 'cedula', header: 'Cédula Consultado' },
        { field: 'nombresCompletos', header: 'Nombres Completos Consultado' },
        { field: 'ruc', header: 'Ruc Empresa' },
        { field: 'razonSocial', header: 'Razon Social Empresa' },
        { field: 'periodoDesde', header: 'Período Desde' },
        { field: 'periodoHasta', header: 'Período Hasta' },
    ];

    const exportColumns = cols.map((col) => ({ title: col.header, dataKey: col.field }));

    const exportCSV = (selectionOnly) => {
        dt.current.exportCSV({ selectionOnly });
    };

    const exportPdf = () => {
        import('jspdf').then((jsPDF) => {
            import('jspdf-autotable').then(() => {
                const doc = new jsPDF.default(0, 0);

                doc.autoTable(exportColumns, historialLaboral);
                doc.save('historialLaboral.pdf');
            });
        });
    };

    const exportExcel = () => {
        import('xlsx').then((xlsx) => {
            const worksheet = xlsx.utils.json_to_sheet(historialLaboral);
            const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
            const excelBuffer = xlsx.write(workbook, {
                bookType: 'xlsx',
                type: 'array'
            });
            saveAsExcelFile(excelBuffer, 'historialLaboral');
        });
    };


    const header = (
        <div className="flex justify-content-end">
            <span className="p-input-icon-left">
                <i className="pi pi-search" />
                <InputText value={globalFilterValue} style={{ fontSize: '10px' }} onChange={onGlobalFilterChange} placeholder="Buscar" />
            </span>
            <br></br>
            <Button type="button" icon="pi pi-file" style={{ fontSize: '10px' }} rounded onClick={() => exportCSV(false)} data-pr-tooltip="CSV" label="CSV" />
            <Button type="button" icon="pi pi-file-excel" style={{ fontSize: '10px' }} severity="success" rounded onClick={exportExcel} data-pr-tooltip="XLS" label="XLSX" />
            <Button type="button" icon="pi pi-file-pdf" style={{ fontSize: '10px' }} severity="warning" rounded onClick={exportPdf} data-pr-tooltip="PDF" label="PDF" />
        </div>
    );

    const saveAsExcelFile = (buffer, fileName) => {
        import('file-saver').then((module) => {
            if (module && module.default) {
                let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
                let EXCEL_EXTENSION = '.xlsx';
                const data = new Blob([buffer], {
                    type: EXCEL_TYPE
                });
                module.default.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
            }
        });
    };

    const DrawerHeader = styled('div')(({ theme }) => ({
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    }));

    const toast = useRef(null);

    const getFormErrorMessage = (name) => {
        return errors[name] ? <small className="p-error">{errors[name].message}</small> : <small className="p-error">&nbsp;</small>;
    };

    const showToast = (severityValue, summaryValue, detailValue, timeLife) => {
        toast?.current?.show({ severity: severityValue, summary: summaryValue, detail: detailValue, life: timeLife });
    }

    const aceptarConsulta = (data) => {
        if (archivoJustificacion) {
            setVisibleConsultar(true)
            setCedula(data.cedula)
            setJustificacionDetalleConn(data.justificacion)
        } else {
            showToast('error', 'Error', Mensajes.ARCHIVO_DE_SUBIDA_PERFIL_RIESGO_NO_VALIDO, 6000);
        }
    }

    const realizarConsultaHistorial = () => {
        let resultadoConsultaCedula = [];
        let respuestaDeConsulta = []
        let respuestaDeConsultaAux = [];
        setArregloRespuestaFinal([])
        setArregloRespuesta([])
        setHistorialLaboral([])
        let arregloCedulas = cedula.trim().split(",");
        let contador = 0;
        ; (async () => {
            setBlocked(true)
            setLoading(true)
            let tokenApi = await ServicioWeb.obtenerToken(Variables.USER_LOGIN_API_BACKEND, Variables.PASSWORD_LOGIN_API_BACKEND, Variables.PETICION_APIS_HISTORIAL_LABORAL_TOKEN);
            for (var i = 0; i < arregloCedulas.length; i++) {
                let rowDetallado = {
                    cedula: arregloCedulas[i].trim(),
                }
                let requestBody = {
                    method: 'POST',
                    headers: {
                        Accept: "application/json",
                        'Content-Type': 'application/json',
                        'origin': 'x-request-with',
                        'Access-Control-Allow-Origin': '*',
                        'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                        'Access-Control-Allow-Credentials': 'true',
                        'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                        'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                        Authorization: tokenApi
                    },
                    body: JSON.stringify(rowDetallado)
                }
                while (resultadoConsultaCedula.length === 0 && contador <= 2) {
                    let rows = await ServicioWeb.obtenerDatosConBody(
                        Variables.PETICION_APIS_HISTORIAL_LABORAL_CONSULTAR_CEDULA_IESS,
                        requestBody
                    );
                    resultadoConsultaCedula = rows.respuesta;
                    contador++;
                    if (resultadoConsultaCedula.length !== 0) {
                        arregloRespuesta.push(rows.respuesta)
                    }
                }
                resultadoConsultaCedula = [];
                contador = 0;
            }
            if (arregloRespuesta.length !== 0) {
                for (var x = 0; x < arregloRespuesta.length; x++) {
                    let arregloOut = arregloRespuesta[x]
                    for (var y = 0; y < arregloOut.length; y++) {
                        arregloRespuestaFinal.push(arregloOut[y])
                        respuestaDeConsulta.push(JSON.stringify(arregloOut[y]))
                    }
                }
                setHistorialLaboral(arregloRespuestaFinal)
            }

            // Metodo para guardar la información //

            let tokenBackend = await ServicioWeb.obtenerToken(Variables.USER_LOGIN_API_BACKEND, Variables.PASSWORD_LOGIN_API_BACKEND, Variables.PETICION_BACKEND_ARBOL_TOKEN);
            for (var z = 0; z < respuestaDeConsulta.length; z++) {
                respuestaDeConsultaAux = respuestaDeConsultaAux + respuestaDeConsulta[z] + "|"
            }
            let formData = new FormData();
            formData.append("cedulasConsultadas", cedula.trim());
            formData.append("idUsuario", objetoSesion.idUsuario);
            formData.append("motivoConsulta", justificacionDetalleConn.trim());
            formData.append('archivoRespaldo', archivoJustificacion);
            formData.append('respuestaCedulasConsultadas', respuestaDeConsultaAux.length !== 0 ? respuestaDeConsultaAux.replace("undefined", "") : "");
            let requestBodyGuardado = {
                method: 'POST',
                headers: {
                    Authorization: tokenBackend
                },
                body: formData,
            };
            let rowsGuardado = await ServicioWeb.obtenerDatosConBody(
                Variables.PETICION_BACKEND_REGISTRO_CONSULTA_HISTORIAL_LABORAL,
                requestBodyGuardado,
            );
            if (rowsGuardado.status) {
                showToast('success', 'Success', rowsGuardado.mensaje, 6000);
            } else {
                showToast('error', 'Error', rowsGuardado.mensaje, 6000);
            }
            setLoading(false)
            setBlocked(false)
        })();
    }

    //Tipo de archivo
    const metodoSubir = (event) => {
        let file = event.files[0];
        let nombreArchivo = file.name;
        let tipoArchivo = nombreArchivo.split(".");
        if (tipoArchivo[(tipoArchivo.length) - 1].toLowerCase() === 'pdf' || tipoArchivo[(tipoArchivo.length) - 1].toLowerCase() === 'PDF') {
            setArchivoJustificacion(file)
            showToast('success', 'Correcto', Mensajes.ARCHIVO_DE_SUBIDA_PERFIL_RIESGO, 6000);
        } else {
            setArchivoJustificacion("")
            showToast('error', 'Error', Mensajes.ARCHIVO_DE_CON_FORMATO_INCORRECTO, 6000);
        }
    }

    const onTemplateRemove = (file, callback) => {
        setTotalSize(totalSize - file.size);
        setArchivoJustificacion("")
        callback();
    };

    const itemTemplate = (file, props) => {
        return (
            <div className="flex align-items-center flex-wrap">
                <div className="flex align-items-center" style={{ width: '40%' }}>
                    <img
                        alt={file.name}
                        role="presentation"
                        src={file.objectURL}
                        width={100}
                    />
                    <span className="flex flex-column text-left ml-3">
                        {file.name}
                        <small>{new Date().toLocaleDateString()}</small>
                    </span>
                </div>
                <Tag
                    value={props.formatSize}
                    severity="warning"
                    className="px-3 py-2"
                />
                <Button
                    type="button"
                    icon="pi pi-times"
                    className="p-button-outlined p-button-rounded p-button-danger ml-auto"
                    onClick={() => onTemplateRemove(file, props.onRemove)}
                />
            </div>
        );
    };

    const chooseOptionsPdf = { label: 'Escoger', icon: 'pi pi-fw pi-plus', style: { 'fontSize': '10px' } };
    const uploadOptionsPdf = { label: 'Subir', icon: 'pi pi-upload', className: 'p-button-success', style: { 'fontSize': '10px' } };
    const cancelOptionsPdf = { label: 'Cancelar', icon: 'pi pi-times', className: 'p-button-danger', style: { 'fontSize': '10px' } };

    return (
        <>
            <DrawerHeader />
            <ConfirmDialog visible={visibleConsultar} onHide={() => setVisibleConsultar(false)} message={Mensajes.ACEPTAR_CONSULTA_HISTORIAL_LABORAL}
                header="Confirmación" icon="pi pi-exclamation-triangle" accept={realizarConsultaHistorial} />
            <div className="container_consultar_historial_laboral">
                <Toast ref={toast} />
                <div className="panel_principal_consultar_historial_laboral">
                    <Panel header={<h5>Consultar Historial Laboral</h5>} className="mypanel">
                        <form onSubmit={form.handleSubmit(aceptarConsulta)} className="flex flex-column gap-2">
                            <Controller
                                name="cedula"
                                control={form.control}
                                rules={{ required: 'La/Las cédulas es obligatoria.' }}
                                render={({ field, fieldState }) => (
                                    <>
                                        <label style={{ fontSize: '10px' }} htmlFor={field.name} className={classNames({ 'p-error': errors.value })}>Ingrese la/las cédula/s: <span className='required'>*</span></label>
                                        <span className="p-float-label">
                                            <InputTextarea id={field.name} {...field}
                                                maxLength={300}
                                                rows={4}
                                                cols={100}
                                                style={{ fontSize: '10px' }}
                                                className={classNames({ 'p-invalid': fieldState.error })}
                                                autoFocus
                                                onKeyPress={(event) => {
                                                    if (!(event.which >= 48 && event.which <= 57 || event.which === 44)) {
                                                        event.preventDefault();
                                                    }
                                                }} />
                                            <label htmlFor={field.name}></label>
                                        </span>
                                        {getFormErrorMessage(field.name)}
                                    </>
                                )}
                            />
                            <Controller
                                name="justificacion"
                                control={form.control}
                                rules={{ required: 'La jutificación es obligatoria.' }}
                                render={({ field, fieldState }) => (
                                    <>
                                        <label style={{ fontSize: '10px' }} htmlFor={field.name} className={classNames({ 'p-error': errors.value })}>Justificación: <span className='required'>*</span></label>
                                        <span className="p-float-label">
                                            <InputTextarea id={field.name} {...field}
                                                maxLength={60}
                                                rows={4}
                                                cols={100}
                                                style={{ fontSize: '10px' }}
                                                className={classNames({ 'p-invalid': fieldState.error })}
                                            />
                                            <label htmlFor={field.name}></label>
                                        </span>
                                        {getFormErrorMessage(field.name)}
                                    </>
                                )}
                            />
                            <label style={{ fontSize: '10px' }}>Archivo de Justificación (.pdf): <span className='required'>*</span></label>
                            <FileUpload name="demo[]"
                                url={'/api/upload'}
                                multiple={false}
                                accept=".pdf"
                                maxFileSize={1000000}
                                customUpload
                                chooseOptions={chooseOptionsPdf} uploadOptions={uploadOptionsPdf} cancelOptions={cancelOptionsPdf}
                                uploadHandler={metodoSubir}
                                itemTemplate={itemTemplate}
                                emptyTemplate={<h5>Arrastre aqui los archivos a subir o de clic en el boton "Escoger"</h5>}
                            />
                            <br></br>
                            <Button label="Consultar" type="submit" icon="pi pi-search" loading={blocked} style={{ fontSize: '10px' }} />
                        </form>
                        <Divider />
                        <div className="tamanio_toolbar">
                            <Tooltip target=".export-buttons>button" position="bottom" />
                            <DataTable value={historialLaboral}
                                paginator
                                rows={10}
                                dataKey={(row) => row.cedula + row.razonSocial + row.periodoDesde + row.periodoHasta}
                                rowsPerPageOptions={[10, 25, 50]}
                                ref={dt}
                                filters={filters}
                                filterDisplay="row"
                                loading={loading}
                                globalFilterFields={['cedula', 'nombresCompletos', 'ruc', 'razonSocial', 'periodoDesde', 'periodoHasta']}
                                header={header}
                                emptyMessage="Información no encontrada"
                                paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                                currentPageReportTemplate="Showing {first} to {last} of {totalRecords} entries"
                                size={"large"}>

                                <Column field="cedula" sortable header="Cédula Consultado" footer="Cédula Consultado" filter filterPlaceholder="Buscar por Cédula Consultado" style={{ fontSize: '10px' }} />
                                <Column field="nombresCompletos" sortable header="Nombres Completos Consultado" footer="Nombres Completos Consultado" filter filterPlaceholder="Buscar por Nombres Completos Consultado" style={{ fontSize: '10px' }} />
                                <Column field="ruc" sortable header="Ruc Empresa" footer="Ruc Empresa" filter filterPlaceholder="Buscar por Ruc Empresa" style={{ fontSize: '10px' }} />
                                <Column field="razonSocial" sortable header="Razon Social Empresa" footer="Razon Social Empresa" filter filterPlaceholder="Buscar por Razon Social Empresa" style={{ fontSize: '10px' }} />
                                <Column field="periodoDesde" sortable header="Período Desde" footer="Período Desde" filter filterPlaceholder="Buscar por Período Desde" style={{ fontSize: '10px' }} />
                                <Column field="periodoHasta" sortable header="Período Hasta" footer="Período Hasta" filter filterPlaceholder="Buscar por Período Hasta" style={{ fontSize: '10px' }} />
                            </DataTable>
                        </div>
                    </Panel>
                </div>
            </div>
        </>
    );
}

export default ConsultarHistorialLaboral;