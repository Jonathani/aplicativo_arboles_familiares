import React, { useState, useEffect, useRef } from 'react'
import { styled } from '@mui/material/styles';
import { Panel } from 'primereact/panel';
import { Toast } from 'primereact/toast';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Tooltip } from 'primereact/tooltip';
import * as XLSX from 'xlsx';
import ReporteConsultasHistorialLaboralService from '../service/ReporteConsultasHistorialLaboralService';
import './ListarHistorialLaboral.css';

function ListarHistorialLaboral() {


    const dt = useRef(null);

    const [globalFilterValue, setGlobalFilterValue] = useState('');

    const [historialLaboral, setHistorialLaboral] = useState([]);
    const [loading, setLoading] = useState(false);

    const cargarDatosData = () => {
        ReporteConsultasHistorialLaboralService.getReporteConsultasHistorialMedium().then((data) => {
            setHistorialLaboral(getHistorialLaboral(data));
            setLoading(false);
        });
    }

    useEffect(() => {
        cargarDatosData();
    }, []);


    const getHistorialLaboral = (data) => {
        return [...(data || [])].map((d) => {
            d.date = new Date(d.date);
            return d;
        });
    };

    const [filters, setFilters] = useState({
        global: { value: null, matchMode: FilterMatchMode.CONTAINS },
        cedulasConsultadas: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        fechaConsulta: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        idConsulta: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        idUsuario: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        identificacion: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        motivoConsulta: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        nombreArchivoRespaldo: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        nombreCompleto: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        pathArchivoRespaldo: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
    });

    const onGlobalFilterChange = (e) => {
        const value = e.target.value;
        let _filters = { ...filters };
        _filters['global'].value = value;
        setFilters(_filters);
        setGlobalFilterValue(value);
    };

    const cols = [
        { field: 'identificacion', header: 'Identificación Usuario Consulta' },
        { field: 'nombreCompleto', header: 'Nombre Completo Usuario Consulta' },        
        { field: 'cedulasConsultadas', header: 'Cédulas Consultadas' },
        { field: 'motivoConsulta', header: 'Notivo Consulta' },
        { field: 'fechaConsulta', header: 'Fecha Consulta' },
    ];

    const exportColumns = cols.map((col) => ({ title: col.header, dataKey: col.field }));

    const exportCSV = (selectionOnly) => {
        dt.current.exportCSV({ selectionOnly });
    };

    const exportPdf = () => {
        import('jspdf').then((jsPDF) => {
            import('jspdf-autotable').then(() => {
                const doc = new jsPDF.default(0, 0);

                doc.autoTable(exportColumns, historialLaboral);
                doc.save('historialLaboral.pdf');
            });
        });
    };

    const saveAsExcelFile = (buffer, fileName) => {
        import('file-saver').then((module) => {
            if (module && module.default) {
                let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
                let EXCEL_EXTENSION = '.xlsx';
                const data = new Blob([buffer], {
                    type: EXCEL_TYPE
                });
                module.default.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
            }
        });
    };

    const exportExcel = () => {
        import('xlsx').then((xlsx) => {
            const worksheet = xlsx.utils.json_to_sheet(historialLaboral);
            const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
            const excelBuffer = xlsx.write(workbook, {
                bookType: 'xlsx',
                type: 'array'
            });
            saveAsExcelFile(excelBuffer, 'historialLaboral');
        });
    };

    const header = (
        <div className="flex justify-content-end">
            <span className="p-input-icon-left">
                <i className="pi pi-search" />
                <InputText style={{fontSize: '10px'}}  value={globalFilterValue} onChange={onGlobalFilterChange} placeholder="Buscar" />
            </span>
            <br></br>
            <Button type="button" style={{fontSize: '10px'}} icon="pi pi-file" rounded onClick={() => exportCSV(false)} data-pr-tooltip="CSV" label="CSV" />
            <Button type="button" style={{fontSize: '10px'}} icon="pi pi-file-excel" severity="success" rounded onClick={exportExcel} data-pr-tooltip="XLS" label="XLSX" />
            <Button type="button" style={{fontSize: '10px'}} icon="pi pi-file-pdf" severity="warning" rounded onClick={exportPdf} data-pr-tooltip="PDF" label="PDF" />
        </div>
    );

    

    const DrawerHeader = styled('div')(({ theme }) => ({
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    }));

    const toast = useRef(null);

    const monstrarAdjunto = (rowData) => {
        let link;
        link = document.createElement('a');
        link.href = rowData.pathArchivoRespaldo.replace("/public", "") + "/" + rowData.nombreArchivoRespaldo;     
        link.setAttribute("target", "_blank");
        link.click();
    }

    const visualizarAdjunto = (rowData) => {
        return (
            <React.Fragment>
                <Button icon="pi pi-file-export" style={{fontSize: '10px'}} rounded outlined className="mr-2" label="Visualizar Adjunto" onClick={() => monstrarAdjunto(rowData)} />
            </React.Fragment>
        );
    }

    const mostrarRespuestaHistorial = (rowData) => {
        let nombreArchivo = rowData.nombreArchivoRespaldo.split(".");
        let arrayObjecto = [];
        let respuestaConsultada = rowData.respuestaCedulasConsultadas.split("|");
        for (var i = 0; i < respuestaConsultada.length; i++) {
            if (respuestaConsultada[i] !== "") {
                arrayObjecto.push(JSON.parse(respuestaConsultada[i]))
            }
        }
        const worksheet = XLSX.utils.json_to_sheet(arrayObjecto);
        const workbook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workbook, worksheet, "Sheet1");
        XLSX.writeFile(workbook, nombreArchivo[0] + ".xlsx");
    }

    const visualizarRespuesta = (rowData) => {
        return (
            <React.Fragment>
                <Button icon="pi pi-table" style={{fontSize: '10px'}} rounded outlined className="mr-2" label="Visualizar Respuesta" onClick={() => mostrarRespuestaHistorial(rowData)} />
            </React.Fragment>
        );
    }

    return (
        <>
            <DrawerHeader />
            <div className="container_listar_historial_laboral">
                <Toast ref={toast} />
                <div className="panel_principal_listar_historial_laboral">
                    <Panel header={<h5>Consultar Historial Laboral</h5>} className="mypanel">
                        <div className="panel_contenedor_listar_historial_laboral">
                            <div className="tamanio_toolbar">
                                <Tooltip target=".export-buttons>button" position="bottom" />
                                <DataTable value={historialLaboral} paginator rows={10}
                                    dataKey="idConsulta"
                                    rowsPerPageOptions={[10, 25, 50]}
                                    ref={dt}
                                    filters={filters}
                                    filterDisplay="row"
                                    loading={loading}
                                    globalFilterFields={['identificacion', 'nombreCompleto', 'cedulasConsultadas', 'motivoConsulta', 'fechaConsulta']}
                                    header={header}
                                    emptyMessage="Información no encontrada"
                                    paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                                    currentPageReportTemplate="Showing {first} to {last} of {totalRecords} entries"
                                    tableStyle={{ minWidth: '50rem' }}
                                    size={"large"}>

                                    <Column field="identificacion" sortable header="Identificación Usuario Consulta" footer="Identificación Usuario Consulta" filter filterPlaceholder="Buscar por Identificación Usuario Consulta" style={{fontSize: '10px'}} />
                                    <Column field="nombreCompleto" sortable header="Nombre Completo Usuario Consulta" footer="Nombre Completo Usuario Consulta" filter filterPlaceholder="Buscar por Nombre Completo Usuario Consulta" style={{ fontSize: '10px' }} />
                                    <Column field="cedulasConsultadas" sortable header="Cédulas Consultadas" footer="Cédulas Consultadas" filter filterPlaceholder="Buscar por Cédulas Consultadas" style={{ fontSize: '10px' }} />
                                    <Column field="motivoConsulta" sortable header="Motivo Consulta" footer="Motivo Consulta" filter filterPlaceholder="Buscar por Motivo Consulta" style={{ fontSize: '10px' }} />
                                    <Column field="fechaConsulta" sortable header="Fecha Consulta" footer="Fecha Consulta" filter filterPlaceholder="Buscar por Fecha Consulta" style={{ fontSize: '10px' }} />
                                    <Column body={visualizarRespuesta} header="Visualizar Respuesta" footer="Visualizar Respuesta"  exportable={false} style={{ fontSize: '10px' }}></Column>
                                    <Column body={visualizarAdjunto} header="Visualizar Adjunto" footer="Visualizar Adjunto"  exportable={false} style={{ fontSize: '10px' }}></Column>
                                </DataTable>
                            </div>
                        </div>
                    </Panel>
                </div>
            </div>
        </>
    );
}

export default ListarHistorialLaboral;