class Utilitario {

    static ordenarArbolesFamiliares(infoArboles) {

        let informacionArboles = [];

        for (var i = 0; i < infoArboles.length; i++) {
            let dato = {
                cedula: infoArboles[i].cedula,
                nombresUsuario: infoArboles[i].nombresUsuario,
                fechaNacimiento: infoArboles[i].fechaNacimiento,
                estadoCivil: infoArboles[i].estadoCivil,
                cedulaPadre: infoArboles[i].cedulaPadre,
                nombresPadre: infoArboles[i].nombresPadre,
                cedulaMadre: infoArboles[i].cedulaMadre,
                nombresMadre: infoArboles[i].nombresMadre,
                cedulaConyuge: infoArboles[i].cedulaConyuge,
                nombreConyuge: infoArboles[i].nombreConyuge,
                nivel: infoArboles[i].nivel,
                tipo: infoArboles[i].tipo,
                identificacionUsuarioConsultado: infoArboles[i].identificacionUsuarioConsultado,
                nombresUsuarioConsultado: infoArboles[i].nombresUsuarioConsultado
            }
            informacionArboles.push(dato)
        }
        return informacionArboles;
    }

    static exportarSoloDataExcel (dataExcel){
        let informacionExcel = [];
        for (var i = 0; i < dataExcel.length; i++) {
            let row = {
                identificacion: dataExcel[i].identificacion,
                nombreCompleto: dataExcel[i].nombreCompleto,
                cedulasConsultadas: dataExcel[i].cedulasConsultadas,
                motivoConsulta: dataExcel[i].motivoConsulta,
                fechaConsulta: dataExcel[i].fechaConsulta
            }
            informacionExcel.push(row)
        }
        return informacionExcel;
    }

} export default Utilitario;