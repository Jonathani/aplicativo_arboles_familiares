class Variables {

    // Tamaño del archivo
    static TAMANIO_DEL_ARCHIVO = 2000000;

    // Variables de acceso a las apis
    static USER_LOGIN_API_BACKEND = "abc";
    
    static PASSWORD_LOGIN_API_BACKEND = "12345";

    // Consulta al api de árboles familiares
    static PETICION_ARBOL_TOKEN ='http://localhost:5005/authenticate';

    static PETICION_ARBOL_CONSULTA='http://localhost:5005/api/consulta/arbolesFamiliaresNOSQL';

    // Consulta al backend transaccional de arboles familiares y entre otros
    static PETICION_BACKEND_ARBOL_TOKEN='/api/arbolesFamiliares/authentication';

    static PETICION_BACKEND_ARBOL_INFO_SESION='/api/arbolesFamiliares/infoSesionUsuario';

    static PETICION_BACKEND_ARBOL_ACTUALIZAR_CONTRASENIA='/api/arbolesFamiliares/actualizarContrasenia';

    static PETICION_BACKEND_LISTA_USUARIOS='/api/arbolesFamiliares/listarUsuariosSistema';

    static PETICION_BACKEND_LISTAR_PERFILES_MENUS_USUARIO='/api/arbolesFamiliares/menusPerfilUsuario';

    static PETICION_BACKEND_ARBOL_LISTA_PERFILES='/api/arbolesFamiliares/listarPerfilesSistema';

    static PETICION_BACKEND_ARBOL_MENUS_USUARIO='/api/arbolesFamiliares/menusDelUsuario';

    static PETICION_BACKEND_ARBOL_LISTAR_MENUS='/api/arbolesFamiliares/mostrarTodosLosMenus';
                                             
    static PETICION_BACKEND_ARBOL_NUEVO_USUARIO='/api/arbolesFamiliares/crearUsuario';
    
    static PETICION_BACKEND_ARBOL_ACTUALIZAR_INFO_USUARIO='/api/arbolesFamiliares/actualizarInfoUsuario';
    
    static PETICION_BACKEND_ARBOL_MOSTRAR_INO_USUARIO='/api/arbolesFamiliares/mostrarInfoUsuario';
    
    static PETICION_BACKEND_ARBOL_REGISTRAR_CONSULTA='/api/arbolesFamiliares/registrarConsulta';
    
    static PETICION_BACKEND_ARBOL_REPORTE_CONSULTA='/api/arbolesFamiliares/mostrarConsulta';

    static PETICION_BACKEND_ARBOL_REPORTE_CONSULTA_DETALLADO='/api/arbolesFamiliares/mostrarConsultaDet';

    static PETICION_BACKEND_ARBOL_RECUPERAR_CONTRASENIA='/api/arbolesFamiliares/recuperarContrasenia';

    static PETICION_BACKEND_GENERACION_PERFIL_DE_RIESGO = '/api/perfilDeRiesgo/generacionPerfilRiesgo';

    static PETICION_BACKEND_LISTAR_PERFILES_DE_RIESGO = '/api/perfilDeRiesgo/listarPerfilRiesgo';

    static PETICION_BACKEND_REGISTRO_CONSULTA_HISTORIAL_LABORAL = '/api/historialLaboral/registrarHistorialLaboral';

    static PETICION_BACKEND_LISTAR_CONSULTA_HISTORIAL_LABORAL = '/api/historialLaboral/listarHistorialLaboral';

    static PETICION_BACKEND_TRAER_ARCHIVO_REPORTE_PERFIL = '/api/perfilDeRiesgo/archivoPerfilRiesgo';

    // Apis para ejecución de etls pentaho
    static PETICION_APIS_ETLS_TOKEN ='http://localhost:8084/authenticate';

    static PETICION_APIS_EJECUCION_ETL='http://localhost:8084/api/etlRiesgoFacticoController/ejecutarRiesgoFactico';

    static PETICION_APIS_MOSTRAR_VARIABLES_CONSULTA='http://localhost:8084/api/variablesConsultaController/mostrarVariablesConsulta';

    static PETICION_APIS_MOSTRAR_CONSORCIO='http://localhost:8084/api/consorcioController/mostrarConsorcio';

    static PETICION_APIS_MOSTRAR_AUTORIDADES='http://localhost:8084/api/autoridadesController/mostrarAutoridades';

    static PETICION_APIS_MOSTRAR_COMISION='http://localhost:8084/api/comisionController/mostrarComision';

    static PETICION_APIS_MOSTRAR_OFERTAS='http://localhost:8084/api/ofertasController/mostrarOfertas';

    static PETICION_APIS_MOSTRAR_ACCIONISTAS_SUPERCIAS_DE='http://localhost:8084/api/accionistasSuperciasDeController/mostrarAccionistasSuperciasDe';

    static PETICION_APIS_MOSTRAR_ACCIONISTAS_SOCE_DE='http://localhost:8084/api/accionistasSoceDeController/mostrarAccionistasSoceDe';

    static PETICION_APIS_MOSTRAR_ARBOLES_FAMILIARES='http://localhost:8084/api/arbolesFamiliaresController/mostrarArbolesFamiliares';

    static PETICION_APIS_MOSTRAR_ACCIONISTAS_SUPERCIAS_EN='http://localhost:8084/api/accionistasSuperciasEnController/mostrarAccionistasSuperciasEn';

    static PETICION_APIS_MOSTRAR_DIRECCIONES_COMERCIALES_SOCE='http://localhost:8084/api/direccionesComercialSoceController/mostrarDireccionesComercialesSoce';

    static PETICION_APIS_MOSTRAR_DIRECCIONES_COMERCIALES_SRI='http://localhost:8084/api/direccionesComercialesSriController/mostrarDireccionesComercialesSri';

    // Apis consultar rucs
    static PETICION_APIS_SRI_TOKEN ='http://localhost:8083/authenticate';

    static PETICION_APIS_SRI_CONSULTAR_RUC_CONTRIBUYENTES = 'http://localhost:8083/api/dirComercialesController/consultarRucContribuyentes';

    // Apis consulta Historial Laboral

    static PETICION_APIS_HISTORIAL_LABORAL_TOKEN ='http://localhost:5015/api/consultaApiSriIess/authentication';

    static PETICION_APIS_HISTORIAL_LABORAL_CONSULTAR_CEDULA_IESS = 'http://localhost:5015//api/apiConsultaIESS/consultarCedulaIESS';

    //Apis de perfil de Riesgo
    static PETICION_PERFIL_DE_RIESGO = "http://localhost:8081/perfil_request";

} export default Variables;
