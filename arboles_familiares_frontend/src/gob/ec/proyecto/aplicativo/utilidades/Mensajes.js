class Mensajes {

    //Pantalla de Cambiar Contraseña
    static MENSAJE_CONTRASENIAS_NO_IGUALES = "Estimado Usuario, la contraseña nueva y la contraseña a repetir deben ser iguales"

    //Pantalla de Recuperar Contraseña
    static MENSAJE_ERROR_RECUPERAR_CONTRASENIA = "Estimado Usuario, debe ingresar un correo valido";

    //Mensajes para registrar un usuario
    static ACEPTAR_GUARDADO_USUARIO_NUEVO = "¿Está seguro que desea guardar, una vez guardado no se podrá modificar los datos?";

    //Mensajes para editar un usuario
    static ACEPTAR_GUARDADO_USUARIO_EDITAR = "¿Está seguro que desea guardar?";

    //Mensajes para guardar de editar usuario
    static MENSAJE_PARA_EDITAR_GUARDAR_DATOS = "Estimado Usuario, debe ingresar todos los datos para continuar con el guardado";

    //Mensaje del tamaño del archivo
    static MENSAJE_ARCHIVO_SIN_TAMANIO_PERMITIDO = "Estimado Usuario, el archivo que está tratando de subir no debe superar los 2 MB"; 

    //Mensaje para guardar el riesgo
    static ACEPTAR_GUARDADO_GENERICO = "¿Está seguro que desea guardar, una vez guardado no se podrá modificar los datos?";

    //Mensaje para consultar Ruc
    static ACEPTAR_CONSULTA_RUC = "¿Está seguro que desea consultar el RUC ingresado?";

    //Mensaje para aceptar generación de perfil
    static ACEPTAR_GENERACION_PERFIL_DE_RIESGO = "¿Está seguro que desea generar el Perfil de Riesgo?";

    //Mensaje para consultar arbol familiar
    static ACEPTAR_CONSULTA_ARBOL_FAMILIAR = "¿Está seguro que desea consultar el arbol familiar de la/s cédula/s ingresada/s?";

    //Mensaje para validar todos los datos en solicitar registro de perfil de riesgo
    static VALIDAR_GUARDADO_PERFIL_DE_RIESGO = "Estimado Usuario, debe estar llenos todos los datos del ruc.";

    //Mensaje de archivo de subida
    static ARCHIVO_DE_SUBIDA_PERFIL_RIESGO = "El archivo es válido";

    //Mensaje de archivo de subida es obligatorio a subir
    static ARCHIVO_DE_SUBIDA_PERFIL_RIESGO_NO_VALIDO = "El archivo es obligatorio a subir";

    //Mensaje de archivo de subida exitoso
    static ARCHIVO_DE_CON_FORMATO_INCORRECTO = "Archivo con formato incorrecto y/o tamaño inválido";

    //Mensaje para consultar 
    static ACEPTAR_CONSULTA_HISTORIAL_LABORAL = "¿Está seguro que desea consultar el Historial Laboral de la/las cédulas ingresadas?";

} export default Mensajes;
