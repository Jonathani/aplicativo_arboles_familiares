import React, { useState, useEffect, useRef } from 'react'
import { styled } from '@mui/material/styles';
import Cookies from 'js-cookie';
import { Toast } from 'primereact/toast';
import { Toolbar } from 'primereact/toolbar';
import { Button } from 'primereact/button';
import { Tooltip } from 'primereact/tooltip';
import { Column } from 'primereact/column';
import { DataTable } from 'primereact/datatable';
import { FilterMatchMode } from 'primereact/api';
import { InputText } from 'primereact/inputtext';
import { Dialog } from 'primereact/dialog';
import { ConfirmDialog } from 'primereact/confirmdialog';
import { Checkbox } from 'primereact/checkbox';
import { Dropdown } from 'primereact/dropdown';
import { TreeSelect } from 'primereact/treeselect';
import GestionUsuariosService from '../service/GestionUsuariosService';
import Variables from '../utilidades/Variables';
import ServicioWeb from '../utilidades/ServicioWeb';
import './GestionUsuarios.css'
import "primeicons/primeicons.css";
import Mensajes from '../utilidades/Mensajes';


function GestionUsuarios() {

    const DrawerHeader = styled('div')(({ theme }) => ({
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    }));

    let informacionSesionUsuario = Cookies.get('infoSesionUsuario');
    let objetoSesion = !!informacionSesionUsuario ? JSON.parse(informacionSesionUsuario) : "";

    let emptyUsuario = {
        idUsuario: null,
        correo: "",
        descripcionPerfil: "",
        estadoUsuario: "",
        idPerfil: "",
        idPerfilUsuario: "",
        identificacionUsuario: "",
        nombresUsuario: "",
    };

    //Para editar Usuario
    const [identificacion, setIdentificacion] = useState('');
    const [nombresCompletosEditar, setNombresCompletosEditar] = useState('');
    const [correoEditar, setCorreoEditar] = useState('');
    const [perfilEditar, setPerfilEditar] = useState(0);
    const [perfilEditSelected, setPerfilEditSelected] = useState([]);

    //Para un nuevo Usuario variables
    const [identificacionNuevo, setIdentificacionNuevo] = useState('');
    const [nombresCompletosNuevo, setNombresCompletosNuevo] = useState('');
    const [correoNuevo, setCorreoNuevo] = useState('');
    const [selectedPerfil, setSelectedPerfil] = useState(null);

    //Para crear Usuario
    const [checked, setChecked] = useState(false);
    const [valorMenus, setValorMenus] = useState(true);
    const [visibleGuardar, setVisibleGuardar] = useState(false);
    const [usuarioEditarDialog, setUsuarioEditarDialog] = useState(false);
    const [usuario, setUsuario] = useState(emptyUsuario);
    const [usuarioCrearDialog, setUsuarioCrearDialog] = useState(false);
    const [perfilesUsuario, setPerfilesUsuario] = useState([]);
    const [menusSistema, setMenusSistema] = useState([]);
    const [menusSistemaSelec, setMenusSistemaSelec] = useState([]);
    const [usuarios, setUsuarios] = useState(null);
    const [loading, setLoading] = useState(true);
    const [globalFilterValue, setGlobalFilterValue] = useState('');
    const [saving, setSaving] = useState(false);
    const [filters, setFilters] = useState({
        global: { value: null, matchMode: FilterMatchMode.CONTAINS },
        identificacionUsuario: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        nombresUsuario: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        correo: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        descripcionPerfil: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        estadoUsuario: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
    });

    const showToast = (severityValue, summaryValue, detailValue, timeLife) => {
        toast?.current?.show({ severity: severityValue, summary: summaryValue, detail: detailValue, life: timeLife });
    }

    const ocultarDialogConfirm = () => {
        setVisibleGuardar(false);
    }

    const toast = useRef(null);
    const dt = useRef(null);

    const onGlobalFilterChange = (e) => {
        const value = e.target.value;
        let _filters = { ...filters };
        _filters['global'].value = value;
        setFilters(_filters);
        setGlobalFilterValue(value);
    };

    const cambiarPerfil = (valor, tipoOperacion) => {
        let todosLosMenus = {}
        if (tipoOperacion === 1) {//Crear
            setSelectedPerfil(valor);
        } else if (tipoOperacion === 2) { //Editar
            setPerfilEditSelected(valor);
        }

        if (valor.idPerfil === "1") {//Administrador
            setValorMenus(true)
            for (var menuPrincipal of menusSistema) {
                let objetoMenu = {
                    [menuPrincipal.key]:
                    {
                        "checked": true,
                        "partialChecked": false
                    }
                }
                Object.assign(todosLosMenus, objetoMenu);
                if (menuPrincipal.children.length !== 0) {
                    for (var menuSecundario of menuPrincipal.children) {
                        let objetoMenu = {
                            [menuSecundario.key]:
                            {
                                "checked": true,
                                "partialChecked": false
                            }
                        }
                        Object.assign(todosLosMenus, objetoMenu);
                    }
                }
            }
            setMenusSistemaSelec(todosLosMenus)
        } else {//Analista
            setValorMenus(false)
            setMenusSistemaSelec(null)
        }
    }

    const cols = [
        { field: 'identificacionUsuario', header: 'Identificacion' },
        { field: 'nombresUsuario', header: 'Nombre Completo' },
        { field: 'correo', header: 'Correo' },
        { field: 'descripcionPerfil', header: 'Descripción Perfil' },
        { field: 'estadoUsuario', header: 'Estado Usuario' }
    ];

    const exportColumns = cols.map((col) => ({ title: col.header, dataKey: col.field }));

    const exportCSV = (selectionOnly) => {
        dt.current.exportCSV({ selectionOnly });
    };

    const exportPdf = () => {
        import('jspdf').then((jsPDF) => {
            import('jspdf-autotable').then(() => {
                const doc = new jsPDF.default(0, 0);

                doc.autoTable(exportColumns, usuarios);
                doc.save('usuarios.pdf');
            });
        });
    };

    const exportExcel = () => {
        import('xlsx').then((xlsx) => {
            const worksheet = xlsx.utils.json_to_sheet(usuarios);
            const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
            const excelBuffer = xlsx.write(workbook, {
                bookType: 'xlsx',
                type: 'array'
            });
            saveAsExcelFile(excelBuffer, 'usuarios');
        });
    };

    const cargarDatosData = () => {
        GestionUsuariosService.getGestionUsuariosMedium().then((data) => {
            setUsuarios(getUsuarios(data));
            setLoading(false);
        });
    }

    useEffect(() => {
        cargarDatosData();
    }, []);


    const getUsuarios = (data) => {
        return [...(data || [])].map((d) => {
            d.date = new Date(d.date);
            return d;
        });
    };

    const header = (
        <div className="flex justify-content-end">
            <span className="p-input-icon-left">
                <i className="pi pi-search" />
                <InputText value={globalFilterValue} onChange={onGlobalFilterChange} placeholder="Buscar" />
            </span>
            <br></br>
            <Button type="button" icon="pi pi-file" style={{ fontSize: '10px' }} rounded onClick={() => exportCSV(false)} data-pr-tooltip="CSV" label="CSV" />
            <Button type="button" icon="pi pi-file-excel" style={{ fontSize: '10px' }} severity="success" rounded onClick={exportExcel} data-pr-tooltip="XLS" label="XLSX" />
            <Button type="button" icon="pi pi-file-pdf" style={{ fontSize: '10px' }} severity="warning" rounded onClick={exportPdf} data-pr-tooltip="PDF" label="PDF" />
        </div>
    );

    const saveAsExcelFile = (buffer, fileName) => {
        import('file-saver').then((module) => {
            if (module && module.default) {
                let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
                let EXCEL_EXTENSION = '.xlsx';
                const data = new Blob([buffer], {
                    type: EXCEL_TYPE
                });
                module.default.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
            }
        });
    };

    const guardarNuevoUsuarioDialog = () => {
        setValorMenus(true)
        setIdentificacionNuevo("")
        setNombresCompletosNuevo("")
        setCorreoNuevo("")
        setUsuarioCrearDialog(true)
            ; (async () => {
                let tokenApi = await ServicioWeb.obtenerToken(Variables.USER_LOGIN_API_BACKEND, Variables.PASSWORD_LOGIN_API_BACKEND, Variables.PETICION_BACKEND_ARBOL_TOKEN);
                let requestBody = {
                    method: 'POST',
                    headers: {
                        Accept: "application/json",
                        'Content-Type': 'application/json',
                        'origin': 'x-request-with',
                        'Access-Control-Allow-Origin': '*',
                        'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                        'Access-Control-Allow-Credentials': 'true',
                        'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                        'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                        Authorization: tokenApi
                    }
                }
                //Perfiles
                let rows = await ServicioWeb.obtenerDatosConBody(
                    Variables.PETICION_BACKEND_ARBOL_LISTA_PERFILES,
                    requestBody
                );
                if (rows.mensaje.length !== 0) {
                    setPerfilesUsuario(rows.mensaje);
                }
                //Menus
                let rowsMenus = await ServicioWeb.obtenerDatosConBody(
                    Variables.PETICION_BACKEND_ARBOL_LISTAR_MENUS,
                    requestBody
                );
                if (rowsMenus.mensaje.length !== 0) {
                    setMenusSistema(rowsMenus.mensaje)
                }
            })()
    }

    const startContent = (
        <React.Fragment>
            <Button style={{ fontSize: '10px' }} label="Nuevo Usuario" icon="pi pi-plus" className="mr-2" onClick={guardarNuevoUsuarioDialog} />
        </React.Fragment>
    );

    const actionBodyTemplate = (rowData) => {
        return (
            <React.Fragment>
                <Button style={{ fontSize: '10px' }} icon="pi pi-pencil" rounded outlined className="mr-2" label="Editar" onClick={() => editarUsuario(rowData)} />
            </React.Fragment>
        );
    };

    const setearMenusUsuario = (menusUsuario, idPerfil) => {
        setValorMenus(true)
        let todosLosMenus = {}
        for (var menuPrincipal of menusUsuario) {
            let objetoMenu = {
                [menuPrincipal.idMenu]:
                {
                    "checked": true,
                    "partialChecked": false
                }
            }
            Object.assign(todosLosMenus, objetoMenu);
            if (menuPrincipal.menusHijos.length !== 0) {
                for (var menuSecundario of menuPrincipal.menusHijos) {
                    let objetoMenu = {
                        [menuSecundario.idMenu]:
                        {
                            "checked": true,
                            "partialChecked": false
                        }
                    }
                    Object.assign(todosLosMenus, objetoMenu);
                }
            }
        }
        setMenusSistemaSelec(todosLosMenus)
        if (idPerfil === "1") {//Administrador
            setValorMenus(true)
        } else if (idPerfil === "2") {//Analista
            setValorMenus(false)
        }
    }

    //Editar Usuario 
    const editarUsuario = (usuarios) => {
        setValorMenus(true)
        let idUsuario = usuarios.idUsuario;
        let idPerfil = usuarios.idPerfil;
        let estadoDelUsuario = usuarios.estadoUsuario === "Activo" ? true : false;
        setChecked(estadoDelUsuario);
        setIdentificacion(usuarios.identificacionUsuario)
        setNombresCompletosEditar(usuarios.nombresUsuario)
        setCorreoEditar(usuarios.correo)
        setUsuario({ ...usuarios });
        setUsuarioEditarDialog(true);

        ; (async () => {
            let tokenApi = await ServicioWeb.obtenerToken(Variables.USER_LOGIN_API_BACKEND, Variables.PASSWORD_LOGIN_API_BACKEND, Variables.PETICION_BACKEND_ARBOL_TOKEN);
            let requestBody = {
                method: 'POST',
                headers: {
                    Accept: "application/json",
                    'Content-Type': 'application/json',
                    'origin': 'x-request-with',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                    'Access-Control-Allow-Credentials': 'true',
                    'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                    Authorization: tokenApi
                }
            }
            let rows = await ServicioWeb.obtenerDatosConBody(
                Variables.PETICION_BACKEND_ARBOL_LISTA_PERFILES,
                requestBody
            );
            if (rows.mensaje.length !== 0) {
                setPerfilesUsuario(rows.mensaje);
                for (var i = 0; i < rows.mensaje.length; i++) {
                    if (rows.mensaje[i].idPerfil === usuarios.idPerfil) {
                        setPerfilEditar(i)
                        setPerfilEditSelected(perfilesUsuario[i])
                    }
                }
            }
            //Menus Sistema
            let rowsMenus = await ServicioWeb.obtenerDatosConBody(
                Variables.PETICION_BACKEND_ARBOL_LISTAR_MENUS,
                requestBody
            );
            if (rowsMenus.mensaje.length !== 0) {
                setMenusSistema(rowsMenus.mensaje)
            }

            //Menus de Usuario
            let rowDetallado = {
                idUsuario: idUsuario
            }
            let requestBodyMenus = {
                method: 'POST',
                headers: {
                    Accept: "application/json",
                    'Content-Type': 'application/json',
                    'origin': 'x-request-with',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                    'Access-Control-Allow-Credentials': 'true',
                    'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                    Authorization: tokenApi
                },
                body: JSON.stringify(rowDetallado)
            }
            let rowMenusUsuario = await ServicioWeb.obtenerDatosConBody(
                Variables.PETICION_BACKEND_ARBOL_MENUS_USUARIO,
                requestBodyMenus
            );
            setearMenusUsuario(rowMenusUsuario.mensaje, idPerfil)
        })()
    }

    const editarGuardarUsuario = () => {
        let idPerfil = typeof perfilEditSelected === 'undefined' ? usuario.idPerfil : perfilEditSelected.idPerfil;
        if (nombresCompletosEditar.trim().length !== 0 && correoEditar.trim().length !== 0) {
            let validEmail = /^\w+([.-_+]?\w+)*@\w+([.-]?\w+)*(\.\w{2,10})+$/;
            if (validEmail.test(correoEditar.trim())) {
                ; (async () => {
                    setSaving(true)
                    let tokenApi = await ServicioWeb.obtenerToken(Variables.USER_LOGIN_API_BACKEND, Variables.PASSWORD_LOGIN_API_BACKEND, Variables.PETICION_BACKEND_ARBOL_TOKEN);
                    let rowDetallado = {
                        identificacion: identificacion.trim(),
                        nombresCompletos: nombresCompletosEditar.trim(),
                        correo: correoEditar.trim(),
                        estado: checked,
                        idPerfil: idPerfil,
                        menusSeleccionados: Object.keys(menusSistemaSelec).toString()
                    }
                    let requestBody = {
                        method: 'POST',
                        headers: {
                            Accept: "application/json",
                            'Content-Type': 'application/json',
                            'origin': 'x-request-with',
                            'Access-Control-Allow-Origin': '*',
                            'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                            'Access-Control-Allow-Credentials': 'true',
                            'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                            'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                            Authorization: tokenApi
                        },
                        body: JSON.stringify(rowDetallado)
                    }
                    let rows = await ServicioWeb.obtenerDatosConBody(
                        Variables.PETICION_BACKEND_ARBOL_ACTUALIZAR_INFO_USUARIO,
                        requestBody
                    );
                    if (rows.status) {
                        setSaving(false);
                        cargarDatosData();
                        hideDialogEditar();
                        showToast('success', 'Success', rows.mensaje, 6000);
                    } else {
                        setSaving(false);
                        showToast('error', 'Error', rows.mensaje, 6000);
                    }
                })()
            } else {
                showToast('error', 'Error', Mensajes.MENSAJE_ERROR_RECUPERAR_CONTRASENIA, 6000);
            }
        } else {
            showToast('error', 'Error', Mensajes.MENSAJE_PARA_EDITAR_GUARDAR_DATOS, 6000);
        }
    }

    const hideDialogEditar = () => {
        setUsuarioEditarDialog(false);
        setUsuarioCrearDialog(false);
    };

    const aceptarGuardado = () => {
        setVisibleGuardar(true);
    }

    const usuarioDialogFooterEdit = (
        <>
            <Toast ref={toast} />
            <ConfirmDialog visible={visibleGuardar} onHide={() => setVisibleGuardar(false)} message={Mensajes.ACEPTAR_GUARDADO_USUARIO_EDITAR}
                header="Confirmación" icon="pi pi-exclamation-triangle" accept={editarGuardarUsuario} reject={ocultarDialogConfirm} />
            <React.Fragment>
                <Button label="Cancelar" style={{ fontSize: '10px' }} icon="pi pi-times" outlined onClick={hideDialogEditar} />
                <Button label="Guardar" style={{ fontSize: '10px' }} loading={saving} icon="pi pi-check" onClick={aceptarGuardado} />
            </React.Fragment>
        </>
    );

    //Nuevo Usuario
    const aceptarGuardarNuevoUsuario = () => {
        if (identificacionNuevo.trim().length !== 0 && correoNuevo.trim().length !== 0 && nombresCompletosNuevo.trim().length !== 0 && Object.keys(menusSistemaSelec).length !== 0) {
            let validEmail = /^\w+([.-_+]?\w+)*@\w+([.-]?\w+)*(\.\w{2,10})+$/;
            if (validEmail.test(correoNuevo.trim())) {
                setVisibleGuardar(true);
            }
        } else {
            showToast('error', 'Error', Mensajes.MENSAJE_PARA_EDITAR_GUARDAR_DATOS, 6000);
        }
    }

    const guardarDatosNuevoUsuario = () => {
        ; (async () => {
            setSaving(true)
            let tokenApi = await ServicioWeb.obtenerToken(Variables.USER_LOGIN_API_BACKEND, Variables.PASSWORD_LOGIN_API_BACKEND, Variables.PETICION_BACKEND_ARBOL_TOKEN);
            let rowDetallado = {
                identificacion: identificacionNuevo.trim(),
                nombresCompletos: nombresCompletosNuevo.trim(),
                correo: correoNuevo.trim(),
                perfilId: selectedPerfil.idPerfil,
                menusSeleccionados: Object.keys(menusSistemaSelec).toString()
            }
            let requestBody = {
                method: 'POST',
                headers: {
                    Accept: "application/json",
                    'Content-Type': 'application/json',
                    'origin': 'x-request-with',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                    'Access-Control-Allow-Credentials': 'true',
                    'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                    Authorization: tokenApi
                },
                body: JSON.stringify(rowDetallado)
            }
            let rows = await ServicioWeb.obtenerDatosConBody(
                Variables.PETICION_BACKEND_ARBOL_NUEVO_USUARIO,
                requestBody
            );
            if (rows.status) {
                setSaving(false);
                cargarDatosData();
                hideDialogEditar();
                showToast('success', 'Success', rows.mensaje, 6000);
            } else {
                setSaving(false);
                showToast('error', 'Error', rows.mensaje, 6000);
            }
        })();
    }

    const usuarioDialogFooterNuevo = (
        <>
            <Toast ref={toast} />
            <ConfirmDialog visible={visibleGuardar} onHide={() => setVisibleGuardar(false)} message={Mensajes.ACEPTAR_GUARDADO_USUARIO_EDITAR}
                header="Confirmación" icon="pi pi-exclamation-triangle" accept={guardarDatosNuevoUsuario} reject={ocultarDialogConfirm} />
            <React.Fragment>
                <Button label="Cancelar" style={{ fontSize: '10px' }} icon="pi pi-times" outlined onClick={hideDialogEditar} />
                <Button label="Guardar" style={{ fontSize: '10px' }} loading={saving} icon="pi pi-check" onClick={aceptarGuardarNuevoUsuario} />
            </React.Fragment>
        </>
    );

    return (
        <>
            <DrawerHeader />
            <Toast ref={toast} />
            <div className='container_gestion_de_usuarios'>
                <>
                    <Toolbar start={startContent} />
                </>
                <div className="tamanio_toolbar">
                    <Tooltip target=".export-buttons>button" position="bottom" />
                    <DataTable value={usuarios} 
                        paginator 
                        rows={10}
                        dataKey="idUsuario"
                        rowsPerPageOptions={[10, 25, 50]}
                        ref={dt}
                        filters={filters}
                        scrollable="true"
                        filterDisplay="row"
                        loading={loading}
                        globalFilterFields={['identificacionUsuario', 'nombresUsuario', 'estadoUsuario', 'correo', 'descripcionPerfil']}
                        header={header}
                        action={<GestionUsuarios loadParameter={cargarDatosData} />}
                        emptyMessage="Usuarios no encontrados"
                        style={{ fontSize: '10px' }}
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} entries">

                        <Column field="identificacionUsuario" sortable header="Identificación" footer="Identificación" filter filterPlaceholder="Buscar por Identificación" style={{ fontSize: '10px' }} />
                        <Column field="nombresUsuario" sortable header="Nombres Completos" footer="Nombres Completos" filter filterPlaceholder="Buscar por Nombres Completos" style={{ fontSize: '10px' }} />
                        <Column field="correo" sortable header="Correo" footer="Nombres Completos" filter filterPlaceholder="Buscar por Nombres Completos" style={{ fontSize: '10px' }} />
                        <Column field="descripcionPerfil" sortable header="Descripción Perfil" footer="Descripción Perfil" filter filterPlaceholder="Buscar por Descripción Perfil" style={{ fontSize: '10px' }} />
                        <Column field="estadoUsuario" sortable header="Estado Usuario" footer="Estado Usuario" filter filterPlaceholder="Buscar por Estado Usuario" style={{ fontSize: '10px' }} />
                        <Column body={actionBodyTemplate} exportable={false} style={{ fontSize: '10px' }}></Column>
                    </DataTable>
                </div>
                {/* Dialogo para Crear nuevo usuario */}
                <Dialog visible={usuarioCrearDialog} style={{ width: '32rem' }}
                    breakpoints={{ '960px': '75vw', '641px': '90vw' }}
                    header="Nuevo Usuario" modal className="p-fluid"
                    footer={usuarioDialogFooterNuevo}
                    onHide={hideDialogEditar}>

                    <div className="field">
                        <label htmlFor="identificacion" className="font-bold" style={{ fontSize: '10px' }}>
                            Identificación
                        </label>
                        <InputText id="identificacion" type="text" keyfilter="int" style={{ fontSize: '10px' }} maxLength={10} defaultValue={identificacionNuevo} onChange={(e) => setIdentificacionNuevo(e.target.value)} required autoFocus />
                    </div>
                    <div className="field">
                        <label htmlFor="nombresCompletos" className="font-bold" style={{ fontSize: '10px' }}>
                            Nombres Completos
                        </label>
                        <InputText id="nombresCompletos" type="text" style={{ fontSize: '10px' }} maxLength={60} defaultValue={nombresCompletosNuevo} onChange={(e) => setNombresCompletosNuevo(e.target.value)} required />
                    </div>
                    <div className="field">
                        <label htmlFor="correo" className="font-bold" style={{ fontSize: '10px' }}>
                            Correo
                        </label>
                        <InputText id="correo" type="email" style={{ fontSize: '10px' }} maxLength={60} defaultValue={correoNuevo} onChange={(e) => setCorreoNuevo(e.target.value)} required />
                    </div>
                    <div className="field">
                        <label htmlFor="perfil" className="font-bold" style={{ fontSize: '10px' }}>
                            Perfil
                        </label>
                        <Dropdown
                            value={selectedPerfil}
                            onChange={(e) => cambiarPerfil(e.value, 1)}
                            options={perfilesUsuario}
                            optionLabel="perfilDescripcion"
                            placeholder="Seleccionar Perfil"
                            className="w-full md:w-40rem"
                            style={{ fontSize: '10px' }} />
                    </div>
                    <div className="field">
                        <label htmlFor="perfil" className="font-bold" style={{ fontSize: '10px' }}>
                            Menus Sistema
                        </label>
                        <TreeSelect
                            disabled={valorMenus}
                            placeholder="Seleccione los menús"
                            value={menusSistemaSelec}
                            onChange={(e) => setMenusSistemaSelec(e.value)}
                            options={menusSistema}
                            metaKeySelection={false}
                            className="md:w-40rem w-full"
                            selectionMode="checkbox"
                            display="chip"
                            style={{ fontSize: '10px' }}>
                        </TreeSelect>
                    </div>
                </Dialog>

                {/* Dialogo para Editar */}
                <Dialog visible={usuarioEditarDialog} style={{ width: '32rem' }}
                    breakpoints={{ '960px': '75vw', '641px': '90vw' }}
                    header="Detalle Usuario" modal className="p-fluid"
                    footer={usuarioDialogFooterEdit}
                    onHide={hideDialogEditar}>

                    <div className="field">
                        <label htmlFor="identificacion" className="font-bold" style={{ fontSize: '10px' }}>
                            Identificación
                        </label>
                        <InputText id="identificacion" type="text" style={{ fontSize: '10px' }} keyfilter="int" readOnly={true} maxLength={10} defaultValue={usuario.identificacionUsuario} onChange={(e) => setIdentificacion(e.target.value)} required autoFocus />
                    </div>
                    <div className="field">
                        <label htmlFor="nombresCompletos" className="font-bold" style={{ fontSize: '10px' }}>
                            Nombres Completos
                        </label>
                        <InputText id="nombresCompletos" type="text" style={{ fontSize: '10px' }} maxLength={60} defaultValue={usuario.nombresUsuario} onChange={(e) => setNombresCompletosEditar(e.target.value)} required />
                    </div>
                    <div className="field">
                        <label htmlFor="correo" className="font-bold" style={{ fontSize: '10px' }}>
                            Correo
                        </label>
                        <InputText id="correo" type="text" style={{ fontSize: '10px' }} maxLength={60} defaultValue={usuario.correo} onChange={(e) => setCorreoEditar(e.target.value)} required />
                    </div>
                    <div className="field">
                        <label htmlFor="perfil" className="font-bold" style={{ fontSize: '10px' }}>
                            Perfil
                        </label>
                        <Dropdown
                            value={perfilEditSelected ? perfilEditSelected : perfilesUsuario[perfilEditar]}
                            onChange={(e) => cambiarPerfil(e.value ? e.value : perfilesUsuario[perfilEditar], 2)}
                            options={perfilesUsuario}
                            optionLabel="perfilDescripcion"
                            placeholder="Seleccionar Perfil"
                            className="w-full md:w-14rem"
                            style={{ fontSize: '10px' }}
                        />
                    </div>
                    <div className="field">
                        <label htmlFor="perfil" className="font-bold" style={{ fontSize: '10px' }}>
                            Menus Sistema
                        </label>
                        <TreeSelect
                            disabled={valorMenus}
                            placeholder="Seleccione los menús"
                            value={menusSistemaSelec}
                            onChange={(e) => setMenusSistemaSelec(e.value)}
                            options={menusSistema}
                            metaKeySelection={false}
                            className="md:w-40rem w-full"
                            selectionMode="checkbox"
                            display="chip"
                            style={{ fontSize: '10px' }}>
                        </TreeSelect>
                    </div>
                    <div className="field">
                        <label htmlFor="estadoUsuario" className="font-bold" style={{ fontSize: '10px' }}>
                            Estado Usuario ( Activo/Inactivo )
                        </label>
                        <Checkbox id="estadoUsuario" style={{ fontSize: '10px' }} onChange={e => setChecked(e.checked)} checked={checked}></Checkbox>
                    </div>
                </Dialog>
            </div>
        </>
    );
}

export default GestionUsuarios