import React, { useState, useRef } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { classNames } from 'primereact/utils';
import { styled } from '@mui/material/styles';
import ReactDOM from 'react-dom/client';
import {
    BrowserRouter as Router,
    useNavigate,
    Routes,
    Route,
} from "react-router-dom";
import { Panel } from 'primereact/panel';
import { Password } from 'primereact/password';
import { Button } from 'primereact/button';
import { Toast } from 'primereact/toast';
import { ConfirmDialog } from 'primereact/confirmdialog';
import Cookies from 'js-cookie';
import Variables from '../utilidades/Variables';
import Mensajes from '../utilidades/Mensajes';
import './CambiarContrasenia.css'
import ServicioWeb from '../utilidades/ServicioWeb';
import Login from '../../../../../Login';
import Menu from '../../../../../Menu';

function CambiarContrasenia() {

    const navigate = useNavigate();
    let objetoSesion = "";
    const toast = useRef(null);
    const [blocked, setBlocked] = useState(false);
    const [visibleGuardar, setVisibleGuardar] = useState(false);
    const [contraseniaActual, setContraseniaActual] = useState("");
    const [contraseniaNueva, setContraseniaNueva] = useState("");
    const [contraseniaRepetir, setContraseniaRepetir] = useState("");
    let informacionSesionUsuario = Cookies.get('infoSesionUsuario');
    objetoSesion = !!informacionSesionUsuario ? JSON.parse(informacionSesionUsuario) : "";

    const DrawerHeader = styled('div')(({ theme }) => ({
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    }));

    const ocultarDialogConfirm = () => {
        setVisibleGuardar(false);
    }

    const getFormErrorMessage = (name) => {
        return errors[name] ? <small className="p-error">{errors[name].message}</small> : <small className="p-error">&nbsp;</small>;
    };

    const showToast = (severityValue, summaryValue, detailValue, timeLife) => {
        toast?.current?.show({ severity: severityValue, summary: summaryValue, detail: detailValue, life: timeLife });
    }

    const aceptarGuardado = (data) => {
        if (data.contraseniaNueva.trim() === data.contraseniaRepetir.trim()) {
            setVisibleGuardar(true);
            setContraseniaActual(data.contraseniaActual);
            setContraseniaNueva(data.contraseniaNueva);
            setContraseniaRepetir(data.contraseniaRepetir);
        } else {
            showToast('error', 'Error', Mensajes.MENSAJE_CONTRASENIAS_NO_IGUALES, 6000);
        }
    }

    const funcionDeslogear = () => {
        const root = ReactDOM.createRoot(document.getElementById("root"));
        root.render(
            <React.StrictMode>
                <Router>
                    <Routes>
                        <Route path="/" element={<Login />} />
                        <Route path="/login" element={<Login />} />
                        <Route path="/menu" element={<Menu />} />
                        <Route path="/cambiarContrasenia" element={<CambiarContrasenia />} />
                    </Routes>
                    <Login />
                </Router>
            </React.StrictMode>
        );
        navigate("/login", { replace: true });
        window.location.reload(false)
    }

    const cambiarContrasenia = () => {
        ; (async () => {
            setBlocked(true)
            let tokenWS = await ServicioWeb.obtenerToken(Variables.USER_LOGIN_API_BACKEND, Variables.PASSWORD_LOGIN_API_BACKEND, Variables.PETICION_BACKEND_ARBOL_TOKEN);
            let rowDetallado = {
                identificacion: objetoSesion.identificacion.trim(),
                contraseniaRegistrada: contraseniaActual.trim(),
                contraseniaNueva: contraseniaNueva.trim(),
                contraseniaNuevaRepetir: contraseniaRepetir.trim(),
            }
            let requestBody = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                    'Access-Control-Allow-Credentials': 'true',
                    'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                    Authorization: tokenWS
                },
                body: JSON.stringify(rowDetallado)
            }
            let rows = await ServicioWeb.obtenerDatosConBody(
                Variables.PETICION_BACKEND_ARBOL_ACTUALIZAR_CONTRASENIA,
                requestBody
            );
            if (rows.status) {
                setBlocked(false)
                Cookies.remove('infoSesionUsuario');
                funcionDeslogear();
            } else {
                setBlocked(false)
                showToast('error', 'Error', rows.mensaje, 6000);
            }
        })()
    }

    const defaultValues = { contraseniaActual: '', contraseniaNueva: '', contraseniaRepetir: '' };
    const form = useForm({ defaultValues });
    const errors = form.formState.errors;

    return (
        <>
            <DrawerHeader />
            <div className="container_cambiarcontrasenia">
                <Toast ref={toast} />
                <ConfirmDialog visible={visibleGuardar} onHide={() => setVisibleGuardar(false)}
                    message={Mensajes.ACEPTAR_GUARDADO_USUARIO_NUEVO}
                    header="Confirmación" icon="pi pi-exclamation-triangle"
                    accept={cambiarContrasenia}
                    reject={ocultarDialogConfirm} />
                <div className="panel_principal_contrasenia">
                    <Panel header="Cambiar Contraseña">
                        <div className="card flex justify-content-center">
                            <form onSubmit={form.handleSubmit(aceptarGuardado)} className="flex flex-column gap-2">
                                <Controller
                                    name="contraseniaActual"
                                    control={form.control}
                                    rules={{ required: 'La contraseña actual es obligatoria.' }}
                                    render={({ field, fieldState }) => (
                                        <>
                                            <label htmlFor={field.name} className={classNames({ 'p-error': errors.value })}></label>
                                            <span className="p-float-label">
                                                <Password id={field.name} {...field} maxLength={60} className={classNames({ 'p-invalid': fieldState.error })} feedback={false} autoFocus toggleMask />
                                                <label htmlFor={field.name}>Contraseña Actual</label>
                                            </span>
                                            {getFormErrorMessage(field.name)}
                                        </>
                                    )}
                                />
                                <Controller
                                    name="contraseniaNueva"
                                    control={form.control}
                                    rules={{ required: 'La contraseña nueva es obligatoria.' }}
                                    render={({ field, fieldState }) => (
                                        <>
                                            <label htmlFor={field.name} className={classNames({ 'p-error': errors.value })}></label>
                                            <span className="p-float-label">
                                                <Password id={field.name} {...field} maxLength={60} className={classNames({ 'p-invalid': fieldState.error })} toggleMask />
                                                <label htmlFor={field.name}>Contraseña Nueva</label>
                                            </span>
                                            {getFormErrorMessage(field.name)}
                                        </>
                                    )}
                                />
                                <Controller
                                    name="contraseniaRepetir"
                                    control={form.control}
                                    rules={{ required: 'La contraseña nueva repetir es obligatoria.' }}
                                    render={({ field, fieldState }) => (
                                        <>
                                            <label htmlFor={field.name} className={classNames({ 'p-error': errors.value })}></label>
                                            <span className="p-float-label">
                                                <Password id={field.name} {...field} maxLength={60} className={classNames({ 'p-invalid': fieldState.error })} toggleMask />
                                                <label htmlFor={field.name}>Repetir Contraseña Nueva</label>
                                            </span>
                                            {getFormErrorMessage(field.name)}
                                        </>
                                    )}
                                />
                                <br></br>
                                <Button label="Cambiar Contraseña" type="submit" icon="pi pi-user-edit" loading={blocked} outlined />
                            </form>
                        </div>
                    </Panel>
                </div>
            </div>
        </>
    )
}

export default CambiarContrasenia