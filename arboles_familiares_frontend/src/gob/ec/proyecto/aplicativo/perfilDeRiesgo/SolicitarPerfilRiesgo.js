import React, { useState, useEffect, useRef } from 'react'
import { styled } from '@mui/material/styles';
import { Panel } from 'primereact/panel';
import { Toast } from 'primereact/toast';
import { classNames } from 'primereact/utils';
import Typography from '@mui/material/Typography';
import { Controller, useForm } from 'react-hook-form';
import { InputText } from 'primereact/inputtext';
import { InputTextarea } from "primereact/inputtextarea";
import { Button } from 'primereact/button';
import { ConfirmDialog } from 'primereact/confirmdialog';
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { Divider } from 'primereact/divider';
import { Column } from 'primereact/column';
import { Tooltip } from 'primereact/tooltip';
import { FileUpload } from 'primereact/fileupload';
import { Tag } from 'primereact/tag';
import Mensajes from '../utilidades/Mensajes';
import ServicioWeb from '../utilidades/ServicioWeb';
import Variables from '../utilidades/Variables';
import PerfilesDeRiesgoService from '../service/PerfilesDeRiesgoService';
import Cookies from 'js-cookie';
import './SolicitarPerfilRiesgo.css';

function SolicitarPerfilRiesgo() {

    let informacionSesionUsuario = Cookies.get('infoSesionUsuario');
    let objetoSesion = !!informacionSesionUsuario ? JSON.parse(informacionSesionUsuario) : "";

    const dt = useRef(null);
    const defaultValues = { ruc: '' };
    const form = useForm({ defaultValues });
    const errors = form.formState.errors;
    const [blocked, setBlocked] = useState(false);
    const [blockedGenerar, setBlockedGenerar] = useState(false);
    const [visibleConsultar, setVisibleConsultar] = useState(false);
    const [visibleGeneracionPerfil, setVisibleGeneracionPerfil] = useState(false);
    const [generarSolicitud, setGenerarSolicitud] = useState(true);
    const [datosConsulta, setDatosConsulta] = useState({});
    const [globalFilterValue, setGlobalFilterValue] = useState('');
    const [archivoRespaldo, setArchivoRespaldo] = useState();
    const [archivoJustificacion, setArchivoJustificacion] = useState();
    const [justificacion, setJustificacion] = useState("");

    const [ruc, setRuc] = useState("");
    const [razonSocial, setRazonSocial] = useState("");
    const [fechaInicioActividades, setFechaInicioActividades] = useState("");

    const [perfilesDeRiesgo, setPerfilesDeRiesgo] = useState([]);
    const [loading, setLoading] = useState(true);

    //Tamaño de archivos
    const [totalSizeCPC, setTotalSizeCPC] = useState(0);
    const [totalSizeJust, setTotalSizeJust] = useState(0);

    const [filters, setFilters] = useState({
        global: { value: null, matchMode: FilterMatchMode.CONTAINS },
        numeroSolicitud: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        estadoSolicitud: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        usuarioCreacion: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        fechaCreacion: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        fechaActualizacion: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        rucSolicitado: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        razonSocialSolicitado: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        rucFechaInicioActividades: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
    });

    const onGlobalFilterChange = (e) => {
        const value = e.target.value;
        let _filters = { ...filters };
        _filters['global'].value = value;
        setFilters(_filters);
        setGlobalFilterValue(value);
    };

    const cols = [
        { field: 'numeroSolicitud', header: 'Número Solicitud' },
        { field: 'estadoSolicitud', header: 'Estado Solicitud' },
        { field: 'usuarioCreacion', header: 'Usuario Creación' },
        { field: 'fechaCreacion', header: 'Fecha Creación' },
        { field: 'fechaActualizacion', header: 'Fecha Actualización' },
        { field: 'rucSolicitado', header: 'Ruc Solicitado' },
        { field: 'razonSocialSolicitado', header: 'Razon Social Solicitado' },
        { field: 'rucFechaInicioActividades', header: 'Ruc Fecha Inicio Actividades' },
    ];

    const exportColumns = cols.map((col) => ({ title: col.header, dataKey: col.field }));

    const exportCSV = (selectionOnly) => {
        dt.current.exportCSV({ selectionOnly });
    };

    const exportPdf = () => {
        import('jspdf').then((jsPDF) => {
            import('jspdf-autotable').then(() => {
                const doc = new jsPDF.default(0, 0);

                doc.autoTable(exportColumns, perfilesDeRiesgo);
                doc.save('usuarios.pdf');
            });
        });
    };

    const exportExcel = () => {
        import('xlsx').then((xlsx) => {
            const worksheet = xlsx.utils.json_to_sheet(perfilesDeRiesgo);
            const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
            const excelBuffer = xlsx.write(workbook, {
                bookType: 'xlsx',
                type: 'array'
            });
            saveAsExcelFile(excelBuffer, 'perfilesDeRiesgo');
        });
    };

    //Listar solicitudes
    const cargarDatosData = () => {
        PerfilesDeRiesgoService.getPerfilesDeRiesgoMedium().then((data) => {
            setPerfilesDeRiesgo(getPerfilesDeRiesgo(data));
            setLoading(false);
        });
    }

    useEffect(() => {
        cargarDatosData();
    }, []);

    const header = (
        <div className="flex justify-content-end">
            <span className="p-input-icon-left">
                <i className="pi pi-search" />
                <InputText value={globalFilterValue} onChange={onGlobalFilterChange} style={{fontSize: '10px'}} placeholder="Buscar" />
            </span>
            <br></br>
            <Button type="button" icon="pi pi-refresh" style={{fontSize: '10px'}} rounded onClick={() => cargarDatosData()} label="Refrescar" />
            <Button type="button" icon="pi pi-file" style={{fontSize: '10px'}} rounded onClick={() => exportCSV(false)} data-pr-tooltip="CSV" label="CSV" />
            <Button type="button" icon="pi pi-file-excel" style={{fontSize: '10px'}} severity="success" rounded onClick={exportExcel} data-pr-tooltip="XLS" label="XLSX" />
            <Button type="button" icon="pi pi-file-pdf" style={{fontSize: '10px'}} severity="warning" rounded onClick={exportPdf} data-pr-tooltip="PDF" label="PDF" />
        </div>
    );

    const saveAsExcelFile = (buffer, fileName) => {
        import('file-saver').then((module) => {
            if (module && module.default) {
                let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
                let EXCEL_EXTENSION = '.xlsx';
                const data = new Blob([buffer], {
                    type: EXCEL_TYPE
                });
                module.default.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
            }
        });
    };

    const getPerfilesDeRiesgo = (data) => {
        return [...(data || [])].map((d) => {
            d.date = new Date(d.date);
            return d;
        });
    };

    const DrawerHeader = styled('div')(({ theme }) => ({
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    }));

    const toast = useRef(null);

    const getFormErrorMessage = (name) => {
        return errors[name] ? <small className="p-error">{errors[name].message}</small> : <small className="p-error">&nbsp;</small>;
    };

    const showToast = (severityValue, summaryValue, detailValue, timeLife) => {
        toast?.current?.show({ severity: severityValue, summary: summaryValue, detail: detailValue, life: timeLife });
    }

    const aceptarConsulta = (data) => {
        setVisibleConsultar(true)
        setGenerarSolicitud(true)
        setRuc(data.ruc)
    }

    const realizarConsultaRuc = () => {
        ; (async () => {
            setBlocked(true)
            let tokenApi = await ServicioWeb.obtenerTokenEtl(Variables.USER_LOGIN_API_BACKEND, Variables.PASSWORD_LOGIN_API_BACKEND, Variables.PETICION_APIS_SRI_TOKEN);
            let rowDetallado = {
                ruc: ruc,
            }
            let requestBody = {
                method: 'POST',
                headers: {
                    Accept: "application/json",
                    'Content-Type': 'application/json',
                    'origin': 'x-request-with',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                    'Access-Control-Allow-Credentials': 'true',
                    'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                    Authorization: tokenApi
                },
                body: JSON.stringify(rowDetallado)
            }
            let rows = await ServicioWeb.obtenerDatosConBody(
                Variables.PETICION_APIS_SRI_CONSULTAR_RUC_CONTRIBUYENTES,
                requestBody
            );
            if (rows.estado) {
                setBlocked(false)
                setRuc(rows.respuesta[0].numeroRuc);
                setRazonSocial(rows.respuesta[0].razonSocial);
                setFechaInicioActividades(rows.respuesta[0].fechaInicioActividades);
                setGenerarSolicitud(false)
                setDatosConsulta(rows.respuesta)
                showToast('success', 'Exitoso', rows.mensaje, 6000);

            } else {
                setRuc("");
                setRazonSocial("");
                setFechaInicioActividades("");
                setBlocked(false)
                setGenerarSolicitud(true)
                setDatosConsulta([])
                showToast('error', 'Error', rows.consulta, 6000);
            }
        })();
    }

    const aceptarGeneracionPerfil = () => {
        setVisibleGeneracionPerfil(true)
    }

    const generarPerfilDeRiesgo = () => {
        let rows = false;
        if (archivoRespaldo && archivoJustificacion) {
            if (ruc.trim().length !== 0 && razonSocial.trim().length !== 0 && fechaInicioActividades.trim().length !== 0 && justificacion.trim() !== 0) {
                ; (async () => {
                    setBlockedGenerar(true)
                    try {

                        let tokenApi = await ServicioWeb.obtenerToken(Variables.USER_LOGIN_API_BACKEND, Variables.PASSWORD_LOGIN_API_BACKEND, Variables.PETICION_BACKEND_ARBOL_TOKEN);
                        let formData = new FormData();
                        formData.append("idUsuario", objetoSesion.idUsuario);
                        formData.append("ruc", ruc.trim());
                        formData.append("razonSocial", razonSocial.trim());
                        formData.append("fechaInicioActividades", fechaInicioActividades.trim());
                        formData.append("archivoRespaldoPerfilRiesgo", archivoRespaldo);
                        formData.append("justificacion", justificacion.trim());
                        formData.append("archivoJustificacion", archivoJustificacion);
                        let requestBody = {
                            method: 'POST',
                            headers: {
                                Authorization: tokenApi
                            },
                            body: formData
                        }
                        rows = await ServicioWeb.obtenerDatosConBody(
                            Variables.PETICION_BACKEND_GENERACION_PERFIL_DE_RIESGO,
                            requestBody
                        );

                        //----------- Peticion Token Api Perfil de Riesgo ----------- //
                        let rowDetallado = {
                            ruc: ruc.trim(),
                            fecha_ini_activ: fechaInicioActividades.trim(),
                            id_user: objetoSesion.idUsuario
                        }
                        let requestBodyPerfil = {
                            method: 'POST',
                            headers: {
                                Accept: "application/json",
                                'Content-Type': 'application/json',
                                'origin': 'x-request-with',
                                'Access-Control-Allow-Origin': '*',
                                'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                                'Access-Control-Allow-Credentials': 'true',
                                'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                            },
                            body: JSON.stringify(rowDetallado)
                        }
                        console.log(requestBodyPerfil)
                        let rowsPerfil = await ServicioWeb.obtenerDatosConBody(
                            Variables.PETICION_PERFIL_DE_RIESGO,
                            requestBodyPerfil
                        );
                        //----------- Peticion Token Api Perfil de Riesgo ----------- //                        

                        if (rows.status && rowsPerfil.status) {
                            setBlockedGenerar(false)
                            cargarDatosData()
                            showToast('success', 'Exitoso', rows.mensaje, 6000);
                        } else {
                            setBlockedGenerar(false)
                            showToast('error', 'Error', "Error", 6000);
                        }

                    } catch (error) {
                        setBlockedGenerar(false)
                        showToast('error', 'Error', "Ha ocurrido un error al momento de realizar la solicitud, vuelva a intentarlo luego", 6000);
                    }
                })();
            } else {
                showToast('error', 'Error', Mensajes.VALIDAR_GUARDADO_PERFIL_DE_RIESGO, 6000);
            }
        } else {
            showToast('error', 'Error', Mensajes.ARCHIVO_DE_SUBIDA_PERFIL_RIESGO_NO_VALIDO, 6000);
        }
    }

    //Tipo de archivo
    const metodoSubir = (event) => {
        let file = event.files[0];
        let nombreArchivo = file.name;
        let tipoArchivo = nombreArchivo.split(".");
        if (tipoArchivo[(tipoArchivo.length) - 1].toLowerCase() === 'xlsx' || tipoArchivo[(tipoArchivo.length) - 1].toLowerCase() === 'xls') {
            setArchivoRespaldo(file)
            showToast('success', 'Correcto', Mensajes.ARCHIVO_DE_SUBIDA_PERFIL_RIESGO, 6000);
        } else {
            setArchivoRespaldo("")
            showToast('error', 'Error', Mensajes.ARCHIVO_DE_CON_FORMATO_INCORRECTO, 6000);
        }
    }

    //Archivo de justificación
    const metodoSubirArchivoJust = (event) => {
        let file = event.files[0];
        let nombreArchivo = file.name;
        let tipoArchivo = nombreArchivo.split(".");
        if (tipoArchivo[(tipoArchivo.length) - 1].toLowerCase() === 'pdf' || tipoArchivo[(tipoArchivo.length) - 1].toLowerCase() === 'pdf') {
            setArchivoJustificacion(file)
            showToast('success', 'Correcto', Mensajes.ARCHIVO_DE_SUBIDA_PERFIL_RIESGO, 6000);
        } else {
            setArchivoJustificacion("")
            showToast('error', 'Error', Mensajes.ARCHIVO_DE_CON_FORMATO_INCORRECTO, 6000);
        }
    }


    const monstrarAdjunto = (rowData) => {
        console.log(rowData)
            ; (async () => {
                let tokenApi = await ServicioWeb.obtenerToken(Variables.USER_LOGIN_API_BACKEND, Variables.PASSWORD_LOGIN_API_BACKEND, Variables.PETICION_BACKEND_ARBOL_TOKEN);
                let rowDetallado = {
                    idPerfilRiesgo: rowData.idPerfilRiesgo
                }
                let requestBody = {
                    method: 'POST',
                    headers: {
                        Accept: "application/json",
                        'Content-Type': 'application/json',
                        'origin': 'x-request-with',
                        'Access-Control-Allow-Origin': '*',
                        'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                        'Access-Control-Allow-Credentials': 'true',
                        'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                        'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                        Authorization: tokenApi
                    },
                    body: JSON.stringify(rowDetallado)
                }
                let rows = await ServicioWeb.obtenerDatosConBody(
                    Variables.PETICION_BACKEND_TRAER_ARCHIVO_REPORTE_PERFIL,
                    requestBody
                );
                if (rows.status) {
                    let link;
                    link = document.createElement('a');
                    link.href = "archivos_reporte_perfil_riesgo/" + "SPR_" + rowData.idPerfilRiesgo + ".pdf";
                    link.setAttribute("target", "_blank");
                    link.click();
                } else {
                    showToast('error', 'Error', rows.mensaje, 6000);
                }
            })();
    }

    const visualizarReporte = (rowData) => {
        if (rowData.estadoSolicitud === "Generado") {
            return (
                <React.Fragment>
                    <Button icon="pi pi-file-export" style={{fontSize: '10px'}} rounded outlined className="mr-2" label="Visualizar Reporte" onClick={() => monstrarAdjunto(rowData)} />
                </React.Fragment>
            );
        } else {
            return (
                <></>
            );
        }
    }

    const colorColumnaEstado = (rowData) => {
        let color = "#f2f45c";//Amarillo
        switch (rowData.estadoSolicitud) {
            case "Error":
                color = "#ff0000";//Rojo
                break;
            case "Registrado":
                color = "#230dfa";//Azul
                break;
            case "Generado":
                color = "#00ff5d";//Verde
                break;
            default:
                color = "#f2f45c";//Amarillo
        }
        return (
            <React.Fragment>
                <Typography variant="h10" noWrap component="div" style={{ fontSize: '12px', color: color }}>{rowData.estadoSolicitud}</Typography>
            </React.Fragment>
        );
    }

    const chooseOptionsXls = {label: 'Escoger', icon: 'pi pi-fw pi-plus', style: {'fontSize': '10px'}};
    const uploadOptionsXls = {label: 'Subir', icon: 'pi pi-upload', className: 'p-button-success', style: {'fontSize': '10px'}};
    const cancelOptionsXls = {label: 'Cancelar', icon: 'pi pi-times', className: 'p-button-danger', style: {'fontSize': '10px'}};

    const chooseOptionsPdf = {label: 'Escoger', icon: 'pi pi-fw pi-plus', style: {'fontSize': '10px'}};
    const uploadOptionsPdf = {label: 'Subir', icon: 'pi pi-upload', className: 'p-button-success', style: {'fontSize': '10px'}};
    const cancelOptionsPdf = {label: 'Cancelar', icon: 'pi pi-times', className: 'p-button-danger', style: {'fontSize': '10px'}};

    const paginatorLeft = <Button type="button" icon="pi pi-refresh" onClick={() => cargarDatosData()} />;

    const onTemplateRemoveCPC = (file, callback) => {
        setTotalSizeCPC(totalSizeCPC - file.size);
        setArchivoRespaldo("")
        callback();
    };

    const onTemplateRemoveJust = (file, callback) => {
        setTotalSizeJust(totalSizeJust - file.size);
        setArchivoJustificacion("")
        callback();
    };

    const itemTemplateCPC = (file, props) => {
        return (
            <div className="flex align-items-center flex-wrap">
                <div className="flex align-items-center" style={{ width: '40%' }}>
                    <img
                        alt={file.name}
                        role="presentation"
                        src={file.objectURL}
                        width={100}
                    />
                    <span className="flex flex-column text-left ml-3">
                        {file.name}
                        <small>{new Date().toLocaleDateString()}</small>
                    </span>
                </div>
                <Tag
                    value={props.formatSize}
                    severity="warning"
                    className="px-3 py-2"
                />
                <Button
                    type="button"
                    icon="pi pi-times"
                    className="p-button-outlined p-button-rounded p-button-danger ml-auto"
                    onClick={() => onTemplateRemoveCPC(file, props.onRemove)}
                />
            </div>
        );
    };

    const itemTemplateArchivoJust = (file, props) => {
        return (
            <div className="flex align-items-center flex-wrap">
                <div className="flex align-items-center" style={{ width: '40%' }}>
                    <img
                        alt={file.name}
                        role="presentation"
                        src={file.objectURL}
                        width={100}
                    />
                    <span className="flex flex-column text-left ml-3">
                        {file.name}
                        <small>{new Date().toLocaleDateString()}</small>
                    </span>
                </div>
                <Tag
                    value={props.formatSize}
                    severity="warning"
                    className="px-3 py-2"
                />
                <Button
                    type="button"
                    icon="pi pi-times"
                    className="p-button-outlined p-button-rounded p-button-danger ml-auto"
                    onClick={() => onTemplateRemoveJust(file, props.onRemove)}
                />
            </div>
        );
    };


    return (
        <>
            <DrawerHeader />
            <ConfirmDialog visible={visibleGeneracionPerfil} onHide={() => setVisibleGeneracionPerfil(false)} message={Mensajes.ACEPTAR_GENERACION_PERFIL_DE_RIESGO}
                header="Confirmación" icon="pi pi-exclamation-triangle" accept={generarPerfilDeRiesgo} />
            <ConfirmDialog visible={visibleConsultar} onHide={() => setVisibleConsultar(false)} message={Mensajes.ACEPTAR_CONSULTA_RUC}
                header="Confirmación" icon="pi pi-exclamation-triangle" accept={realizarConsultaRuc} />
            <div className="container_solicitar_perfil_riesgo">
                <Toast ref={toast} />
                <div className="panel_principal_solicitar_perfil_riesgo">
                    <Panel header={<h5>Solicitar Perfil de Riesgo</h5>} className="mypanel">                        
                        <form onSubmit={form.handleSubmit(aceptarConsulta)} className="flex flex-column gap-2">
                            <div className="grid">
                                <div className="col-12 md:col-4 mb-4 px-6">
                                    <Controller
                                        name="ruc"
                                        control={form.control}
                                        rules={{ required: 'El ruc es obligatorio.' }}
                                        render={({ field, fieldState }) => (
                                            <>
                                                <label style={{fontSize: '10px'}} htmlFor={field.name} className={classNames({ 'p-error': errors.value })}>Ingrese el Ruc: <span className='required'>*</span></label>
                                                <span className="p-float-label">
                                                    <InputText id={field.name} {...field}
                                                        style={{fontSize: '10px'}}
                                                        type="text"
                                                        keyfilter="int"
                                                        maxLength={13}
                                                        minLength={13}
                                                        className={classNames({ 'p-invalid': fieldState.error }, "md:w-20rem w-full")}
                                                    />
                                                    <label htmlFor={field.name}></label>
                                                </span>
                                                {getFormErrorMessage(field.name)}
                                            </>
                                        )}
                                    />
                                </div>
                                <div className="col-12 md:col-4 mb-4 px-5">
                                    <br></br>
                                    <Button label="Buscar" type="submit" icon="pi pi-search" loading={blocked} style={{fontSize: '10px'}}/>
                                </div>
                            </div>
                        </form>
                        <br></br>
                        <div className="card">
                            <DataTable showGridlines value={datosConsulta} className='tabla_ruc'
                                emptyMessage="Información no encontrada">
                                <Column field="numeroRuc" header="Ruc"></Column>
                                <Column field="razonSocial" header="Razon Social"></Column>
                                <Column field="fechaInicioActividades" header="Fecha Inicio Actividades"></Column>
                            </DataTable>
                        </div>
                        <br></br>
                        <br></br>
                        {/* <label htmlFor="archivoJustPerfilRiesgo">Archivo Justificación: <input id={"archivoJustPerfilRiesgo"} disabled={generarSolicitud} type="file" accept=".xlsx" /></label> */}
                        {/* <label htmlFor="archivoJustPerfilRiesgo">Archivo Justificación: <input id={"archivoJustPerfilRiesgo"} type="file" accept=".xlsx" /></label> */}
                        <h5>Archivo CPC Solicitados (.xlsx): <span className='required'>*</span></h5>
                        <div className="tamanio_archivo">
                            <FileUpload name="demo[]"
                                style={{fontSize: '10px'}}
                                url={'/api/upload'}
                                multiple={false}
                                accept=".xlsx"
                                maxFileSize={1000000}
                                customUpload
                                disabled={generarSolicitud}
                                chooseOptions={chooseOptionsXls} uploadOptions={uploadOptionsXls} cancelOptions={cancelOptionsXls}
                                uploadHandler={metodoSubir}
                                itemTemplate={itemTemplateCPC}
                                emptyTemplate={<p className="m-0">Arrastre aqui el archivo cpc solicitado a subir o de clic en el boton "Escoger"</p>}
                            />
                        </div>
                        <h5>Justificación: <span className='required'>*</span></h5>
                        <InputTextarea
                            maxLength={300} 
                            rows={4} 
                            cols={100}
                            defaultValue={justificacion} 
                            disabled={generarSolicitud}                            
                            onChange={(e) => setJustificacion(e.target.value)} 
                        />
                        <h5>Archivo de Justificación (.pdf): <span className='required'>*</span></h5>
                        <div className="tamanio_archivo">
                            <FileUpload name="demo[]"
                                style={{fontSize: '10px'}}
                                url={'/api/upload'}
                                multiple={false}
                                accept=".pdf"
                                maxFileSize={1000000}
                                customUpload
                                disabled={generarSolicitud}
                                chooseOptions={chooseOptionsPdf} uploadOptions={uploadOptionsPdf} cancelOptions={cancelOptionsPdf}
                                uploadHandler={metodoSubirArchivoJust}
                                itemTemplate={itemTemplateArchivoJust}
                                emptyTemplate={<p className="m-0">Arrastre aqui el archivo de Justificación a subir o de clic en el boton "Escoger"</p>}
                            />
                        </div>
                        <br></br>
                        <br></br>
                        <Button label="Generar Perfil" icon="pi pi-user-edit" disabled={generarSolicitud} loading={blockedGenerar} onClick={aceptarGeneracionPerfil} style={{fontSize: '10px'}} />
                        <br></br>
                        <Divider />
                        <h6>Listado de Solicitudes</h6>
                        <div className="tamanio_toolbar">
                            <Tooltip target=".export-buttons>button" position="bottom" />
                            <DataTable 
                                value={perfilesDeRiesgo} 
                                paginator 
                                rows={10}                                
                                dataKey="idPerfilRiesgo"
                                rowsPerPageOptions={[10, 25, 50]}
                                ref={dt}
                                filters={filters}
                                filterDisplay="row"
                                loading={loading}
                                globalFilterFields={['numeroSolicitud', 'estadoSolicitud', 'usuarioCreacion', 'fechaCreacion', 'fechaActualizacion', 'rucSolicitado', 'razonSocialSolicitado', 'rucFechaInicioActividades']}
                                header={header}
                                emptyMessage="Información no encontrada"                                
                                paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                                currentPageReportTemplate="Mostrando {first} / {last} de un total de {totalRecords} registros"
                                paginatorLeft={paginatorLeft}>

                                <Column field="numeroSolicitud" sortable header="Número Solicitud" footer="Número Solicitud" filter filterPlaceholder="Buscar por Número Solicitud" style={{ fontSize: '10px' }} />
                                {/* <Column field="estadoSolicitud" sortable header="Estado Solicitud" footer="Estado Solicitud" filter filterPlaceholder="Buscar por Estado Solicitud" style={{ fontSize: '10px' }} /> */}
                                <Column body={colorColumnaEstado} sortable header="Estado Solicitud" footer="Estado Solicitud" filter filterPlaceholder="Buscar por Estado Solicitud" style={{ fontSize: '10px' }}></Column>
                                <Column field="usuarioCreacion" sortable header="Usuario Creación" footer="Usuario Creación" filter filterPlaceholder="Buscar por Usuario Creación" style={{ fontSize: '10px' }} />
                                <Column field="fechaCreacion" sortable header="Fecha Creación" footer="Fecha Creación" filter filterPlaceholder="Buscar por Fecha Creación" style={{ fontSize: '10px' }} />
                                <Column field="fechaActualizacion" sortable header="Fecha Actualización" footer="Fecha Actualización" filter filterPlaceholder="Buscar por Fecha Actualización" style={{ fontSize: '10px' }} />
                                <Column field="rucSolicitado" sortable header="Ruc Solicitado" footer="Ruc Solicitado" filter filterPlaceholder="Buscar por Ruc Solicitado" style={{ fontSize: '10px' }} />
                                <Column field="razonSocialSolicitado" sortable header="Razon Social Solicitado" footer="Razon Social Solicitado" filter filterPlaceholder="Buscar por Razon Social Solicitado" style={{ fontSize: '10px' }} />
                                <Column field="rucFechaInicioActividades" sortable header="Fecha Inicio Actividades" footer="Fecha Inicio Actividades" filter filterPlaceholder="Buscar por Fecha Inicio Actividades" style={{ fontSize: '10px' }} />
                                <Column body={visualizarReporte} header="Visualizar Reporte" footer="Visualizar Reporte" exportable={false} style={{ fontSize: '10px' }}></Column>
                            </DataTable>
                        </div>
                    </Panel>
                </div>
            </div>
        </>
    );
}

export default SolicitarPerfilRiesgo;