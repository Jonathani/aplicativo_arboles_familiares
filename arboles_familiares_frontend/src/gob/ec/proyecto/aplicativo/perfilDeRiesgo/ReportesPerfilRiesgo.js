import React, { useState, useEffect, useRef } from 'react'
import { styled } from '@mui/material/styles';
import { Panel } from 'primereact/panel';
import { Toast } from 'primereact/toast';
import Typography from '@mui/material/Typography';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Tooltip } from 'primereact/tooltip';
import Mensajes from '../utilidades/Mensajes';
import ServicioWeb from '../utilidades/ServicioWeb';
import Variables from '../utilidades/Variables';
import PerfilesDeRiesgoService from '../service/PerfilesDeRiesgoService';
import Cookies from 'js-cookie';
import './ReportesPerfilRiesgo.css';

function ReportesPerfilRiesgo() {

    let informacionSesionUsuario = Cookies.get('infoSesionUsuario');
    let objetoSesion = !!informacionSesionUsuario ? JSON.parse(informacionSesionUsuario) : "";

    const dt = useRef(null);
    const [globalFilterValue, setGlobalFilterValue] = useState('');

    const [perfilesDeRiesgo, setPerfilesDeRiesgo] = useState([]);
    const [loading, setLoading] = useState(true);

    const [filters, setFilters] = useState({
        global: { value: null, matchMode: FilterMatchMode.CONTAINS },
        numeroSolicitud: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        estadoSolicitud: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        usuarioCreacion: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        justificacion: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        fechaCreacion: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        fechaActualizacion: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        rucSolicitado: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        razonSocialSolicitado: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        rucFechaInicioActividades: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
    });

    const onGlobalFilterChange = (e) => {
        const value = e.target.value;
        let _filters = { ...filters };
        _filters['global'].value = value;
        setFilters(_filters);
        setGlobalFilterValue(value);
    };

    const cols = [
        { field: 'numeroSolicitud', header: 'Número Solicitud' },
        { field: 'estadoSolicitud', header: 'Estado Solicitud' },
        { field: 'usuarioCreacion', header: 'Usuario Creación' },
        { field: 'justificacion', header: 'Justificación' },
        { field: 'fechaCreacion', header: 'Fecha Creación' },
        { field: 'fechaActualizacion', header: 'Fecha Actualización' },
        { field: 'rucSolicitado', header: 'Ruc Solicitado' },
        { field: 'razonSocialSolicitado', header: 'Razon Social Solicitado' },
        { field: 'rucFechaInicioActividades', header: 'Ruc Fecha Inicio Actividades' },
    ];

    const exportColumns = cols.map((col) => ({ title: col.header, dataKey: col.field }));

    const exportCSV = (selectionOnly) => {
        dt.current.exportCSV({ selectionOnly });
    };

    const exportPdf = () => {
        import('jspdf').then((jsPDF) => {
            import('jspdf-autotable').then(() => {
                const doc = new jsPDF.default(0, 0);

                doc.autoTable(exportColumns, perfilesDeRiesgo);
                doc.save('usuarios.pdf');
            });
        });
    };

    const exportExcel = () => {
        import('xlsx').then((xlsx) => {
            const worksheet = xlsx.utils.json_to_sheet(perfilesDeRiesgo);
            const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
            const excelBuffer = xlsx.write(workbook, {
                bookType: 'xlsx',
                type: 'array'
            });
            saveAsExcelFile(excelBuffer, 'perfilesDeRiesgo');
        });
    };

    //Listar solicitudes
    const cargarDatosData = () => {
        PerfilesDeRiesgoService.getPerfilesDeRiesgoMedium().then((data) => {
            setPerfilesDeRiesgo(getPerfilesDeRiesgo(data));
            setLoading(false);
        });
    }

    useEffect(() => {
        cargarDatosData();
    }, []);

    const header = (
        <div className="flex justify-content-end">
            <span className="p-input-icon-left">
                <i className="pi pi-search" />
                <InputText value={globalFilterValue} onChange={onGlobalFilterChange} style={{ fontSize: '10px' }} placeholder="Buscar" />
            </span>
            <br></br>
            <Button type="button" icon="pi pi-refresh" style={{ fontSize: '10px' }} rounded onClick={() => cargarDatosData()} label="Refrescar" />
            <Button type="button" icon="pi pi-file" style={{ fontSize: '10px' }} rounded onClick={() => exportCSV(false)} data-pr-tooltip="CSV" label="CSV" />
            <Button type="button" icon="pi pi-file-excel" style={{ fontSize: '10px' }} severity="success" rounded onClick={exportExcel} data-pr-tooltip="XLS" label="XLSX" />
            <Button type="button" icon="pi pi-file-pdf" style={{ fontSize: '10px' }} severity="warning" rounded onClick={exportPdf} data-pr-tooltip="PDF" label="PDF" />
        </div>
    );

    const saveAsExcelFile = (buffer, fileName) => {
        import('file-saver').then((module) => {
            if (module && module.default) {
                let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
                let EXCEL_EXTENSION = '.xlsx';
                const data = new Blob([buffer], {
                    type: EXCEL_TYPE
                });
                module.default.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
            }
        });
    };

    const getPerfilesDeRiesgo = (data) => {
        return [...(data || [])].map((d) => {
            d.date = new Date(d.date);
            return d;
        });
    };

    const DrawerHeader = styled('div')(({ theme }) => ({
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    }));

    const toast = useRef(null);

    const showToast = (severityValue, summaryValue, detailValue, timeLife) => {
        toast?.current?.show({ severity: severityValue, summary: summaryValue, detail: detailValue, life: timeLife });
    }

    const monstrarAdjunto = (rowData) => {
        ; (async () => {
            let tokenApi = await ServicioWeb.obtenerToken(Variables.USER_LOGIN_API_BACKEND, Variables.PASSWORD_LOGIN_API_BACKEND, Variables.PETICION_BACKEND_ARBOL_TOKEN);
            let rowDetallado = {
                idPerfilRiesgo: rowData.idPerfilRiesgo
            }
            let requestBody = {
                method: 'POST',
                headers: {
                    Accept: "application/json",
                    'Content-Type': 'application/json',
                    'origin': 'x-request-with',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                    'Access-Control-Allow-Credentials': 'true',
                    'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                    Authorization: tokenApi
                },
                body: JSON.stringify(rowDetallado)
            }
            let rows = await ServicioWeb.obtenerDatosConBody(
                Variables.PETICION_BACKEND_TRAER_ARCHIVO_REPORTE_PERFIL,
                requestBody
            );
            if (rows.status) {
                let link;
                link = document.createElement('a');
                link.href = "archivos_reporte_perfil_riesgo/" + "SPR_" + rowData.idPerfilRiesgo + ".pdf";
                link.setAttribute("target", "_blank");
                link.click();
            } else {
                showToast('error', 'Error', rows.mensaje, 6000);
            }
        })();
    }

    const visualizarReporte = (rowData) => {
        if (rowData.estadoSolicitud === "Generado") {
            return (
                <React.Fragment>
                    <Button icon="pi pi-file-export" style={{ fontSize: '10px' }} rounded outlined className="mr-2" label="Visualizar Reporte" onClick={() => monstrarAdjunto(rowData)} />
                </React.Fragment>
            );
        } else {
            return (
                <></>
            );
        }
    }

    const archivoDeJustificacion = (rowData) => {
        let link;
        link = document.createElement('a');
        link.href = rowData.pathArchivoJustificacion.replace("/public", "") + "/" + rowData.archivoJustificacion;       
        link.setAttribute("target", "_blank");
        link.click();
    }

    const botonArchivoJustificacion = (rowData) => {
        if (rowData.archivoJustificacion !== null) {
            return (
                <React.Fragment>
                    <Button icon="pi pi-file-export" style={{ fontSize: '10px' }} rounded outlined className="mr-2" label="Visualizar Archivo Justificación" onClick={() => archivoDeJustificacion(rowData)} />
                </React.Fragment>
            );
        }else{
            <></>
        }
    }

    const colorColumnaEstado = (rowData) => {
        let color = "#f2f45c";//Amarillo
        switch (rowData.estadoSolicitud) {
            case "Error":
                color = "#ff0000";//Rojo
                break;
            case "Registrado":
                color = "#230dfa";//Azul
                break;
            case "Generado":
                color = "#00ff5d";//Verde
                break;
            default:
                color = "#f2f45c";//Amarillo
        }
        return (
            <React.Fragment>
                <Typography variant="h10" noWrap component="div" style={{ fontSize: '12px', color: color }}>{rowData.estadoSolicitud}</Typography>
            </React.Fragment>
        );
    }


    const paginatorLeft = <Button type="button" icon="pi pi-refresh" onClick={() => cargarDatosData()} />;

    return (
        <>
            <DrawerHeader />
            <div className="container_solicitar_perfil_riesgo">
                <Toast ref={toast} />
                <div className="panel_principal_solicitar_perfil_riesgo">
                    <Panel header={<h5>Reportes Solicitudes Perfil de Riesgo</h5>} className="mypanel">
                        <div className="tamanio_toolbar">
                            <Tooltip target=".export-buttons>button" position="bottom" />
                            <DataTable value={perfilesDeRiesgo} paginator
                                rows={10}
                                dataKey="idPerfilRiesgo"
                                rowsPerPageOptions={[10, 25, 50]}
                                ref={dt}
                                filters={filters}
                                filterDisplay="row"
                                loading={loading}
                                globalFilterFields={['numeroSolicitud', 'estadoSolicitud', 'usuarioCreacion', 'fechaCreacion', 'fechaActualizacion', 'rucSolicitado', 'razonSocialSolicitado', 'rucFechaInicioActividades']}
                                header={header}
                                emptyMessage="Información no encontrada"
                                paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                                currentPageReportTemplate="Mostrando {first} / {last} de un total de {totalRecords} registros"
                                paginatorLeft={paginatorLeft}>

                                <Column field="numeroSolicitud" sortable header="Número Solicitud" footer="Número Solicitud" filter filterPlaceholder="Buscar por Número Solicitud" style={{ fontSize: '10px' }} />
                                {/* <Column field="estadoSolicitud" sortable header="Estado Solicitud" footer="Estado Solicitud" filter filterPlaceholder="Buscar por Estado Solicitud" style={{ fontSize: '10px' }} /> */}
                                <Column body={colorColumnaEstado} sortable header="Estado Solicitud" footer="Estado Solicitud" filter filterPlaceholder="Buscar por Estado Solicitud" style={{ fontSize: '10px' }}></Column>
                                <Column field="usuarioCreacion" sortable header="Usuario Creación" footer="Usuario Creación" filter filterPlaceholder="Buscar por Usuario Creación" style={{ fontSize: '10px' }} />
                                <Column field="justificacion" sortable header="Justificación" footer="Justificación" filter filterPlaceholder="Buscar por Justificación" style={{ fontSize: '10px' }} />
                                <Column field="fechaCreacion" sortable header="Fecha Creación" footer="Fecha Creación" filter filterPlaceholder="Buscar por Fecha Creación" style={{ fontSize: '10px' }} />
                                <Column field="fechaActualizacion" sortable header="Fecha Actualización" footer="Fecha Actualización" filter filterPlaceholder="Buscar por Fecha Actualización" style={{ fontSize: '10px' }} />
                                <Column field="rucSolicitado" sortable header="Ruc Solicitado" footer="Ruc Solicitado" filter filterPlaceholder="Buscar por Ruc Solicitado" style={{ fontSize: '10px' }} />
                                <Column field="razonSocialSolicitado" sortable header="Razon Social Solicitado" footer="Razon Social Solicitado" filter filterPlaceholder="Buscar por Razon Social Solicitado" style={{ fontSize: '10px' }} />
                                <Column field="rucFechaInicioActividades" sortable header="Fecha Inicio Actividades" footer="Fecha Inicio Actividades" filter filterPlaceholder="Buscar por Fecha Inicio Actividades" style={{ fontSize: '10px' }} />
                                <Column body={botonArchivoJustificacion} header="Archivo de Justificación" footer="Archivo de Justificación" exportable={false} style={{ fontSize: '10px' }}></Column>
                                <Column body={visualizarReporte} header="Visualizar Reporte" footer="Visualizar Reporte" exportable={false} style={{ fontSize: '10px' }}></Column>
                            </DataTable>
                        </div>
                    </Panel>
                </div>
            </div>
        </>
    );
}

export default ReportesPerfilRiesgo;
