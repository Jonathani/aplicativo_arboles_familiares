import React, { useState, useRef } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { classNames } from 'primereact/utils';
import { styled } from '@mui/material/styles';
import { FilterMatchMode } from 'primereact/api';
import { Tooltip } from 'primereact/tooltip';
import { Column } from 'primereact/column';
import { DataTable } from 'primereact/datatable';
import { InputText } from 'primereact/inputtext';
import { Panel } from 'primereact/panel';
import { InputTextarea } from "primereact/inputtextarea";
import { Button } from 'primereact/button';
import { Toast } from 'primereact/toast';
import { FileUpload } from 'primereact/fileupload';
import { ConfirmDialog } from 'primereact/confirmdialog';
import { Tag } from 'primereact/tag';
import Cookies from 'js-cookie';
import Variables from '../utilidades/Variables';
import Mensajes from '../utilidades/Mensajes';
import ServicioWeb from '../utilidades/ServicioWeb';
import Utilitario from '../utilidades/Utilitario';
import { ScrollPanel } from 'primereact/scrollpanel';
import './ConsultarArboles.css'

function ConsultarArboles() {
    let objetoSesion = "";
    let informacionSesionUsuario = Cookies.get('infoSesionUsuario');
    objetoSesion = !!informacionSesionUsuario ? JSON.parse(informacionSesionUsuario) : "";

    const [blocked, setBlocked] = useState(false);

    const toast = useRef(null);

    const DrawerHeader = styled('div')(({ theme }) => ({
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    }));

    const getFormErrorMessage = (name) => {
        return errors[name] ? <small className="p-error">{errors[name].message}</small> : <small className="p-error">&nbsp;</small>;
    };

    const showToast = (severityValue, summaryValue, detailValue, timeLife) => {
        toast?.current?.show({ severity: severityValue, summary: summaryValue, detail: detailValue, life: timeLife });
    }

    const defaultValues = { cedula: '', justificacion: '', archivoJustificacion: '' };
    const form = useForm({ defaultValues });
    const errors = form.formState.errors;

    //Tamaño de archivos
    const [totalSizeJust, setTotalSizeJust] = useState(0);
    const [cedula, setCedula] = useState("");
    const [justificacionDetalleConn, setJustificacionDetalleConn] = useState("");
    const [archivoJustificacion, setArchivoJustificacion] = useState();
    const [visibleConsultar, setVisibleConsultar] = useState(false);

    //Peticion a consulta de Arboles
    const [loading, setLoading] = useState(false);
    const [consultaArboles, setConsultaArboles] = useState(null);
    const [globalFilterValue, setGlobalFilterValue] = useState('');
    const [arregloRespuesta, setArregloRespuesta] = React.useState([]);
    const [arregloRespuestaFinal, setArregloRespuestaFinal] = React.useState([]);
    const dt = useRef(null);

    const [filters, setFilters] = useState({
        global: { value: null, matchMode: FilterMatchMode.CONTAINS },
        cedula: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        cedulaConyuge: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        cedulaMadre: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        cedulaPadre: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        estadoCivil: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        fechaNacimiento: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        identificacionUsuarioConsultado: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        nivel: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        nombreConyuge: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        nombresMadre: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        nombresPadre: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        nombresUsuario: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        nombresUsuarioConsultado: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        tipo: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
    });

    const cols = [
        { field: 'cedula', header: 'Cédula' },
        { field: 'nombresUsuario', header: 'Nombres Usuario' },
        { field: 'fechaNacimiento', header: 'Fecha Nacimiento' },
        { field: 'estadoCivil', header: 'Estado Civil' },
        { field: 'cedulaPadre', header: 'Cédula Padre' },
        { field: 'nombresPadre', header: 'Nombres Padre' },
        { field: 'cedulaMadre', header: 'Cédula Madre' },
        { field: 'nombresMadre', header: 'Nombres Madre' },
        { field: 'cedulaConyuge', header: 'Cédula Conyuge' },
        { field: 'nombreConyuge', header: 'Nombre Cónyuge' },
        { field: 'nivel', header: 'Nivel' },
        { field: 'tipo', header: 'Tipo' },
        { field: 'identificacionUsuarioConsultado', header: 'Identificación Usuario Consultado' },
        { field: 'nombresUsuarioConsultado', header: 'Nombres Usuario Consultado' },

    ];

    const exportColumns = cols.map((col) => ({ title: col.header, dataKey: col.field }));

    const saveAsExcelFile = (buffer, fileName) => {
        import('file-saver').then((module) => {
            if (module && module.default) {
                let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
                let EXCEL_EXTENSION = '.xlsx';
                const data = new Blob([buffer], {
                    type: EXCEL_TYPE
                });
                module.default.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
            }
        });
    };

    const onGlobalFilterChange = (e) => {
        const value = e.target.value;
        let _filters = { ...filters };
        _filters['global'].value = value;
        setFilters(_filters);
        setGlobalFilterValue(value);
    };

    const realizarConsulta = () => {
        let respuestaDeConsulta = []
        let respuestaDeConsultaAux;
        if (archivoJustificacion.size < Variables.TAMANIO_DEL_ARCHIVO) {
            setConsultaArboles([])
            setArregloRespuestaFinal([])
            setArregloRespuesta([])
            let arregloCedulas = cedula.trim().split(",");
            ; (async () => {
                setBlocked(true);
                setLoading(true);
                //Consulta de los arboles familiares
                let tokenWS = await ServicioWeb.obtenerToken(Variables.USER_LOGIN_API_BACKEND, Variables.PASSWORD_LOGIN_API_BACKEND, Variables.PETICION_ARBOL_TOKEN);
                for (var i = 0; i < arregloCedulas.length; i++) {
                    let rowDetallado = {
                        cedulasConsultar: arregloCedulas[i].trim()
                    }
                    let requestBody = {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'Access-Control-Allow-Origin': '*',
                            'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                            'Access-Control-Allow-Credentials': 'true',
                            'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                            'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                            Authorization: tokenWS
                        },
                        body: JSON.stringify(rowDetallado)
                    }
                    let rows = await ServicioWeb.obtenerDatosConBody(
                        Variables.PETICION_ARBOL_CONSULTA,
                        requestBody
                    )
                    arregloRespuesta.push(Utilitario.ordenarArbolesFamiliares(rows.consulta))
                }
                if (arregloRespuesta.length !== 0) {
                    for (var x = 0; x < arregloRespuesta.length; x++) {
                        let arregloOut = arregloRespuesta[x]
                        for (var y = 0; y < arregloOut.length; y++) {
                            arregloRespuestaFinal.push(arregloOut[y])
                            respuestaDeConsulta.push(JSON.stringify(arregloOut[y]))
                        }
                    }
                    setConsultaArboles(arregloRespuestaFinal)
                }

                //PETICION_BACKEND_ARBOL_TOKEN
                let tokenBackend = await ServicioWeb.obtenerToken(Variables.USER_LOGIN_API_BACKEND, Variables.PASSWORD_LOGIN_API_BACKEND, Variables.PETICION_BACKEND_ARBOL_TOKEN);

                //Guardar la consulta de los arboles familiares
                for (var z = 0; z < respuestaDeConsulta.length; z++) {
                    respuestaDeConsultaAux = respuestaDeConsultaAux + respuestaDeConsulta[z] + "|"
                }
                let formData = new FormData();
                formData.append("cedulasConsultadas", cedula.trim());
                formData.append("idUsuario", objetoSesion['idUsuario']);
                formData.append("motivoConsulta", justificacionDetalleConn.trim());
                formData.append('archivoRespaldo', archivoJustificacion);
                formData.append('respuestaCedulasConsultadas', respuestaDeConsultaAux.replace("undefined", ""));
                let requestBodyGuardado = {
                    method: 'POST',
                    headers: {
                        Authorization: tokenBackend
                    },
                    body: formData,
                };
                let rowsGuardado = await ServicioWeb.obtenerDatosConBody(
                    Variables.PETICION_BACKEND_ARBOL_REGISTRAR_CONSULTA,
                    requestBodyGuardado,
                );
                if (rowsGuardado.status) {
                    showToast('success', 'Success', rowsGuardado.mensaje, 6000);
                } else {
                    showToast('error', 'Error', rowsGuardado.mensaje, 6000);
                }
                setBlocked(false);
                setLoading(false);
            })()
        } else {
            showToast('error', 'Error', Mensajes.MENSAJE_ARCHIVO_SIN_TAMANIO_PERMITIDO, 6000);
        }
    }

    const exportCSV = (selectionOnly) => {
        dt.current.exportCSV({ selectionOnly });
    };

    const exportPdf = () => {
        import('jspdf').then((jsPDF) => {
            import('jspdf-autotable').then(() => {
                const doc = new jsPDF.default(
                    {
                        orientation: 'landscape',
                        unit: 'pt',
                        format: '',
                        compressPdf: false
                    }
                );
                doc.autoTable(exportColumns, consultaArboles);
                doc.save('arboles.pdf');
            });
        });
    };

    const exportExcel = () => {
        import('xlsx').then((xlsx) => {
            const worksheet = xlsx.utils.json_to_sheet(consultaArboles);
            const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
            const excelBuffer = xlsx.write(workbook, {
                bookType: 'xlsx',
                type: 'array'
            });
            saveAsExcelFile(excelBuffer, 'arbolesFamiliares');
        });
    };

    const header = (
        <div className="flex justify-content-end">
            <span className="p-input-icon-left">
                <i className="pi pi-search" />
                <InputText value={globalFilterValue} onChange={onGlobalFilterChange} placeholder="Buscar" />
            </span>
            <br></br>
            <Button type="button" icon="pi pi-file" style={{ fontSize: '10px' }} rounded onClick={() => exportCSV(false)} data-pr-tooltip="CSV" label="CSV" />
            <Button type="button" icon="pi pi-file-excel" style={{ fontSize: '10px' }} severity="success" rounded onClick={exportExcel} data-pr-tooltip="XLS" label="XLSX" />
            <Button type="button" icon="pi pi-file-pdf" style={{ fontSize: '10px' }} severity="warning" rounded onClick={exportPdf} data-pr-tooltip="PDF" label="PDF" />
        </div>
    );

    const chooseOptionsPdf = {label: 'Escoger', icon: 'pi pi-fw pi-plus', style: {'fontSize': '10px'}};
    const uploadOptionsPdf = {label: 'Subir', icon: 'pi pi-upload', className: 'p-button-success', style: {'fontSize': '10px'}};
    const cancelOptionsPdf = {label: 'Cancelar', icon: 'pi pi-times', className: 'p-button-danger', style: {'fontSize': '10px'}};

    const onTemplateRemoveJust = (file, callback) => {
        setTotalSizeJust(totalSizeJust - file.size);
        setArchivoJustificacion("")
        callback();
    };

    //Archivo de justificación
    const metodoSubirArchivoJust = (event) => {
        let file = event.files[0];
        let nombreArchivo = file.name;
        let tipoArchivo = nombreArchivo.split(".");
        if (tipoArchivo[(tipoArchivo.length) - 1].toLowerCase() === 'pdf' || tipoArchivo[(tipoArchivo.length) - 1].toLowerCase() === 'pdf') {
            setArchivoJustificacion(file)
            showToast('success', 'Correcto', Mensajes.ARCHIVO_DE_SUBIDA_PERFIL_RIESGO, 6000);
        } else {
            setArchivoJustificacion("")
            showToast('error', 'Error', Mensajes.ARCHIVO_DE_CON_FORMATO_INCORRECTO, 6000);
        }
    }

    const aceptarConsulta = (data) => {
        if (archivoJustificacion) {
            setVisibleConsultar(true)
            setCedula(data.cedula)
            setJustificacionDetalleConn(data.justificacion)
        } else {
            showToast('error', 'Error', Mensajes.ARCHIVO_DE_SUBIDA_PERFIL_RIESGO_NO_VALIDO, 6000);
        }
    }

    const itemTemplateArchivoJust = (file, props) => {
        return (
            <div className="flex align-items-center flex-wrap">
                <div className="flex align-items-center" style={{ width: '40%' }}>
                    <img
                        alt={file.name}
                        role="presentation"
                        src={file.objectURL}
                        width={100}
                    />
                    <span className="flex flex-column text-left ml-3">
                        {file.name}
                        <small>{new Date().toLocaleDateString()}</small>
                    </span>
                </div>
                <Tag
                    value={props.formatSize}
                    severity="warning"
                    className="px-3 py-2"
                />
                <Button
                    type="button"
                    icon="pi pi-times"
                    className="p-button-outlined p-button-rounded p-button-danger ml-auto"
                    onClick={() => onTemplateRemoveJust(file, props.onRemove)}
                />
            </div>
        );
    };

    return (
        <>
            <DrawerHeader />
            <ConfirmDialog visible={visibleConsultar} onHide={() => setVisibleConsultar(false)} message={Mensajes.ACEPTAR_CONSULTA_ARBOL_FAMILIAR}
                header="Confirmación" icon="pi pi-exclamation-triangle" accept={realizarConsulta} />
            <div className="container_consultar_arboles">
                <Toast ref={toast} />
                <div className="panel_principal_consultar_arboles">
                    <Panel header={<h5>Consultar Árboles Familiares</h5>} className="mypanel">
                        <form onSubmit={form.handleSubmit(aceptarConsulta)} className="flex flex-column gap-2">
                            <div className="grid">
                                <div className="col-12 md:col-4 mb-4 px-6">
                                    <Controller
                                        name="cedula"
                                        control={form.control}
                                        rules={{ required: 'Las/La cédula/s es/son obligatoria.' }}
                                        render={({ field, fieldState }) => (
                                            <>
                                                <label style={{ fontSize: '10px' }} htmlFor={field.name} className={classNames({ 'p-error': errors.value })}>Cédula: <span className='required'>*</span></label>
                                                <span className="p-float-label">
                                                    <InputTextarea id={field.name} {...field}
                                                        style={{ fontSize: '10px' }}
                                                        maxLength={60}
                                                        rows={4} cols={100}
                                                        className={classNames({ 'p-invalid': fieldState.error })}
                                                        autoFocus
                                                        onKeyPress={(event) => {
                                                            if (!(event.which >= 48 && event.which <= 57 || event.which === 44)) {
                                                                event.preventDefault();
                                                            }
                                                        }} />
                                                    <label htmlFor={field.name}></label>
                                                </span>
                                                {getFormErrorMessage(field.name)}
                                            </>
                                        )}
                                    />
                                    <br></br>
                                    <Controller
                                        name="justificacion"
                                        control={form.control}
                                        rules={{ required: 'La justificación es obligatoria.' }}
                                        render={({ field, fieldState }) => (
                                            <>
                                                <label style={{ fontSize: '10px' }} htmlFor={field.name} className={classNames({ 'p-error': errors.value })}>Justificación: <span className='required'>*</span></label>
                                                <span className="p-float-label">
                                                    <InputTextarea id={field.name} {...field}
                                                        style={{ fontSize: '10px' }}
                                                        maxLength={60}
                                                        rows={4}
                                                        cols={100}
                                                        className={classNames({ 'p-invalid': fieldState.error })} />
                                                    <label htmlFor={field.name}></label>
                                                </span>
                                                {getFormErrorMessage(field.name)}
                                            </>
                                        )}
                                    />
                                    <br></br>
                                    <Controller
                                        name="archivoJustificacion"
                                        control={form.control}
                                        rules={{ required: 'El archivo de justificación es obligatorio.' }}
                                        render={({ field, fieldState }) => (
                                            <>
                                                <label style={{ fontSize: '10px' }} htmlFor={field.name} className={classNames({ 'p-error': errors.value })}>Archivo de Justificación: <span className='required'>*</span></label>
                                                <span className="p-float-label">
                                                    <FileUpload name="demo[]"
                                                        id={field.name} {...field}
                                                        style={{ fontSize: '10px' }}
                                                        url={'/api/upload'}
                                                        multiple={false}
                                                        accept=".pdf"
                                                        className={classNames({ 'p-invalid': fieldState.error })}
                                                        maxFileSize={Variables.TAMANIO_DEL_ARCHIVO}
                                                        customUpload
                                                        chooseOptions={chooseOptionsPdf} uploadOptions={uploadOptionsPdf} cancelOptions={cancelOptionsPdf}
                                                        uploadHandler={metodoSubirArchivoJust}
                                                        itemTemplate={itemTemplateArchivoJust}
                                                        emptyTemplate={<p className="m-0">Arrastre aqui el archivo cpc solicitado a subir o de clic en el boton "Escoger"</p>}
                                                    />
                                                    <label htmlFor={field.name}></label>
                                                </span>
                                                {getFormErrorMessage(field.name)}
                                            </>
                                        )}
                                    />
                                    <br></br>
                                    <br></br>
                                    <Button label="Consultar" type="submit" icon="pi pi-search" loading={blocked} style={{ fontSize: '10px' }} />
                                </div>
                            </div>
                        </form>
                        <ScrollPanel style={{ width: '85%' }} className="custombar2">
                            {/* <div className="tamanio_toolbar"> */}
                            <Tooltip target=".export-buttons>button" position="bottom" />
                            <DataTable value={consultaArboles}
                                paginator
                                showGridlines
                                rows={10}
                                dataKey={(row) => row.cedula + row.tipo}
                                rowsPerPageOptions={[10, 25, 50]}
                                ref={dt}
                                filters={filters}
                                filterDisplay="row"
                                loading={loading}
                                globalFilterFields={['cedula', 'cedulaConyuge', 'cedulaMadre', 'cedulaPadre', 'estadoCivil', 'fechaNacimiento',
                                    'identificacionUsuarioConsultado', 'nivel', 'nombreConyuge', 'nombresMadre', 'nombresPadre', 'nombresUsuario',
                                    'tipo', 'nombresUsuarioConsultado']}
                                header={header}
                                scrollable="true"
                                emptyMessage="Información no encontrada"
                                paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                                currentPageReportTemplate="Showing {first} to {last} of {totalRecords} entries">

                                <Column sortable field="nombresUsuario" header="NOMBRE USUARIO" footer="NOMBRE USUARIO" filter filterPlaceholder="Buscar por Nombre Usuario" style={{ fontSize: '10px' }} />
                                <Column sortable field="fechaNacimiento" header="FECHA NACIMIENTO" footer="FECHA NACIMIENTO" filter filterPlaceholder="Buscar por Fecha Nacimiento" style={{ fontSize: '10px' }} />
                                <Column sortable field="estadoCivil" header="ESTADO CIVIL" footer="ESTADO CIVIL" filter filterPlaceholder="Buscar por Estado Civil" style={{ fontSize: '10px' }} />
                                <Column sortable field="cedulaPadre" header="CEDULA PADRE" footer="CEDULA PADRE" filter filterPlaceholder="Buscar por Cédula Padre" style={{ fontSize: '10px' }} />
                                <Column sortable field="nombresPadre" header="NOMBRE PADRE" footer="NOMBRE PADRE" filter filterPlaceholder="Buscar por Nombre Padre" style={{ fontSize: '10px' }} />
                                <Column sortable field="cedulaMadre" header="CEDULA MADRE" footer="CEDULA MADRE" filter filterPlaceholder="Buscar por Cédula Madre" style={{ fontSize: '10px' }} />
                                <Column sortable field="nombresMadre" header="NOMBRE MADRE" footer="NOMBRE MADRE" filter filterPlaceholder="Buscar por Nombre Madre" style={{ fontSize: '10px' }} />
                                <Column sortable field="cedulaConyuge" header="CEDULA CONYUGE" footer="CEDULA CONYUGE" filter filterPlaceholder="Buscar por Cédula Cónyugue" style={{ fontSize: '10px' }} />
                                <Column sortable field="nombreConyuge" header="NOMBRE CONYUGE" footer="NOMBRE CONYUGE" filter filterPlaceholder="Buscar por Nombre Cónyugue" style={{ fontSize: '10px' }} />
                                <Column sortable field="nivel" header="NIVEL" footer="NIVEL" filter filterPlaceholder="Buscar por Nivel" style={{ fontSize: '10px' }} />
                                <Column sortable field="tipo" header="TIPO" footer="TIPO" filter filterPlaceholder="Buscar por Tipo" style={{ fontSize: '10px' }} />
                                <Column sortable field="identificacionUsuarioConsultado" header="CEDULA CONSULTADO" footer="CEDULA CONSULTADO" filter filterPlaceholder="Buscar por Cédula Consultado" style={{ fontSize: '10px' }} />
                                <Column sortable field="nombresUsuarioConsultado" header="NOMBRE CONSULTADO" footer="NOMBRE CONSULTADO" filter filterPlaceholder="Buscar por Nombre Consultado" style={{ fontSize: '10px' }} />
                            </DataTable>
                            {/* </div> */}
                        </ScrollPanel>
                    </Panel>
                </div>
            </div>
        </>
    )
}

export default ConsultarArboles