import React, { useState, useEffect, useRef } from 'react';
import { styled } from '@mui/material/styles';
import { Column } from 'primereact/column';
import { Panel } from 'primereact/panel';
import { FilterMatchMode } from 'primereact/api';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { DataTable } from 'primereact/datatable';
import Cookies from 'js-cookie';
import * as XLSX from 'xlsx';
import ReporteConsultasService from '../service/ReporteConsultasService';
import Utilitario from '../utilidades/Utilitario';
import './ReporteConsultas.css'

function ReporteConsultas() {

    let objetoSesion = "";
    let informacionSesionUsuario = Cookies.get('infoSesionUsuario');
    objetoSesion = !!informacionSesionUsuario ? JSON.parse(informacionSesionUsuario) : "";

    const dt = useRef(null);
    const [reporteConsultas, setReporteConsultas] = useState([]);
    const [loading, setLoading] = useState(true);
    const [globalFilterValue, setGlobalFilterValue] = useState('');

    const DrawerHeader = styled('div')(({ theme }) => ({
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    }));

    const cargarDatosData = () => {
        ReporteConsultasService.getReporteConsultasMedium().then((data) => {
            setReporteConsultas(getReporteConsultas(data));
            setLoading(false);
        });
    }

    useEffect(() => {
        cargarDatosData();
    }, []);


    const getReporteConsultas = (data) => {
        return [...(data || [])].map((d) => {
            d.date = new Date(d.date);
            return d;
        });
    };

    const [filters, setFilters] = useState({
        global: { value: null, matchMode: FilterMatchMode.CONTAINS },
        identificacion: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        nombreCompleto: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        cedulasConsultadas: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        motivoConsulta: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        fechaConsulta: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
    });

    const onGlobalFilterChange = (e) => {
        const value = e.target.value;
        let _filters = { ...filters };
        _filters['global'].value = value;
        setFilters(_filters);
        setGlobalFilterValue(value);
    };    

    const cols = [
        { field: 'identificacion', header: 'Identificación' },
        { field: 'nombreCompleto', header: 'Nombre Completo' },
        { field: 'cedulasConsultadas', header: 'Cedulas Consultadas' },
        { field: 'motivoConsulta', header: 'Motivo Consulta' },
        { field: 'fechaConsulta', header: 'Fecha Consulta' },
    ];

    const exportColumns = cols.map((col) => ({ title: col.header, dataKey: col.field }));

    const exportCSV = (selectionOnly) => {
        dt.current.exportCSV({ selectionOnly });
    };

    const exportPdf = () => {
        import('jspdf').then((jsPDF) => {
            import('jspdf-autotable').then(() => {
                const doc = new jsPDF.default(
                    {
                        orientation: 'landscape',
                        unit: 'pt',
                        format: '',
                        compressPdf: false
                    }
                );
                doc.autoTable(exportColumns, reporteConsultas);
                doc.save('arboles.pdf');
            });
        });
    };

    const saveAsExcelFile = (buffer, fileName) => {
        import('file-saver').then((module) => {
            if (module && module.default) {
                let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
                let EXCEL_EXTENSION = '.xlsx';
                const data = new Blob([buffer], {
                    type: EXCEL_TYPE
                });                
                module.default.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
            }
        });
    };

    const exportExcel = () => {
        import('xlsx').then((xlsx) => {
            const worksheet = xlsx.utils.json_to_sheet(Utilitario.exportarSoloDataExcel(reporteConsultas));
            const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
            const excelBuffer = xlsx.write(workbook, {
                bookType: 'xlsx',
                type: 'array'
            });
            saveAsExcelFile(excelBuffer, 'reporteConsultas');
        });
    };    

    const header = (
        <div className="flex justify-content-end">
            <span className="p-input-icon-left">
                <i className="pi pi-search" />
                <InputText value={globalFilterValue} onChange={onGlobalFilterChange} placeholder="Buscar" />
            </span>
            <br></br>
            <Button type="button" icon="pi pi-file" style={{ fontSize: '10px' }} rounded onClick={() => exportCSV(false)} data-pr-tooltip="CSV" label="CSV" />
            <Button type="button" icon="pi pi-file-excel" style={{ fontSize: '10px' }} severity="success" rounded onClick={exportExcel} data-pr-tooltip="XLS" label="XLSX" />
            <Button type="button" icon="pi pi-file-pdf" style={{ fontSize: '10px' }} severity="warning" rounded onClick={exportPdf} data-pr-tooltip="PDF" label="PDF" />
        </div>
    );

    const mostrarRespuestaArbol = (rowData) => {
        let nombreArchivo = rowData.nombreArchivoRespaldo.split(".");
        let arrayObjecto = [];
        let respuestaConsultada = rowData.respuestaCedulasConsultadas.split("|");
        for (var i = 0; i < respuestaConsultada.length; i++) {
            if (respuestaConsultada[i] !== "") {
                arrayObjecto.push(JSON.parse(respuestaConsultada[i]))
            }
        }
        const worksheet = XLSX.utils.json_to_sheet(arrayObjecto);
        const workbook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workbook, worksheet, "Sheet1");
        XLSX.writeFile(workbook, nombreArchivo[0] + ".xlsx");
    }

    const visualizarRespuesta = (rowData) => {
        return (
            <React.Fragment>
                <Button icon="pi pi-table" style={{ fontSize: '10px' }} rounded outlined className="mr-2" label="Visualizar Respuesta" onClick={() => mostrarRespuestaArbol(rowData)} />
            </React.Fragment>
        );
    }

    const monstrarAdjunto = (rowData) => {
        let link;
        link = document.createElement('a');
        link.href = rowData.pathArchivoRespaldo.replace("/public", "") + "/" + rowData.nombreArchivoRespaldo;
        //link.href = rowData.pathArchivoRespaldo.replace("/public", "") + "/" + "entity.pdf";        
        link.setAttribute("target", "_blank");
        link.click();
    }

    const visualizarAdjunto = (rowData) => {
        return (
            <React.Fragment>
                <Button icon="pi pi-file-export" style={{ fontSize: '10px' }} rounded outlined className="mr-2" label="Visualizar Adjunto" onClick={() => monstrarAdjunto(rowData)} />
            </React.Fragment>
        );
    }

    return (
        <>
            <DrawerHeader />
            <div className="container_reporte_consultas">
                <div className="panel_principal_reporte_consultas">
                    <Panel header="Reporte de las Consultas">
                        <div className="tamanio_toolbar">
                            <DataTable value={reporteConsultas} 
                                paginator 
                                rows={10}
                                dataKey="idConsulta"
                                rowsPerPageOptions={[10, 25, 50]}
                                ref={dt}
                                filters={filters}
                                filterDisplay="row"
                                loading={loading}
                                globalFilterFields={['identificacion', 'nombreCompleto', 'cedulasConsultadas', 'motivoConsulta', 'fechaConsulta']}
                                header={header}
                                emptyMessage="Información no encontrada"
                                paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                                currentPageReportTemplate="Showing {first} to {last} of {totalRecords} entries">

                                <Column sortable field="identificacion" header="Identificacion" footer="Identificacion" filter filterPlaceholder="Buscar por Identificacion" style={{ fontSize: '10px' }} />
                                <Column sortable field="nombreCompleto" header="Nombre Completo" footer="Nombre Completo" filter filterPlaceholder="Buscar por Nombre Completo" style={{ fontSize: '10px' }} />
                                <Column sortable field="cedulasConsultadas" header="Cedulas Consultadas" footer="Cedulas Consultadas" filter filterPlaceholder="Buscar por Cedulas Consultadas" style={{ fontSize: '10px' }} />
                                <Column sortable field="motivoConsulta" header="Motivo Consulta" footer="Motivo Consulta" filter filterPlaceholder="Buscar por Motivo Consulta" style={{ fontSize: '10px' }} />
                                <Column sortable field="fechaConsulta" header="Fecha Consulta" footer="Fecha Consulta" filter filterPlaceholder="Buscar por Fecha Consulta" style={{ fontSize: '10px' }} />
                                <Column body={visualizarRespuesta} header="Visualizar Respuesta" footer="Visualizar Respuesta"  exportable={false} style={{ fontSize: '10px' }}></Column>
                                <Column body={visualizarAdjunto} header="Visualizar Adjunto" footer="Visualizar Adjunto"  exportable={false} style={{ fontSize: '10px' }}></Column>
                            </DataTable>
                        </div>
                    </Panel>
                </div>
            </div>
        </>
    )
}

export default ReporteConsultas
