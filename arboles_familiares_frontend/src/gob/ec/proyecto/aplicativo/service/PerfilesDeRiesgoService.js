import ServicioWeb from "../utilidades/ServicioWeb";
import Variables from "../utilidades/Variables";
import Cookies from 'js-cookie';

class PerfilesDeRiesgoService {

    static async getData() {
        let informacionSesionUsuario = Cookies.get('infoSesionUsuario');
        let objetoSesion = !!informacionSesionUsuario ? JSON.parse(informacionSesionUsuario) : "";
        let tokenApi = await ServicioWeb.obtenerToken(Variables.USER_LOGIN_API_BACKEND, Variables.PASSWORD_LOGIN_API_BACKEND, Variables.PETICION_BACKEND_ARBOL_TOKEN);
        let rowDetallado = {
            idUsuario: objetoSesion.idUsuario,
            idPerfil: objetoSesion.perfilId
        }
        let requestBody = {
            method: 'POST',
            headers: {
                Accept: "application/json",
                'Content-Type': 'application/json',
                'origin': 'x-request-with',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                Authorization: tokenApi
            },
            body: JSON.stringify(rowDetallado)
        }
        let rows = await ServicioWeb.obtenerDatosConBody(
            Variables.PETICION_BACKEND_LISTAR_PERFILES_DE_RIESGO,
            requestBody
        );
        return rows.perfiles
    }

    static async getPerfilesDeRiesgoMedium (){
        return Promise.resolve(this.getData());
    }


}

export default PerfilesDeRiesgoService;