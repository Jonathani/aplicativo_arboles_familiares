import ServicioWeb from "../utilidades/ServicioWeb";
import Variables from "../utilidades/Variables";

class GestionUsuariosService {

    static async getData() {
        let tokenApi = await ServicioWeb.obtenerToken(Variables.USER_LOGIN_API_BACKEND, Variables.PASSWORD_LOGIN_API_BACKEND, Variables.PETICION_BACKEND_ARBOL_TOKEN);
        let requestBody = {
            method: 'POST',
            headers: {
                Accept: "application/json",
                'Content-Type': 'application/json',
                'origin': 'x-request-with',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                Authorization: tokenApi
            }
        }
        let rows = await ServicioWeb.obtenerDatosConBody(
            Variables.PETICION_BACKEND_LISTA_USUARIOS,
            requestBody
        );
        return rows.mensaje;
    }

    static async getGestionUsuariosMedium (){
        return Promise.resolve(this.getData());
    }
}

export default GestionUsuariosService;