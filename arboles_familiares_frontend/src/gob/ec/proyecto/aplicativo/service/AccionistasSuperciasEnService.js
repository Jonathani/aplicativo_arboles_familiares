import ServicioWeb from "../utilidades/ServicioWeb";
import Variables from "../utilidades/Variables";

class AccionistasSuperciasEn {

    static async getData(idVariableConsulta) {
        let tokenApi = await ServicioWeb.obtenerTokenEtl(Variables.USER_LOGIN_API_BACKEND, Variables.PASSWORD_LOGIN_API_BACKEND, Variables.PETICION_APIS_ETLS_TOKEN);
        let rowDetallado = {
            idVariableConsulta: idVariableConsulta
        }
        let requestBody = {
            method: 'POST',
            headers: {
                Accept: "application/json",
                'Content-Type': 'application/json',
                'origin': 'x-request-with',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                Authorization: tokenApi
            },
            body: JSON.stringify(rowDetallado)
        }
        let rows = await ServicioWeb.obtenerDatosConBody(
            Variables.PETICION_APIS_MOSTRAR_ACCIONISTAS_SUPERCIAS_EN,
            requestBody
        );
        return rows.listaRespuesta
    }

    static async getAccionistasSuperciasEnMedium (idVariableConsulta){
        return Promise.resolve(this.getData(idVariableConsulta));
    }
}

export default AccionistasSuperciasEn;