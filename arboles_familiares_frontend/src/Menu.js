
import * as React from 'react';
import { useRef, useEffect } from "react";
import ReactDOM, { createRoot } from 'react-dom/client';
import * as Icons from "react-icons/fa";
import {
    useNavigate,
    Routes,
    Route,
    BrowserRouter as Router
} from "react-router-dom";
import { styled, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';
import MuiAppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Collapse from '@mui/material/Collapse';
import Add from "@mui/icons-material/Add";
import ExitToApp from "@mui/icons-material/ExitToApp";
import Remove from "@mui/icons-material/Remove";
import { Panel } from 'primereact/panel';
import Cookies from 'js-cookie';
import { Button } from 'primereact/button';
import GestionUsuarios from './gob/ec/sercop/dirRiesgos/administracion/GestionUsuarios';
import CambiarContrasenia from './gob/ec/sercop/dirRiesgos/administracion/CambiarContrasenia';
import ConsultarArboles from './gob/ec/sercop/dirRiesgos/arbolesFamiliares/ConsultarArboles';
import ReporteConsultas from './gob/ec/sercop/dirRiesgos/arbolesFamiliares/ReporteConsultas';
import EjecutarRiesgoFactico from './gob/ec/sercop/dirRiesgos/riesgoFactico/EjecutarRiesgoFactico';
import SolicitarPerfilRiesgo from './gob/ec/sercop/dirRiesgos/perfilDeRiesgo/SolicitarPerfilRiesgo';
import ReportesPerfilRiesgo from './gob/ec/sercop/dirRiesgos/perfilDeRiesgo/ReportesPerfilRiesgo';
import ConsultarHistorialLaboral from './gob/ec/sercop/dirRiesgos/historialLaboral/ConsultarHistorialLaboral';
import ListarHistorialLaboral from './gob/ec/sercop/dirRiesgos/historialLaboral/ListarHistorialLaboral';
import MenuService from './gob/ec/sercop/dirRiesgos/service/MenuService';
import Login from './Login';
import './Menu.css'

const drawerWidth = 250;

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(
    ({ theme, open }) => ({
        flexGrow: 1,
        padding: theme.spacing(3),
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: `-${drawerWidth}px`,
        ...(open && {
            transition: theme.transitions.create('margin', {
                easing: theme.transitions.easing.easeOut,
                duration: theme.transitions.duration.enteringScreen,
            }),
            marginLeft: 0,
        }),
    }),
);

const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
    transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    ...(open && {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: `${drawerWidth}px`,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    }),
}));

const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
}));

function Menu() {

    const navigate = useNavigate();
    const theme = useTheme();
    const [open, setOpen] = React.useState(false);
    const ref = useRef(null);
    const [openArbolesFamiliares, setOpenArbolesFamiliares] = React.useState(false);
    const [openGestionDeUsuarios, setOpenGestionDeUsuarios] = React.useState(false);
    const [openRiesgoFactico, setOpenRiesgoFactico] = React.useState(false);
    const [openPerfilDeRiesgo, setOpenPerfilDeRiesgo] = React.useState(false);
    const [openReportesPerfilDeRiesgo, setReportesOpenPerfilDeRiesgo] = React.useState(false);
    const [openConsultarHistorialLaboral, setOpenConsultarHistorialLaboral] = React.useState(false);
    const [openListarHistorialLaboral, setOpenListarHistorialLaboral] = React.useState(false);
    const [menusUsuario, setMenusUsuario] = React.useState([]);
    let informacionSesionUsuario = Cookies.get('infoSesionUsuario');
    let objetoSesion = !!informacionSesionUsuario ? JSON.parse(informacionSesionUsuario) : "";

    const cargarDatosData = () => {
        MenuService.getMenuMedium().then((data) => {
            setMenusUsuario(getMenusUsuario(data));
        });
    }

    const getMenusUsuario = (data) => {
        return [...(data || [])].map((d) => {
            d.date = new Date(d.date);
            return d;
        });
    };

    useEffect(() => {
        cargarDatosData();
    }, []);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    // -------- INICIO: No borrar estos métodos, ya que se usan para la generacion y manejo de los menus de usuarios de sesion, estan guardado en bdd ----//

    const handleClickArboles = () => {
        setOpenArbolesFamiliares(!openArbolesFamiliares);
    };

    const handleClickGestionArb = () => {
        setOpenGestionDeUsuarios(!openGestionDeUsuarios);
    };

    const handleClickRiesgoFactico = () => {
        setOpenRiesgoFactico(!openRiesgoFactico);
    };

    const handleClickPerfilDeRiesgo = () => {
        setOpenPerfilDeRiesgo(!openPerfilDeRiesgo);
    }

    const handleClickReportesPerfilDeRiesgo = () => {
        setReportesOpenPerfilDeRiesgo(!openReportesPerfilDeRiesgo);
    }

    const handleClickConsultarHistorialLaboral = () => {
        setOpenConsultarHistorialLaboral(!openConsultarHistorialLaboral);
    }

    const handleClickListarHistorialLaboral = () => {
        setOpenListarHistorialLaboral(!openListarHistorialLaboral);
    }

    const cambiarAGestionUsuarios = () => {
        const root = ReactDOM.createRoot(document.getElementById("idContenido"));
        root.render(
            <React.StrictMode>
                <Router>
                    <Routes>
                        <Route path="/" element={<Login />} />
                        <Route path="/login" element={<Login />} />
                    </Routes>
                    <GestionUsuarios />
                </Router>
            </React.StrictMode>
        );
    }

    const cambiarACambiarContrasenia = () => {
        const root = ReactDOM.createRoot(document.getElementById("idContenido"));
        root.render(
            <React.StrictMode>
                <Router>
                    <Routes>
                        <Route path="/" element={<Login />} />
                        <Route path="/login" element={<Login />} />
                    </Routes>
                    <CambiarContrasenia />
                </Router>
            </React.StrictMode>
        );
    }

    const cambiarAConsultarArboles = () => {
        const root = ReactDOM.createRoot(document.getElementById("idContenido"));
        root.render(
            <React.StrictMode>
                <Router>
                    <Routes>
                        <Route path="/" element={<Login />} />
                        <Route path="/login" element={<Login />} />
                    </Routes>
                    <ConsultarArboles />
                </Router>
            </React.StrictMode>
        );
    }

    const cambiarAReporteConsultas = () => {
        const root = ReactDOM.createRoot(document.getElementById("idContenido"));
        root.render(
            <React.StrictMode>
                <Router>
                    <Routes>
                        <Route path="/" element={<Login />} />
                        <Route path="/login" element={<Login />} />
                    </Routes>
                    <ReporteConsultas />
                </Router>
            </React.StrictMode>
        );
    }

    const cambiarAEjecutarRiesgoFactico = () => {
        const root = ReactDOM.createRoot(document.getElementById("idContenido"));
        root.render(
            <React.StrictMode>
                <Router>
                    <Routes>
                        <Route path="/" element={<Login />} />
                        <Route path="/login" element={<Login />} />
                    </Routes>
                    <EjecutarRiesgoFactico />
                </Router>
            </React.StrictMode>
        );
    }

    const cambiarASolicitarPerfilDeRiesgo = () => {
        const root = ReactDOM.createRoot(document.getElementById("idContenido"));
        root.render(
            <React.StrictMode>
                <Router>
                    <Routes>
                        <Route path="/" element={<Login />} />
                        <Route path="/login" element={<Login />} />
                    </Routes>
                    <SolicitarPerfilRiesgo />
                </Router>
            </React.StrictMode>
        );
    }

    const cambiarAReportePerfilDeRiesgo = () => {
        const root = ReactDOM.createRoot(document.getElementById("idContenido"));
        root.render(
            <React.StrictMode>
                <Router>
                    <Routes>
                        <Route path="/" element={<Login />} />
                        <Route path="/login" element={<Login />} />
                    </Routes>
                    <ReportesPerfilRiesgo />
                </Router>
            </React.StrictMode>
        );
    }

    const cambiarAConsultarHistorialLaboral = () => {
        const root = ReactDOM.createRoot(document.getElementById("idContenido"));
        root.render(
            <React.StrictMode>
                <Router>
                    <Routes>
                        <Route path="/" element={<Login />} />
                        <Route path="/login" element={<Login />} />
                    </Routes>
                    <ConsultarHistorialLaboral />
                </Router>
            </React.StrictMode>
        );
    }

    const cambiarAListarHistorialLaboral = () => {
        const root = ReactDOM.createRoot(document.getElementById("idContenido"));
        root.render(
            <React.StrictMode>
                <Router>
                    <Routes>
                        <Route path="/" element={<Login />} />
                        <Route path="/login" element={<Login />} />
                    </Routes>
                    <ListarHistorialLaboral />
                </Router>
            </React.StrictMode>
        );
    }

    // -------- FIN: No borrar estos métodos, ya que se usan para la generacion y manejo de los menus de usuarios de sesion, estan guardado en bdd ----//

    const salirApp = () => {
        Cookies.remove('infoSesionUsuario');
        navigate('/login');
    }

    const DynamicFaIcon = ({ name }) => {
        const IconComponent = Icons[name];
        if (!IconComponent) { // Return a default one
            return <Icons.FaBeer />;
        }
        return <IconComponent />;
    };

    return (
        typeof informacionSesionUsuario !== "undefined" ? (
            <>
                <Box sx={{ display: 'flex' }}>
                    <CssBaseline />
                    <AppBar position="fixed" open={open}>
                        <Toolbar>
                            <IconButton
                                color="inherit"
                                aria-label="open drawer"
                                onClick={handleDrawerOpen}
                                edge="start"
                                sx={{ mr: 2, ...(open && { display: 'none' }) }}
                            >
                                <MenuIcon />
                            </IconButton>
                            <Typography variant="h10" noWrap component="div" style={{ fontSize: '12px' }}>
                                Menú Principal
                            </Typography>
                            <Typography variant="h10" noWrap component="div" style={{ width: '80%', textAlign: 'right', fontSize: '12px' }}>
                                Nombre: {objetoSesion.nombreCompleto}
                                <br></br>
                                Perfil: {objetoSesion.perfilDescripcion}
                            </Typography>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Button label="Salir" icon="pi pi-sign-out" iconPos="right" style={{ width: '8%', 'backgroundColor': '#008CBA', fontSize: '12px' }} onClick={salirApp} />
                        </Toolbar>
                    </AppBar>
                    <Drawer
                        sx={{
                            width: drawerWidth,
                            flexShrink: 0,
                            '& .MuiDrawer-paper': {
                                width: drawerWidth,
                                boxSizing: 'border-box',
                            },
                        }}
                        variant="persistent"
                        anchor="left"
                        open={open}
                    >
                        <DrawerHeader>
                            <IconButton onClick={handleDrawerClose}>
                                {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                            </IconButton>
                        </DrawerHeader>
                        <Divider />
                        <List>
                            {
                                menusUsuario.map((rowPrincipal, i) => (
                                    <div key={i}>
                                        <ListItemButton sx={{ pl: 4 }} onClick={eval(rowPrincipal.nombreMetodoPrincipal)}>
                                            <ListItemIcon>
                                                <DynamicFaIcon name={rowPrincipal.nombreIconoPrincipal} />
                                            </ListItemIcon>
                                            <ListItemText
                                                primaryTypographyProps={{
                                                    fontSize: 12
                                                }}
                                                primary={rowPrincipal.nombreLabel} />
                                            <ListItemIcon>
                                                {!eval(rowPrincipal.nombreMetodoSecundario) ? <Add /> : < Remove />}
                                            </ListItemIcon>
                                        </ListItemButton>
                                        <Collapse in={eval(rowPrincipal.nombreMetodoSecundario)} timeout="auto" unmountOnExit>
                                            {
                                                rowPrincipal.menusHijos.map((rowSecundario, j) => (
                                                    <ListItem key={j} component="div" disablePadding style={{backgroundColor: '#eaeded'}}>
                                                        <ListItemButton sx={{ pl: 4 }} onClick={eval(rowSecundario.nombreMetodoPrincipal)}>
                                                            <ListItemIcon>
                                                                <DynamicFaIcon name={rowSecundario.nombreIconoPrincipal} />
                                                            </ListItemIcon>
                                                            <ListItemText
                                                                primaryTypographyProps={{
                                                                    fontSize: 12
                                                                }}
                                                                primary={rowSecundario.nombreLabel}
                                                            />
                                                        </ListItemButton>
                                                    </ListItem>
                                                ))
                                            }
                                        </Collapse>
                                    </div>
                                ))
                            }
                        </List>
                        <Divider />
                        <List>
                            {['Salir'].map((text, index) => (
                                <ListItem key={text} disablePadding>
                                    <ListItemButton sx={{ pl: 4 }} onClick={salirApp}>
                                        <ListItemIcon>
                                            <ExitToApp />
                                        </ListItemIcon>
                                        <ListItemText
                                            primaryTypographyProps={{
                                                fontSize: 12
                                            }}
                                            primary={text} 
                                        />
                                    </ListItemButton>
                                </ListItem>
                            ))}
                        </List>
                    </Drawer>
                    <Main id='idContenido' open={open}>
                    </Main>
                </Box>
            </>
        ) : (
            <>
                <div className='app_noautorizado_menus'>
                    <br></br>
                    <br></br>
                    <Panel ref={ref} header="Acceso No Autorizado">
                        <Button onClick={salirApp}>Regresar Login</Button>
                    </Panel>
                </div>
            </>
        )

    );
}

export default Menu;